package com.android.airhockey1;

/**
 * Created by ernesto on 26/01/16.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.ProblemData;
import com.android.airhockey1.util.Conditions;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.GeometryGLSurfaceView;
import com.android.airhockey1.util.ModelHelper;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import nb.geometricBot.Model;

public class TabFragment1 extends Fragment implements View.OnClickListener {
//public class TabFragment1 extends Fragment {

    /**
     * Hold a reference to our GLSurfaceView
     */
    private GeometryGLSurfaceView glSurfaceView;
    GeometryData geometry;

    ImageButton insertNode;
    ImageButton insertLine;
    ImageButton insertSurface;
    ImageButton insertForce;
    ImageButton fixDisplacement;
    ImageButton drawRectangle;
    ImageButton drawRegularPolygon;
    ImageButton drawCircle;
    ImageButton splitSegment;
    ImageButton extrudeSegment;
    ImageButton insertLinearForce;
    Button conditions;
    Button freeDrawing;
    Button shapes;
    Button lineOperations;

    boolean insertNodeIsPressed;
    boolean insertLineIsPressed;
    boolean insertSurfaceIsPressed;

    boolean insertLinearForceIsPressed;
    boolean insertForceIsPressed;
    boolean fixDisplacementIsPressed;

    boolean conditionsButtonIsActive;
    boolean freeDrawingButtonIsActive;
    boolean shapesButtonIsActive;
    boolean lineOperationsButtonIsActive;

    Conditions.Force currentForce;
    Conditions.FixedDisplacement currentFixedDisplacement;

    PlotParameters plotParameters;
    ProblemData problemData;

    boolean showGrid;

    @Override
    public void onAttach (Activity activity) {
        super.onAttach(activity);
        Log.i("Geometry fragment", "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        geometry = new GeometryData();

        // Initial plot parameters
        plotParameters = new PlotParameters();
        plotParameters.setZoomFactor(1f);
        plotParameters.setDepth(1f);
        plotParameters.setCenter(0f, 0f, 0f);

        problemData = new ProblemData();

        currentForce = new Conditions.Force();
        currentFixedDisplacement = new Conditions.FixedDisplacement();

        showGrid = false;

        Log.i("Geometry fragment", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);

        Log.i("Geometry fragment", "onCreateView");


        insertNode =      (ImageButton) view.findViewById(R.id.InsertNode);
        insertLine =      (ImageButton) view.findViewById(R.id.InsertLine);
        insertSurface =   (ImageButton) view.findViewById(R.id.InsertSurface);
        insertForce =     (ImageButton) view.findViewById(R.id.InsertForce);
        fixDisplacement = (ImageButton) view.findViewById(R.id.FixedDisplacement);
        drawRectangle   = (ImageButton) view.findViewById(R.id.DrawRectangle);
        drawRegularPolygon = (ImageButton) view.findViewById(R.id.DrawRegularPolygon);
        drawCircle         = (ImageButton) view.findViewById(R.id.DrawCircle);
        splitSegment       = (ImageButton) view.findViewById(R.id.SplitSegment);
        extrudeSegment     = (ImageButton) view.findViewById(R.id.ExtrudeSegment);
        insertLinearForce  = (ImageButton) view.findViewById(R.id.InsertLinearForce);

        conditions  = (Button) view.findViewById(R.id.Conditions);
        freeDrawing = (Button) view.findViewById(R.id.FreeDrawing);
        shapes      = (Button) view.findViewById(R.id.Shapes);
        lineOperations = (Button) view.findViewById(R.id.LineOperations);

        insertNode.setOnClickListener(this);
        insertLine.setOnClickListener(this);
        insertSurface.setOnClickListener(this);
        insertForce.setOnClickListener(this);
        fixDisplacement.setOnClickListener(this);
        drawRectangle.setOnClickListener(this);
        drawRegularPolygon.setOnClickListener(this);
        drawCircle.setOnClickListener(this);
        splitSegment.setOnClickListener(this);
        extrudeSegment.setOnClickListener(this);
        insertLinearForce.setOnClickListener(this);

        conditions.setOnClickListener(this);
        freeDrawing.setOnClickListener(this);
        shapes.setOnClickListener(this);
        lineOperations.setOnClickListener(this);

        conditionsButtonIsActive = false;
        freeDrawingButtonIsActive = false;
        shapesButtonIsActive = false;
        lineOperationsButtonIsActive = false;

        insertNodeIsPressed = false;
        insertNode.setImageResource(R.drawable.insert_node_not_selected);

        insertLineIsPressed = false;
        insertLine.setImageResource(R.drawable.insert_lines_not_selected);

        insertSurfaceIsPressed = false;
        insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);

        insertLinearForceIsPressed = false;
        insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);

        insertLinearForceIsPressed = false;
        insertForce.setImageResource(R.drawable.nodal_force_not_selected);

        fixDisplacementIsPressed = false;
        fixDisplacement.setImageResource(R.drawable.supports_not_selected);

        // Creates the glSurfaceView
        glSurfaceView = (GeometryGLSurfaceView) view.findViewById(R.id.glSurfaceView);

        //readData(Constants.temporalFileNameGeometry);

        // Some tests
        //testRectangleBar();
        //testUsingBooleanOperationOnPolygons();
        //testRectangleBarII();
        //testRectangleBarIII();
        //testSolidOfRevolution();
        //testCar();
        //testBell();
        testPlateWithHoles();

        glSurfaceView.setParameters(geometry, plotParameters, problemData);

        // To enable menus for this fragment so they appear in the menu toolbar
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.i("Geometry fragment", "onActivityCreated");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_geometry, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog dialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;
        View viewDialog;

        // handle item selection
        switch (item.getItemId()) {
            case R.id.show_grid:
                if (showGrid){
                    showGrid = false;
                    glSurfaceView.renderer.showGrid();
                }
                else {
                    showGrid = true;
                    glSurfaceView.renderer.hideGrid();
                }
                return true;

            case R.id.problem_data:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflates and sets the layout for the dialog. Pass null as the parent view because
                // its going in the dialog layout.
                viewDialog = inflater.inflate(R.layout.problem_data, null);
                alertDialogBuilder.setView(viewDialog);

                final CheckBox planeStress = (CheckBox) viewDialog.findViewById(R.id.PlaneStress);
                final CheckBox planeStrain = (CheckBox) viewDialog.findViewById(R.id.PlaneStrain);
                final CheckBox solidOfRevo = (CheckBox) viewDialog.findViewById(R.id.SolidOfRevolution);
                final EditText numberOfMeshNodes = (EditText) viewDialog.findViewById(R.id.NumberOfMeshNodes);

                handleCheckListTypeOfProblem(problemData.getTypeOfProblem(), planeStress,
                        planeStrain, solidOfRevo);

                planeStrain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            planeStress.setChecked(false);
                            solidOfRevo.setChecked(false);
                        }
                    }
                });

                planeStress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            planeStrain.setChecked(false);
                            solidOfRevo.setChecked(false);
                        }
                    }
                });

                solidOfRevo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            planeStrain.setChecked(false);
                            planeStress.setChecked(false);
                        }
                    }
                });

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (planeStress.isChecked()) {
                            if (problemData.getTypeOfProblem() != Constants.PLANE_STRESS)
                                glSurfaceView.renderer.updateLines();
                            problemData.setProblemType(Constants.PLANE_STRESS);
                        }
                        if (planeStrain.isChecked()) {
                            if (problemData.getTypeOfProblem() != Constants.PLANE_STRESS)
                                glSurfaceView.renderer.updateLines();
                            problemData.setProblemType(Constants.PLANE_STRAIN);
                        }
                        if (solidOfRevo.isChecked()) {
                            if (problemData.getTypeOfProblem() != Constants.PLANE_STRESS)
                                glSurfaceView.renderer.updateLines();
                            problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);
                        }

                        problemData.setMeshSize(
                                Integer.parseInt(numberOfMeshNodes.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                return true;

            case R.id.unify_polygons:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_A_SURFACE_FOR_UNIFY_BOOLEAN_OPERATION);
                return true;

            case R.id.intersect_polygons:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_A_SURFACE_FOR_INTERSECT_BOOLEAN_OPERATION);
                return true;

            case R.id.substract_polygons:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_A_SURFACE_FOR_SUBSTRACT_BOOLEAN_OPERATION);
                return  true;

            case R.id.difference_polygons:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_A_SURFACE_FOR_DIFFERENCE_BOOLEAN_OPERATION);
                return  true;

            case R.id.combine_polygons:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_A_SURFACE_FOR_COMBINE_BOOLEAN_OPERATION);
                return  true;

            case R.id.magnetic_grid:
                glSurfaceView.renderer.switchStatusMagneticGrid();
                return true;

            case R.id.rotation_axis:
                glSurfaceView.setTypeOfAction(Constants.SELECTING_ROTATION_AXIS);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleCheckListTypeOfProblem(final int typeOfProblem,
                                              final CheckBox planeStress,
                                              final CheckBox planeStrain,
                                              final CheckBox solidOfRevo) {


        // There is always a default type of problem
        switch (typeOfProblem) {
            case Constants.PLANE_STRESS:
                planeStress.setChecked(true);
                planeStrain.setChecked(false);
                solidOfRevo.setChecked(false);
                break;
            case Constants.PLANE_STRAIN:
                planeStress.setChecked(false);
                planeStrain.setChecked(true);
                solidOfRevo.setChecked(false);
                break;
            case Constants.SOLID_OF_REVOLUTION:
                planeStress.setChecked(false);
                planeStrain.setChecked(false);
                solidOfRevo.setChecked(true);
                break;
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart () {
        super.onStart();
        Log.i("Geometry fragment", "onStart");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (glSurfaceView.rendererSet) {
            glSurfaceView.onPause();
            //Log.i("Saving", "" + getActivity().getFilesDir());
            //saveData(Constants.temporalFileNameGeometry);
        }

        Log.i("Geometry fragment", "onResume");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (glSurfaceView.rendererSet) {
            //glSurfaceView.setGeometryFragment(geometry);
            glSurfaceView.onResume();
        }
        Log.i("Geometry fragment", "onResume");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("Geometry fragment", "onStop");
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView();
        Log.i("Geometry fragment", "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Geometry fragment", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("Geometry fragment", "onDetach");
    }

    @Override
    public void onClick(View view) {
        AlertDialog dialog;
        View viewDialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;

        switch (view.getId()) {
            case R.id.InsertNode:
                Log.i("Clicking", "Nodes button");

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_NODE);
                if (insertNodeIsPressed) {
                    insertNodeIsPressed = false;
                    insertNode.setImageResource(R.drawable.insert_node_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                insertNodeIsPressed = true;
                insertNode.setImageResource(R.drawable.node);

                glSurfaceView.setTypeOfAction(Constants.DRAWING_A_NODE);
                break;

            case R.id.InsertLine:
                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_LINE);
                if (insertLineIsPressed) {
                    insertLineIsPressed = false;
                    insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                insertLineIsPressed = true;
                insertLine.setImageResource(R.drawable.line);

                glSurfaceView.setTypeOfAction(Constants.DRAWING_A_LINE);
                Log.i("Clicking", "Lines button");
                break;

            case R.id.InsertSurface:
                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_SURFACE);
                if (insertSurfaceIsPressed) {
                    insertSurfaceIsPressed = false;
                    insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                insertSurfaceIsPressed = true;
                insertSurface.setImageResource(R.drawable.surface);

                glSurfaceView.setTypeOfAction(Constants.DRAWING_A_SURFACE);
                Log.i("Clicking", "Surfaces button");
                break;

            case R.id.InsertForce:
                Log.i("Clicking", "Force button");

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_NODAL_FORCE);
                if (insertForceIsPressed) {
                    insertForceIsPressed = false;
                    insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                insertForceIsPressed = true;
                insertForce.setImageResource(R.drawable.nodal_force);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.force_dialog, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText captureFX = (EditText) f.findViewById(R.id.ForceX);
                        EditText captureFY = (EditText) f.findViewById(R.id.ForceY);
                        // Then the user has to select a node to apply the force
                        currentForce.setForceX(Float.parseFloat(captureFX.getText().toString()));
                        currentForce.setForceY(Float.parseFloat(captureFY.getText().toString()));
                        glSurfaceView.setCurrentForce(currentForce);
                        glSurfaceView.setTypeOfAction(Constants.SELECTING_A_POINT_TO_APPLY_FORCE);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                        insertForceIsPressed = false;
                        insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.FixedDisplacement:
                Log.i("Clicking", "Displacement button");

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_SUPPORT);
                if (fixDisplacementIsPressed) {
                    fixDisplacementIsPressed = false;
                    fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                fixDisplacementIsPressed = true;
                fixDisplacement.setImageResource(R.drawable.supports);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Displacements");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.displacement_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText captureDisplacementX = (EditText) viewDialog.findViewById(R.id.displacementX);
                final EditText captureDisplacementY = (EditText) viewDialog.findViewById(R.id.displacementY);
                final CheckBox fixX = (CheckBox) viewDialog.findViewById(R.id.fixDisplacementX);
                final CheckBox fixY = (CheckBox) viewDialog.findViewById(R.id.fixDisplacementY);

                // Text fields are disabled
                captureDisplacementX.setEnabled(false);
                captureDisplacementY.setEnabled(false);

                // When the first checkbox is checked
                fixX.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) captureDisplacementX.setEnabled(true);
                        Log.i("Clicking", "Checkbox clicked.");
                    }
                });

                // When the second checkbox is checked
                fixY.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) captureDisplacementY.setEnabled(true);
                        Log.i("Clicking", "Checkbox clicked.");
                    }
                });

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Then the user has to select a node to apply the displacement
                        glSurfaceView.setTypeOfAction(
                                Constants.SELECTING_A_POINT_TO_FIX_DISPLACEMENT);

                        if (fixX.isChecked()){
                            currentFixedDisplacement.fixX(
                                    Float.parseFloat(captureDisplacementX.getText().toString()));
                        }
                        if (fixY.isChecked()) {
                            currentFixedDisplacement.fixY(
                                    Float.parseFloat(captureDisplacementY.getText().toString()));
                        }
                        glSurfaceView.setCurrentFixedDisplacement(currentFixedDisplacement);
                        currentFixedDisplacement.clear();
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                        fixDisplacementIsPressed = false;
                        fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.DrawRectangle:
                glSurfaceView.setTypeOfAction(Constants.DRAWING_A_RECTANGLE);
                break;

            case R.id.DrawRegularPolygon:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.regular_polygon_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText numberOfSides = (EditText) viewDialog.findViewById(R.id.NumberOfSidesRegularPolygon);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        glSurfaceView.setTypeOfAction(Constants.DRAWING_A_REGULAR_POLYGON);

                        glSurfaceView.renderer.setNumberOfSidesRegularPolygon(
                                Integer.parseInt(numberOfSides.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.DrawCircle:
                glSurfaceView.setTypeOfAction(Constants.DRAWING_A_CIRCLE);
                break;

            case R.id.SplitSegment:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.split_segment_dialog, null);
                alertDialogBuilder.setView(viewDialog);

                // The text fields and the checkboxes
                final EditText numberOfDivisions = (EditText) viewDialog.findViewById(R.id.NumberOfDivisions);

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        glSurfaceView.setTypeOfAction(Constants.SPLITTING_SEGMENT);

                        glSurfaceView.renderer.setNumberOfDivisions(
                                Integer.parseInt(numberOfDivisions.getText().toString()));
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the displacement
                        glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.ExtrudeSegment:
                Log.i("EXTRUDE", "Setting the action");
                glSurfaceView.setTypeOfAction(Constants.EXTRUDING_SEGMENT);
                break;
            //*
            case R.id.InsertLinearForce:

                deactivateOtherButtons(Constants.BUTTON_SELECTED_INSERT_LINEAR_FORCE);
                if (insertLinearForceIsPressed) {
                    insertLinearForceIsPressed = false;
                    insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                    glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                    break;
                }

                Log.i("Clicking", "Linear force button");
                insertLinearForceIsPressed = true;
                insertLinearForce.setImageResource(R.drawable.linear_force);

                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                alertDialogBuilder.setView(inflater.inflate(R.layout.force_dialog, null));

                // Add action buttons
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Dialog f = (Dialog) dialog;
                        EditText captureFX = (EditText) f.findViewById(R.id.ForceX);
                        EditText captureFY = (EditText) f.findViewById(R.id.ForceY);
                        // Then the user has to select a node to apply the force
                        currentForce.setForceX(Float.parseFloat(captureFX.getText().toString()));
                        currentForce.setForceY(Float.parseFloat(captureFY.getText().toString()));
                        glSurfaceView.setCurrentForce(currentForce);
                        glSurfaceView.setTypeOfAction(Constants.SELECTING_A_LINE_TO_APPLY_FORCE);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // The user did not want to apply the forces
                        glSurfaceView.setTypeOfAction(Constants.DOING_NOTHING);
                        insertLinearForceIsPressed = false;
                        insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                break;

            case R.id.Conditions:
                conditionsButtonIsActive = conditionsButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.Conditions);

                if (conditionsButtonIsActive) setVisibilityConditionsButtonsGroup(View.VISIBLE);
                else                          setVisibilityConditionsButtonsGroup(View.GONE);

               break;

            case R.id.FreeDrawing:
                freeDrawingButtonIsActive = freeDrawingButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.FreeDrawing);

                if (freeDrawingButtonIsActive) setVisibilityFreeDrawingButtonsGroup(View.VISIBLE);
                else                           setVisibilityFreeDrawingButtonsGroup(View.GONE);

                break;

            case R.id.Shapes:
                shapesButtonIsActive = shapesButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.Shapes);

                if (shapesButtonIsActive) setVisibilityShapesButtonsGroup(View.VISIBLE);
                else                      setVisibilityShapesButtonsGroup(View.GONE);

                break;

            case R.id.LineOperations:
                lineOperationsButtonIsActive = lineOperationsButtonIsActive ? false : true;
                hideOtherGroupButtons(R.id.LineOperations);

                if (lineOperationsButtonIsActive) setVisibilityLineOperationsButtonsGroup(View.VISIBLE);
                else                              setVisibilityLineOperationsButtonsGroup(View.GONE);

                break;
        }
    }

    private void setVisibilityConditionsButtonsGroup(int visibility) {
        insertForce.setVisibility(visibility);
        insertLinearForce.setVisibility(visibility);
        fixDisplacement.setVisibility(visibility);
    }

    private void setVisibilityFreeDrawingButtonsGroup(int visibility) {
        insertNode.setVisibility(visibility);
        insertLine.setVisibility(visibility);
        insertSurface.setVisibility(visibility);
    }

    private void setVisibilityShapesButtonsGroup(int visibility) {
        drawRectangle.setVisibility(visibility);
        drawCircle.setVisibility(visibility);
        drawRegularPolygon.setVisibility(visibility);
    }

    private void setVisibilityLineOperationsButtonsGroup(int visibility) {
        extrudeSegment.setVisibility(visibility);
        splitSegment.setVisibility(visibility);
    }

    private void hideOtherGroupButtons(int buttonSelected) {
        switch (buttonSelected) {
            case R.id.Conditions:
                setVisibilityFreeDrawingButtonsGroup(View.GONE);
                setVisibilityShapesButtonsGroup(View.GONE);
                setVisibilityLineOperationsButtonsGroup(View.GONE);

                freeDrawingButtonIsActive = false;
                shapesButtonIsActive = false;
                lineOperationsButtonIsActive = false;

                break;
            case R.id.FreeDrawing:
                setVisibilityConditionsButtonsGroup(View.GONE);
                setVisibilityShapesButtonsGroup(View.GONE);
                setVisibilityLineOperationsButtonsGroup(View.GONE);

                conditionsButtonIsActive = false;
                shapesButtonIsActive = false;
                lineOperationsButtonIsActive = false;

                break;
            case R.id.Shapes:
                setVisibilityConditionsButtonsGroup(View.GONE);
                setVisibilityFreeDrawingButtonsGroup(View.GONE);
                setVisibilityLineOperationsButtonsGroup(View.GONE);

                conditionsButtonIsActive = false;
                freeDrawingButtonIsActive = false;
                lineOperationsButtonIsActive = false;

                break;
            case R.id.LineOperations:
                setVisibilityConditionsButtonsGroup(View.GONE);
                setVisibilityFreeDrawingButtonsGroup(View.GONE);
                setVisibilityShapesButtonsGroup(View.GONE);

                conditionsButtonIsActive = false;
                freeDrawingButtonIsActive = false;
                shapesButtonIsActive = false;

                break;
        }
    }

    private void deactivateOtherButtons(int buttonSelected) {
        switch (buttonSelected) {
            case Constants.BUTTON_SELECTED_INSERT_SUPPORT:
                insertForceIsPressed = false;
                insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                insertLinearForceIsPressed = false;
                insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                insertNodeIsPressed = false;
                insertNode.setImageResource(R.drawable.insert_node_not_selected);
                insertLineIsPressed = false;
                insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                insertSurfaceIsPressed = false;
                insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                break;

            case Constants.BUTTON_SELECTED_INSERT_NODAL_FORCE:
                fixDisplacementIsPressed = false;
                fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                insertLinearForceIsPressed = false;
                insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                insertNodeIsPressed = false;
                insertNode.setImageResource(R.drawable.insert_node_not_selected);
                insertLineIsPressed = false;
                insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                insertSurfaceIsPressed = false;
                insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                break;

            case Constants.BUTTON_SELECTED_INSERT_LINEAR_FORCE:
                fixDisplacementIsPressed = false;
                fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                insertForceIsPressed = false;
                insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                insertNodeIsPressed = false;
                insertNode.setImageResource(R.drawable.insert_node_not_selected);
                insertLineIsPressed = false;
                insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                insertSurfaceIsPressed = false;
                insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                break;

            case Constants.BUTTON_SELECTED_INSERT_NODE:
                fixDisplacementIsPressed = false;
                fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                insertForceIsPressed = false;
                insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                insertLinearForceIsPressed = false;
                insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                insertLineIsPressed = false;
                insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                insertSurfaceIsPressed = false;
                insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                break;

            case Constants.BUTTON_SELECTED_INSERT_LINE:
                fixDisplacementIsPressed = false;
                fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                insertForceIsPressed = false;
                insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                insertLinearForceIsPressed = false;
                insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                insertNodeIsPressed = false;
                insertNode.setImageResource(R.drawable.insert_node_not_selected);
                insertSurfaceIsPressed = false;
                insertSurface.setImageResource(R.drawable.insert_surfaces_not_selected);
                break;

            case Constants.BUTTON_SELECTED_INSERT_SURFACE:
                fixDisplacementIsPressed = false;
                fixDisplacement.setImageResource(R.drawable.supports_not_selected);
                insertForceIsPressed = false;
                insertForce.setImageResource(R.drawable.nodal_force_not_selected);
                insertLinearForceIsPressed = false;
                insertLinearForce.setImageResource(R.drawable.linear_force_not_selected);
                insertNodeIsPressed = false;
                insertNode.setImageResource(R.drawable.insert_node_not_selected);
                insertLineIsPressed = false;
                insertLine.setImageResource(R.drawable.insert_lines_not_selected);
                break;
        }
    }

    // Save the data for when the app is paused or the problem is saved
    public boolean writeToFile(String outputFileName) {
        FileOutputStream fos;
        DataOutputStream dos;
        try {
            //fos = getActivity().openFileOutput(outputFileName, getActivity().MODE_PRIVATE);
            File myDir = getStorageDir("/" + Constants.APP_DIRECTORY + "/");

            File myFile = new File(myDir, outputFileName);
            if (myFile.exists()) myFile.delete();
            Log.i("FILES", myFile.toString());
            Log.i("FILES", myDir.toString());
            Log.i("FILES", outputFileName.toString());

            fos = new FileOutputStream(myFile);
            dos = new DataOutputStream(fos);

            // Saves the geometry
            if (!geometry.writeToFile(dos)) return false;

            // Saves the problem data
            if (!problemData.writeToFile(dos)) return false;

            // Close the file
            if (null != dos) dos.close();
            if (null != fos) fos.close();

            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static File getStorageDir(String name) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory() + name);
        if (!file.mkdirs()) {
            Log.i("FILES", "Directory not created");
        }
        return file;
    }

    public boolean readFromFile(String fileName) {
        FileInputStream fis;
        DataInputStream dis;

        // Clear all the shapes in the geometry
        geometry.clear();

        try {
            fis = new FileInputStream(fileName);
            dis = new DataInputStream(fis);

            if (!geometry.readFromFile(dis)) return false;

            if (!problemData.readFromFile(dis, geometry)) return false;

            glSurfaceView.renderer.updateForces();
            glSurfaceView.renderer.updateLines();
            glSurfaceView.renderer.updateSurfaces();

            //glSurfaceView.setParameters(geometry, plotParameters, problemData);

            if (null != dis) dis.close();
            if (null != fis) fis.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public GeometryData getGeometry() { return geometry; }
    public ProblemData getProblemData() { return problemData; }

    private void testRectangleBar() {
        Geometry.Node[] nodes = new Geometry.Node[4];
        Geometry.Line[] lines = new Geometry.Line[4];
        Geometry.Surface surface = new Geometry.Surface();

        nodes[0] = new Geometry.Node(-4f, -1f, 0f);
        nodes[1] = new Geometry.Node( 4f, -1f, 0f);
        nodes[2] = new Geometry.Node( 4f,  1f, 0f);
        nodes[3] = new Geometry.Node(-4f,  1f, 0f);
        for (int i = 0; i < 4; i++) geometry.addNode(nodes[i]);

        lines[0] = new Geometry.Line(nodes[0].getPoint(), nodes[1].getPoint());
        lines[1] = new Geometry.Line(nodes[1].getPoint(), nodes[2].getPoint());
        lines[2] = new Geometry.Line(nodes[2].getPoint(), nodes[3].getPoint());
        lines[3] = new Geometry.Line(nodes[3].getPoint(), nodes[0].getPoint());
        for (int i = 0; i < 4; i++) geometry.addLine(lines[i]);

        for (int i = 0; i < 4; i++) surface.addLine(lines[i]);
        surface.commit();
        surface.setModel(surface.createModel(surface));
        surface.meshSurface();
        geometry.addSurface(surface);

        Conditions.FixedDisplacement nodalFixedDisplacementA =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementA.fixX(0f);

        Conditions.FixedDisplacement nodalFixedDisplacementB =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementB.fixX(0f);
        nodalFixedDisplacementB.fixY(0f);

        nodes[0].getPoint().copyFixedDisplacement(nodalFixedDisplacementA);
        nodes[3].getPoint().copyFixedDisplacement(nodalFixedDisplacementB);

        Conditions.Force nodalForce = new Conditions.Force();
        nodalForce.setForceX(100f);
        nodalForce.setForceY(-100f);
        nodes[2].getPoint().copyForce(nodalForce);

        nodalForce.setForceX(0f);
        nodalForce.setForceY(-199);

        nodes[1].getPoint().copyForce(nodalForce);

        Conditions.Force linearForce = new Conditions.Force();
        linearForce.setForceX(0);
        linearForce.setForceY(-50);
        lines[0].copyForce(linearForce);

        problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);
        problemData.solidOfRevolution.setNumberOfSlices(20);
        problemData.solidOfRevolution.setAxis(lines[3]);
    }

    private void testUsingBooleanOperationOnPolygons() {
        Model model;
        Conditions.FixedDisplacement nodalFixedDisplacementA, nodalFixedDisplacementB;
        Conditions.Force nodalForce;
        Geometry.Rectangle rectangleA = new Geometry.Rectangle(0f,  0f, 12f, 2.5f);
        Geometry.Rectangle rectangleB = new Geometry.Rectangle(0f, -1f, 10f, 0.75f);
        Geometry.Rectangle rectangleC = new Geometry.Rectangle(0f,  1f,  6f, 0.75f);

        model = Model.substract(rectangleA.getModel(), rectangleB.getModel());
        model = Model.substract(model, rectangleC.getModel());

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        nodalFixedDisplacementA = new Conditions.FixedDisplacement();
        nodalFixedDisplacementA.fixX(0f);
        nodalFixedDisplacementA.fixY(0f);

        nodalFixedDisplacementB = new Conditions.FixedDisplacement();
        nodalFixedDisplacementB.fixY(0f);

        for (int i = 0; i < geometry.getNodes().getSize(); i++) {
            if ((Math.abs(geometry.getNode(i).GetX() + 6f) < 0.001) &&
                    (Math.abs(geometry.getNode(i).GetY() + 1.25f) < 0.001)) {
                geometry.getNode(i).getPoint().copyFixedDisplacement(nodalFixedDisplacementA);
            }

            if ((Math.abs(geometry.getNode(i).GetX() - 6f) < 0.001) &&
                    (Math.abs(geometry.getNode(i).GetY() + 1.25f) < 0.001)) {
                geometry.getNode(i).getPoint().copyFixedDisplacement(nodalFixedDisplacementB);
            }
        }

        nodalForce = new Conditions.Force();
        nodalForce.setForceX(300f);
        nodalForce.setForceY(-300f);

        for (int i = 0; i < geometry.getNodes().getSize(); i++) {
            if ((Math.abs(geometry.getNode(i).GetX() + 3f) < 0.001) &&
                    (Math.abs(geometry.getNode(i).GetY() - 1.25f) < 0.001)) {
                geometry.getNode(i).getPoint().copyForce(nodalForce);
            }
        }

        nodalForce.setForceX(0f);
        nodalForce.setForceY(-500f);
        for (int i = 0; i < geometry.getNodes().getSize(); i++) {
            if ((Math.abs(geometry.getNode(i).GetX() - 3f) < 0.001) &&
                    (Math.abs(geometry.getNode(i).GetY() - 1.25f) < 0.001)) {
                geometry.getNode(i).getPoint().copyForce(nodalForce);
            }
        }
    }

    private void testCar() {
        Model model;
        Conditions.FixedDisplacement nodalFixedDisplacementA, nodalFixedDisplacementB;
        Conditions.Force nodalForce;

        Geometry.Rectangle rectangleA = new Geometry.Rectangle(3f, 2f, 4f, 2f);
        Geometry.Rectangle rectangleB = new Geometry.Rectangle(6f, 2f, 2f, 2f);

        Geometry.Circle circleA = new Geometry.Circle(new Geometry.SimplePoint(2.5f, 0.5f, 0f),
                new Geometry.SimplePoint(2.5f, -0.1f, 0f), 20);
        Geometry.Circle circleB = new Geometry.Circle(new Geometry.SimplePoint(6f, 0.5f, 0f),
                new Geometry.SimplePoint(6f, -0.1f, 0f), 20);

        Geometry.RegularPolygon polygonA = new Geometry.RegularPolygon(5, 6f, 4f, 6f, 3f);

        rectangleA.addToGeometry(geometry);
        rectangleB.addToGeometry(geometry);
        circleA.addToGeometry(geometry);
        circleB.addToGeometry(geometry);
        polygonA.addToGeometry(geometry);
    }

    private void testSolidOfRevolution() {

        Geometry.Rectangle rectangleA = new Geometry.Rectangle(2f,  0f, 4f, 6f);
        rectangleA.addToGeometry(geometry);

        Conditions.FixedDisplacement nodalFixedDisplacementA =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementA.fixX(0f);

        Conditions.FixedDisplacement nodalFixedDisplacementB =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementB.fixX(0f);
        nodalFixedDisplacementB.fixY(0f);

        Vector<Geometry.Point> points = rectangleA.getPoints();

        points.get(0).copyFixedDisplacement(nodalFixedDisplacementA);
        points.get(3).copyFixedDisplacement(nodalFixedDisplacementB);

        Conditions.Force nodalForce = new Conditions.Force();
        nodalForce.setForceX(1000f);
        nodalForce.setForceY(-5000f);
        points.get(1).copyForce(nodalForce);

        nodalForce.setForceX(0f);
        nodalForce.setForceY(-5000);
        points.get(2).copyForce(nodalForce);

        Vector<Geometry.Line> lines = rectangleA.getLines();

        problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);
        problemData.solidOfRevolution.setNumberOfSlices(20);
        problemData.solidOfRevolution.setAxis(lines.get(3));
    }

    private void testCreatingFigures() {

        //Geometry.Circle circle = new Geometry.Circle(1f, 2f, 2f, 25);
        //circle.addToGeometry(geometry);

        Geometry.Rectangle rectangle = new Geometry.Rectangle(0f, 0f, 6f, 2f);
        rectangle.addToGeometry(geometry);

        //Geometry.RegularPolygon regularPolygon = new Geometry.RegularPolygon(5, 0f, 0f, 1f, 1f);
        //regularPolygon.addToGeometry(geometry);
    }

    private void testRectangleBarII() {
        Geometry.Node[] nodes = new Geometry.Node[4];
        Geometry.Line[] lines = new Geometry.Line[4];
        Geometry.Surface surface = new Geometry.Surface();

        nodes[0] = new Geometry.Node(-4f, -1f, 0f);
        nodes[2] = new Geometry.Node( 4f, -1f, 0f);
        nodes[1] = new Geometry.Node( 4f,  1f, 0f);
        nodes[3] = new Geometry.Node(-4f,  1f, 0f);
        for (int i = 0; i < 4; i++) geometry.addNode(nodes[i]);

        lines[0] = new Geometry.Line(nodes[0].getPoint(), nodes[2].getPoint());
        lines[1] = new Geometry.Line(nodes[2].getPoint(), nodes[1].getPoint());
        lines[2] = new Geometry.Line(nodes[1].getPoint(), nodes[3].getPoint());
        lines[3] = new Geometry.Line(nodes[3].getPoint(), nodes[0].getPoint());
        for (int i = 0; i < 4; i++) geometry.addLine(lines[i]);

        for (int i = 0; i < 4; i++) surface.addLine(lines[i]);
        surface.commit();
        surface.setModel(surface.createModel(surface));
        surface.meshSurface();
        geometry.addSurface(surface);

        Conditions.FixedDisplacement nodalFixedDisplacementA =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementA.fixX(0f);

        Conditions.FixedDisplacement nodalFixedDisplacementB =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementB.fixX(0f);
        nodalFixedDisplacementB.fixY(0f);

        nodes[0].getPoint().copyFixedDisplacement(nodalFixedDisplacementA);
        nodes[3].getPoint().copyFixedDisplacement(nodalFixedDisplacementB);

        Conditions.Force nodalForce = new Conditions.Force();
        nodalForce.setForceX(100f);
        nodalForce.setForceY(-100f);
        nodes[2].getPoint().copyForce(nodalForce);

        nodalForce.setForceX(0f);
        nodalForce.setForceY(-199);

        nodes[1].getPoint().copyForce(nodalForce);

        Conditions.Force linearForce = new Conditions.Force();
        linearForce.setForceX(0);
        linearForce.setForceY(-50);
        lines[0].copyForce(linearForce);

        problemData.setProblemType(Constants.SOLID_OF_REVOLUTION);
        problemData.solidOfRevolution.setNumberOfSlices(20);
        problemData.solidOfRevolution.setAxis(lines[3]);
    }

    // In this test the mesh is not created (maybe is a bug in the mesher)
    private void testRectangleBarIII() {
        Geometry.Node[] nodes = new Geometry.Node[5];
        Geometry.Line[] lines = new Geometry.Line[5];
        Geometry.Surface surface = new Geometry.Surface();

        nodes[0] = new Geometry.Node(-4f, -1f, 0f);
        nodes[1] = new Geometry.Node( 2f, -1f, 0f);
        nodes[2] = new Geometry.Node( 3.9f, -2.7f, 0f);
        //nodes[2] = new Geometry.Node( 4f, -1f, 0f);
        nodes[3] = new Geometry.Node( 4f,  1f, 0f);
        nodes[4] = new Geometry.Node(-4f,  1f, 0f);
        //nodes[1] = new Geometry.Node( 2.240385f, -2.5865383f, 0f);
        //nodes[2] = new Geometry.Node( 3.9614582f, -2.6930242f, 0f);

        for (int i = 0; i < 5; i++) geometry.addNode(nodes[i]);

        lines[0] = new Geometry.Line(nodes[0].getPoint(), nodes[1].getPoint());
        lines[1] = new Geometry.Line(nodes[1].getPoint(), nodes[2].getPoint());
        lines[2] = new Geometry.Line(nodes[2].getPoint(), nodes[3].getPoint());
        lines[3] = new Geometry.Line(nodes[3].getPoint(), nodes[4].getPoint());
        lines[4] = new Geometry.Line(nodes[4].getPoint(), nodes[0].getPoint());
        for (int i = 0; i < 5; i++) geometry.addLine(lines[i]);

        for (int i = 0; i < 5; i++) surface.addLine(lines[i]);
        surface.commit();
        surface.setModel(surface.createModel(surface));
        surface.meshSurface();
        geometry.addSurface(surface);

        Conditions.FixedDisplacement nodalFixedDisplacementA =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementA.fixX(0f);

        Conditions.FixedDisplacement nodalFixedDisplacementB =
                new Conditions.FixedDisplacement();
        nodalFixedDisplacementB.fixX(0f);
        nodalFixedDisplacementB.fixY(0f);

        nodes[0].getPoint().copyFixedDisplacement(nodalFixedDisplacementA);
        nodes[4].getPoint().copyFixedDisplacement(nodalFixedDisplacementB);

        Conditions.Force nodalForce = new Conditions.Force();
        nodalForce.setForceX(100f);
        nodalForce.setForceY(-100f);
        nodes[3].getPoint().copyForce(nodalForce);

        nodalForce.setForceX(0f);
        nodalForce.setForceY(-199);

        nodes[2].getPoint().copyForce(nodalForce);

        Conditions.Force linearForce = new Conditions.Force();
        linearForce.setForceX(0);
        linearForce.setForceY(-50);
        lines[1].copyForce(linearForce);

        problemData.setProblemType(Constants.PLANE_STRESS);
    }

    private void testPlateWithHoles() {
        Model model;
        Geometry.Rectangle rectangle = new Geometry.Rectangle(0f, 0f, 10f, 5f);

        final int nSides = 30;

        Geometry.Circle circleA = new Geometry.Circle(new Geometry.SimplePoint(-3f, 1.25f, 0f),
                new Geometry.SimplePoint(-3f, 0.25f, 0f), nSides);
        Geometry.Circle circleB = new Geometry.Circle(new Geometry.SimplePoint(-3f, -1.25f, 0f),
                new Geometry.SimplePoint(-3f, -0.25f, 0f), nSides);

        Geometry.Circle circleC = new Geometry.Circle(new Geometry.SimplePoint(0f, 1.25f, 0f),
                new Geometry.SimplePoint(0f, 0.25f, 0f), nSides);
        Geometry.Circle circleD = new Geometry.Circle(new Geometry.SimplePoint(0f, -1.25f, 0f),
                new Geometry.SimplePoint(0f, -0.25f, 0f), nSides);

        Geometry.Circle circleE = new Geometry.Circle(new Geometry.SimplePoint(3f, 1.25f, 0f),
                new Geometry.SimplePoint(3f, 0.25f, 0f), nSides);
        Geometry.Circle circleF = new Geometry.Circle(new Geometry.SimplePoint(3f, -1.25f, 0f),
                new Geometry.SimplePoint(3f, -0.25f, 0f), nSides);

        Geometry.Rectangle recA = new Geometry.Rectangle(-3f,  1.25f, 1f, 1f);
        Geometry.Rectangle recB = new Geometry.Rectangle(-3f, -1.25f, 1f, 1f);
        Geometry.Rectangle recC = new Geometry.Rectangle( 0f,  1.25f, 1f, 1f);
        Geometry.Rectangle recD = new Geometry.Rectangle( 0f, -1.25f, 1f, 1f);
        Geometry.Rectangle recE = new Geometry.Rectangle( 3f,  1.25f, 1f, 1f);
        Geometry.Rectangle recF = new Geometry.Rectangle( 3f, -1.25f, 1f, 1f);

        if (false) {
            rectangle.addToGeometry(geometry);
            circleA.addToGeometry(geometry);
            circleB.addToGeometry(geometry);
            circleC.addToGeometry(geometry);
            circleD.addToGeometry(geometry);
            circleE.addToGeometry(geometry);
            circleF.addToGeometry(geometry);
        } else {
            if (false) {
                ModelHelper.printModel("Model boolean A", "ModelBool", recA.getModel());
                ModelHelper.printModel("Model boolean B", "ModelBool", recB.getModel());
                model = Model.unify(recA.getModel(), recB.getModel());
                ModelHelper.printModel("Model boolean C", "ModelBool", recC.getModel());
                model = Model.unify(model, recC.getModel());
                ModelHelper.printModel("Model boolean D", "ModelBool", recD.getModel());
                model = Model.unify(model, recD.getModel());
                ModelHelper.printModel("Model boolean E", "ModelBool", recE.getModel());
                model = Model.unify(model, recE.getModel());
                ModelHelper.printModel("Model boolean F", "ModelBool", recF.getModel());
                model = Model.unify(model, recF.getModel());

                model = Model.substract(rectangle.getModel(), model);

                ModelHelper.printModel("Model boolean", "ModelBool", model);
            } else {
                //model = Model.substract(rectangle.getModel(), circleA.getModel());
                Log.i("CheckingModels", "Status A " + circleA.getModel().verify());
                Log.i("CheckingModels", "Status B " + circleB.getModel().verify());
                Log.i("CheckingModels", "Status C " + circleC.getModel().verify());
                Log.i("CheckingModels", "Status D " + circleD.getModel().verify());
                Log.i("CheckingModels", "Status E " + circleE.getModel().verify());
                Log.i("CheckingModels", "Status F " + circleF.getModel().verify());
                Log.i("CheckingModels", "Status Rec " + rectangle.getModel().verify());
                ModelHelper.printModel("Model boolean Rec", "ModelBool", rectangle.getModel());

                //*
                model = Model.unify(circleA.getModel(), circleB.getModel());
                model = Model.unify(model, circleC.getModel());
                model = Model.unify(model, circleD.getModel());
                //model = Model.unify(model, circleE.getModel());
                //model = Model.unify(model, circleF.getModel());
                model = Model.substract(rectangle.getModel(), model);
                Log.i("CheckingModels", "Status Final " + model.verify());
                //*/

                /*
                model = Model.substract(rectangle.getModel(), circleA.getModel());
                model = Model.substract(model, circleB.getModel());
                //*/
            }

            ModelHelper.createNodesAndLinesFromModel(model, geometry);
        }

    }

}