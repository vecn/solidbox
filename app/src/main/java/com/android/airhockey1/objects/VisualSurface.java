package com.android.airhockey1.objects;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.Surfaces;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.util.Geometry;

import java.util.List;

/**
 * Created by ernesto on 20/01/16.
 */
public class VisualSurface {
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private int size;

    public VisualSurface(Surfaces surfaces) {
        size = surfaces.getSize();
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualSurfaces(surfaces);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    // Update the data for OpenGL
    public void updateOpenGLData(Surfaces surfaces){
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualSurfaces(surfaces);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        size = surfaces.getSize();
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                Constants.POSITION_COMPONENT_COUNT,
                0);
    }

    // Draws everything in the draw list
    public void draw() {
        for (ObjectBuilder.DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }
}