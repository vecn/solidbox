package com.android.airhockey1.objects;

import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.Lines;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.util.Geometry.Line;

import java.util.List;

/**
 * Created by ernesto on 14/01/16.
 */
public class VisualLine {
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawListNonSelectedLines;
    private List<ObjectBuilder.DrawCommand> drawListSelectedLines;
    private List<ObjectBuilder.DrawCommand> drawListRotationAxis;
    private int size;

    public VisualLine(Lines lines,final boolean isAxialSymmetry, Line rotationAxis) {
        size = lines.getSize();
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createVisualLines(lines, isAxialSymmetry, rotationAxis);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawListNonSelectedLines = generatedData.getDrawList(0);
        drawListSelectedLines = generatedData.getDrawList(1);
        drawListRotationAxis = generatedData.getDrawList(2);
    }

    // Update the data for OpenGL
    public void updateOpenGLData(Lines setLines, final boolean isAxialSymmetry, Line rotationAxis){
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createVisualLines(setLines,
                isAxialSymmetry, rotationAxis);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawListNonSelectedLines = generatedData.getDrawList(0);
        drawListSelectedLines = generatedData.getDrawList(1);
        drawListRotationAxis = generatedData.getDrawList(2);
        size = setLines.getSize();
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                Constants.POSITION_COMPONENT_COUNT,
                0);
    }

    // Draws everything in the draw lists
    public void drawNonSelectedLines() {
        //Log.i("VisualLines", "Non selected lines size = " + drawListNonSelectedLines.size());
        for (ObjectBuilder.DrawCommand drawCommand : drawListNonSelectedLines) {
            drawCommand.draw();
        }
    }

    public void drawSelectedLines() {
        for (ObjectBuilder.DrawCommand drawCommand : drawListSelectedLines) {
            drawCommand.draw();
        }
    }

    public void drawRotationAxis() {
        //Log.i("VisualLines", "Rotation axis size = " + drawListRotationAxis.size());
        for (ObjectBuilder.DrawCommand drawCommand : drawListRotationAxis) {
            drawCommand.draw();
        }
    }

}
