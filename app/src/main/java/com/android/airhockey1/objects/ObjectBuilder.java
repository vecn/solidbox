package com.android.airhockey1.objects;

/**
 * Created by ernesto on 6/01/16.
 */
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glLineWidth;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.CompletionService;

import android.util.FloatMath;
import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.Lines;
import com.android.airhockey1.data.Nodes;
import com.android.airhockey1.data.Points;
import com.android.airhockey1.data.Surfaces;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.util.Axis;
import com.android.airhockey1.util.ColorScale;
import com.android.airhockey1.util.Conditions;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.SimpleCircle;
import com.android.airhockey1.util.Geometry.Cylinder;
import com.android.airhockey1.util.Geometry.SimplePoint;
import com.android.airhockey1.util.Geometry.Line;
import com.android.airhockey1.util.Geometry.Node;
import com.android.airhockey1.util.Geometry.Point;
import com.android.airhockey1.util.Geometry.Surface;
import com.android.airhockey1.util.Geometry.Triangle;
import com.android.airhockey1.util.Results.ColorTriangle;
import com.android.airhockey1.util.Results.ColorRGB;
import com.android.airhockey1.util.Results.ColorPaletteA;
import com.android.airhockey1.util.Results.ColorRGBT;
import com.android.airhockey1.util.Results.ColorPalette;
import com.android.airhockey1.util.Results;

import nb.geometricBot.Mesh;
import nb.pdeBot.Material;
import nb.pdeBot.finiteElement.MeshResults;

//import mx.cimat.vcn.*;

class ObjectBuilder {

    // Interface to do the draw list.
    static interface DrawCommand {
        void draw();
    }

    static class GeneratedData {
        final float[] vertexData;
        final List<List<DrawCommand>> drawLists;

        GeneratedData(float[] vertexData, List<DrawCommand> drawList) {
            this.vertexData = vertexData;
            this.drawLists = new ArrayList();
            this.drawLists.add(drawList);
        }

        public List<DrawCommand> getDrawList(int index) {
            if (index < 0) return null;
            if (index > (drawLists.size() - 1)) return null;
            return drawLists.get(index);
        }

        public void addDrawList(List<DrawCommand> drawList) {
            this.drawLists.add(drawList);
        }
    }

    static GeneratedData createPuck(Cylinder puck, int numPoints) {
        int size = sizeOfCircleInVertices(numPoints)
                + sizeOfOpenCylinderInVertices(numPoints);

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        SimpleCircle puckTop = new SimpleCircle(
                puck.center.translateY(puck.height / 2f),
                puck.radius);

        builder.appendCircle(puckTop, numPoints);
        builder.appendOpenCylinder(puck, numPoints);

        return builder.build();
    }

    static GeneratedData createMallet(SimplePoint center, float radius, float height, int numPoints) {
        int size = sizeOfCircleInVertices(numPoints) * 2
                + sizeOfOpenCylinderInVertices(numPoints) * 2;

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        // First, generate the mallet base.
        float baseHeight = height * 0.25f;

        SimpleCircle baseCircle = new SimpleCircle(
                center.translateY(-baseHeight),
                radius);
        Cylinder baseCylinder = new Cylinder(
                baseCircle.center.translateY(-baseHeight / 2f),
                radius, baseHeight);

        builder.appendCircle(baseCircle, numPoints);
        builder.appendOpenCylinder(baseCylinder, numPoints);

        // Now generate the mallet handle.
        float handleHeight = height * 0.75f;
        float handleRadius = radius / 3f;

        SimpleCircle handleCircle = new SimpleCircle(
                center.translateY(height * 0.5f),
                handleRadius);
        Cylinder handleCylinder = new Cylinder(
                handleCircle.center.translateY(-handleHeight / 2f),
                handleRadius, handleHeight);

        builder.appendCircle(handleCircle, numPoints);
        builder.appendOpenCylinder(handleCylinder, numPoints);

        return builder.build();
    }

    static GeneratedData createVisualAxis(Axis axis) {
        int size;

        /*
        Log.i("AXIS", "Before -> Quantity on x = " + axis.mainTics.getQuantityOnX());
        Log.i("AXIS", "Before -> Quantity on y = " + axis.mainTics.getQuantityOnY());
        //*/

        // The axis lines and the tics
        size = (2 + axis.mainTics.getQuantityOnX() + axis.mainTics.getQuantityOnY())*2;
        // The grid lines
        size += ((axis.mainTics.getQuantityOnX() + axis.mainTics.getQuantityOnY())*2);

        /*
        Log.i("AXIS", "Size = " + size);

        Log.i("AXIS", "After 1 -> Quantity on x = " + axis.mainTics.getQuantityOnX());
        Log.i("AXIS", "After 1 -> Quantity on y = " + axis.mainTics.getQuantityOnY());
        //*/

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        // Axis' lines
        builder.appendLine(axis.mainTics.getBeginOnX(), 0f, 0f, axis.mainTics.getEndOnX(), 0f, 0f,
                Constants.LINE_WIDTH_VISUAL_AXIS, 0);
        builder.appendLine(0f, axis.mainTics.getBeginOnY(), 0f, 0f, axis.mainTics.getEndOnY(), 0f,
                Constants.LINE_WIDTH_VISUAL_AXIS, 0);

        // Main tics' lines
        addTicsLinesOnX(axis.mainTics.getLength(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnX(),
                axis.mainTics.getQuantityOnX(),
                0f,
                Constants.LINE_WIDTH_VISUAL_AXIS,
                0,
                builder
        );

        addTicsLinesOnY(axis.mainTics.getLength(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnY(),
                axis.mainTics.getQuantityOnY(),
                0f,
                Constants.LINE_WIDTH_VISUAL_AXIS,
                0,
                builder
        );

        // The grid
        builder.drawLists.add(new ArrayList<DrawCommand>());
        addGridLinesOnX(
                axis.getMinY(),
                axis.getMaxY(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnX(),
                axis.mainTics.getQuantityOnX(),
                Constants.LINE_WIDTH_GRID,
                1,
                builder
        );

        addGridLinesOnY(
                axis.getMinX(),
                axis.getMaxX(),
                axis.mainTics.getIntervalSize(),
                axis.mainTics.getBeginOnY(),
                axis.mainTics.getQuantityOnY(),
                Constants.LINE_WIDTH_GRID,
                1,
                builder
        );

        return builder.build();
    }

    static void addGridLinesOnX(float yMin,
                                float yMax,
                                float intervalSize,
                                float beginX,
                                int numberOfTics,
                                float lineWidth,
                                int listIndex,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = beginX + (float)i*intervalSize;
            y1 = yMin;
            z1 = 0f;
            x2 = x1;
            y2 = yMax;
            z2 = z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, listIndex);
        }
    }

    static void addGridLinesOnY(float xMin,
                                float xMax,
                                float intervalSize,
                                float beginY,
                                int numberOfTics,
                                float lineWidth,
                                int listIndex,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = xMin;
            y1 = beginY + (float)i*intervalSize;
            z1 = 0f;
            x2 = xMax;
            y2 = y1;
            z2 = z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, listIndex);
        }
    }

    static void addTicsLinesOnX(float ticsLength,
                                float intervalSize,
                                float beginX,
                                int numberOfTics,
                                float y,
                                float lineWidth,
                                int listIndex,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = beginX + (float)i*intervalSize;
            y1 = y - 0.5f*ticsLength;
            z1 =  0f;
            x2 =  x1;
            y2 = -y1;
            z2 =  z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, listIndex);
        }
    }

    static void addTicsLinesOnY(float ticsLength,
                                float intervalSize,
                                float beginY,
                                int numberOfTics,
                                float x,
                                float lineWidth,
                                int listIndex,
                                ObjectBuilder builder) {
        float x1, y1, z1, x2, y2, z2;
        for (int i = 0; i < numberOfTics; i++) {
            x1 = x - 0.5f*ticsLength;
            y1 = beginY + (float)i*intervalSize;
            z1 =  0f;
            x2 = -x1;
            y2 =  y1;
            z2 =  z1;
            builder.appendLine(x1, y1, z1, x2, y2, z2, lineWidth, listIndex);
        }
    }

    static void setColorAndCoordinatesVertex( int element,
                                              int localVertex,
                                              int[] connMtx,
                                              float[] vertices,
                                              float[] values,
                                              ColorPalette colorPalette,
                                              ColorTriangle triangle,
                                              char direction) {
        final int dimension = 2;
        int globalPosition;
        float x, y, z, R, G, B, vertexValueX, vertexValueY;

        globalPosition = element*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;

        // Position
        x = vertices[connMtx[globalPosition+localVertex]*dimension];
        y = vertices[connMtx[globalPosition+localVertex]*dimension + 1];
        z = 0f;

        // Color
        vertexValueX = vertexValueY = 0f;
        switch (direction) {
            // Displacement in both directions
            case Constants.ALL_COMPONENTS:
                vertexValueX = values[connMtx[globalPosition+localVertex]*dimension];
                vertexValueY = values[connMtx[globalPosition+localVertex]*dimension + 1];
                colorPalette.setValues(vertexValueX, vertexValueY);
                break;
            // Displacement in X
            case Constants.X_COMPONENT:
                vertexValueX = values[connMtx[globalPosition+localVertex]*dimension];
                colorPalette.setValues(vertexValueX);
                break;
            // Displacement in Y
            case Constants.Y_COMPONENT:
                vertexValueY = values[connMtx[globalPosition+localVertex]*dimension + 1];
                colorPalette.setValues(vertexValueY);
                break;
        }

        R = colorPalette.getRed();
        G = colorPalette.getGreen();
        B = colorPalette.getBlue();

        // Setting the values
        triangle.setCoordinates(localVertex, x, y, z);
        triangle.setColorRGBT(localVertex, R, G, B, 1f);
    }

    static GeneratedData createVisualDisplacements(float[] vertices,
                                                   int[] connMtx,
                                                   float[] displacements,
                                                   ColorPalette colorPalette,
                                                   char direction) {
        final int dimension = Constants.PROBLEM_DIMENSION;
        final int propertiesPerVertex = 6;
        int size, numberOfTriangles;
        ColorTriangle triangle = new ColorTriangle();
        float largestDispl, smallestDispl;

        // Smallest and largest displacement
        smallestDispl = largestDispl = 0f;
        switch (direction){
            case Constants.ALL_COMPONENTS:
                smallestDispl = Results.smallestMagnitude(dimension, displacements);
                largestDispl = Results.largestMagnitude(dimension, displacements);
                break;
            case Constants.X_COMPONENT:
                smallestDispl = Results.smallestValue(dimension, displacements, 0);
                largestDispl = Results.largestValue(dimension, displacements, 0);
                break;
            case Constants.Y_COMPONENT:
                smallestDispl = Results.smallestValue(dimension, displacements, 1);
                largestDispl = Results.largestValue(dimension, displacements, 1);
                break;
        }
        colorPalette.setMinMaxValues(smallestDispl, largestDispl);

        //Log.i("Visual displacements", "smallestDispl = " + smallestDispl);
        //Log.i("Visual displacements", "largestDispl  = " + largestDispl);
        Results.printDisplacements(displacements);

        size = connMtx.length;
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        numberOfTriangles = connMtx.length / 3;
        for (int i = 0; i < numberOfTriangles; i++) {
            setColorAndCoordinatesVertex(i, 0, connMtx, vertices, displacements, colorPalette,
                    triangle, direction);
            setColorAndCoordinatesVertex(i, 1, connMtx, vertices, displacements, colorPalette,
                    triangle, direction);
            setColorAndCoordinatesVertex(i, 2, connMtx, vertices, displacements, colorPalette,
                    triangle,direction);
            builder.appendColorTriangle(triangle);
            triangle.printOpenGLData();
        }

        return builder.build();
    }

    static GeneratedData createVisualSurfaceResults(float[] vertices,
                                                    int[] connMtx,
                                                    float[] values,
                                                    ColorPalette colorPalette,
                                                    char direction) {
        final int dimension = Constants.PROBLEM_DIMENSION;
        final int propertiesPerVertex = 6; // Position and color: x, y, z, R, G, B
        int size, numberOfTriangles;
        ColorTriangle triangle = new ColorTriangle();
        float largestValue, smallestValue;

        // Smallest and largest values
        smallestValue = largestValue = 0f;
        switch (direction){
            case Constants.ALL_COMPONENTS:
                smallestValue = Results.smallestMagnitude(dimension, values);
                largestValue = Results.largestMagnitude(dimension, values);
                break;
            case Constants.X_COMPONENT:
                smallestValue = Results.smallestValue(dimension, values, 0);
                largestValue = Results.largestValue(dimension, values, 0);
                break;
            case Constants.Y_COMPONENT:
                smallestValue = Results.smallestValue(dimension, values, 1);
                largestValue = Results.largestValue(dimension, values, 1);
                break;
        }
        colorPalette.setMinMaxValues(smallestValue, largestValue);

        size = connMtx.length;
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        numberOfTriangles = connMtx.length / Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        for (int i = 0; i < numberOfTriangles; i++) {
            setColorAndCoordinatesVertex(i, 0, connMtx, vertices, values, colorPalette,
                    triangle, direction);
            setColorAndCoordinatesVertex(i, 1, connMtx, vertices, values, colorPalette,
                    triangle, direction);
            setColorAndCoordinatesVertex(i, 2, connMtx, vertices, values, colorPalette,
                    triangle,direction);
            builder.appendColorTriangle(triangle);
            triangle.printOpenGLData();
        }

        return builder.build();
    }

    static GeneratedData createAxisOrientation(float centerX,
                                               float centerY,
                                               float width,
                                               float height,
                                               float scaleOnX,
                                               float scaleOnY) {
        int size;
        SimplePoint p1 = new SimplePoint();
        SimplePoint p2 = new SimplePoint();
        SimplePoint p3 = new SimplePoint();
        final double pi = 3.1416;

        float triangleBase = 0.25f*width;
        float triangleHeight = (float)Math.tan(pi/3.0) * triangleBase * 0.5f;

        size = 10;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        Triangle triangle = new Triangle();

        p1.setCoordinates(centerX  + (-width*0.5f + triangleBase*0.5f)*scaleOnX, centerY + (height*0.5f                 )*scaleOnY, 0f);
        p2.setCoordinates(centerX  + (-width*0.5f                    )*scaleOnX, centerY + (height*0.5f - triangleHeight)*scaleOnY, 0f);
        p3.setCoordinates(centerX  + (-width*0.5f + triangleBase     )*scaleOnX, centerY + (height*0.5f - triangleHeight)*scaleOnY, 0f);
        triangle.copyCoordinates(0, p1);
        triangle.copyCoordinates(1, p2);
        triangle.copyCoordinates(2, p3);
        builder.appendTriangle(triangle);

        p1.setCoordinates(centerX + (width*0.5f                 )*scaleOnX, centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY, 0f);
        p2.setCoordinates(centerX + (width*0.5f - triangleHeight)*scaleOnX, centerY + (-height*0.5f + triangleBase         )*scaleOnY, 0f);
        p3.setCoordinates(centerX + (width*0.5f - triangleHeight)*scaleOnX, centerY + (-height*0.5f                        )*scaleOnY, 0f);
        triangle.copyCoordinates(0, p1);
        triangle.copyCoordinates(1, p2);
        triangle.copyCoordinates(2, p3);
        builder.appendTriangle(triangle);

        builder.appendLine(
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (height * 0.5f - triangleHeight)*scaleOnY,
                0f,
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY,
                0f,
                Constants.LINE_WIDTH_AXIS_ORIENTATION
        );

        builder.appendLine(
                centerX + (-width * 0.5f + triangleBase * 0.5f)*scaleOnX,
                centerY + (-height * 0.5f + triangleBase * 0.5f)*scaleOnY,
                0f,
                centerX + (width*0.5f - triangleHeight)*scaleOnX,
                centerY + (-height*0.5f + triangleBase*0.5f)*scaleOnY,
                0f,
                Constants.LINE_WIDTH_AXIS_ORIENTATION
        );

        return builder.build();
    }

    // Creates the info needed by OpenGL to draw a node on the screen. Unlike the others
    // (createVisualLines and createVisualSurfaces) this method only will draw a single node.
    // Maybe it is better to put all together so all the nodes will be drawn.
    static GeneratedData createVisualNode(SimplePoint center, float radius, int numPoints) {

        int size = sizeOfCircleInVertices(numPoints);

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        SimpleCircle visualCircle = new SimpleCircle(center, radius);

        builder.appendCircle(visualCircle, numPoints);

        return builder.build();

    }

    static GeneratedData createForces(Points points, float unitLengthNDC, float maxForce) {
        int size, numberOfNodalForces;
        float factor, lengthSideLines;
        Point point;

        // Only nodal forces
        numberOfNodalForces = 0;
        for (int i = 0; i < points.getSize(); i++) {
            point = points.getElement(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasAppliedForce()){
                    numberOfNodalForces += 1;
                }
            }
        }
        size = 6*numberOfNodalForces;

        ObjectBuilder builder = new ObjectBuilder(size, Constants.FLOATS_PER_VERTEX);

        factor = Constants.LENGTH_LARGEST_MAGNITUDE_FORCE / (maxForce*unitLengthNDC);
        lengthSideLines = Constants.LENGTH_FORCES_SIDE_LINES / unitLengthNDC;
        for (int i = 0; i < points.getSize(); i++) {
            point = points.getElement(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasAppliedForce()){
                    builder.appendForce(point, factor, lengthSideLines);
                }
            }
        }

        return builder.build();
    }

    static int getNumberOfForcesPerLine(Line line, float unitLengthNDC) {
        float area, height;
        Geometry.Vector a = new Geometry.Vector(
                line.getLastPoint().getX() - line.getFirstPoint().getX(),
                line.getLastPoint().getY() - line.getFirstPoint().getY(),
                line.getLastPoint().getZ() - line.getFirstPoint().getZ()
        );
        Geometry.Vector b = new Geometry.Vector(
                line.getConditions().getForce().getForceX(),
                line.getConditions().getForce().getForceY(),
                line.getConditions().getForce().getForceZ()
        );

        area = a.crossProduct(b).length();
        if (area > (100*Constants.PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE))
            height = area/b.length();
        else
            height = a.length();

        //Log.i("LinearForce", "Area = " + area);
        //Log.i("LinearForce", "PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE = " + Constants.PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE);

        int numberOfForces = (int)(
                height*unitLengthNDC/Constants.PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE);
        numberOfForces++;

        return numberOfForces;
    }

    static GeneratedData createForces(Lines lines, float unitLengthNDC, float maxForce) {
        int size, numberOfForces, n;
        float factor, lengthSideLines;
        Line line;
        ArrayList<Integer> numberOfForcesPerLine = new ArrayList();

        numberOfForces = 0;
        for (int i = 0; i < lines.getSize(); i++) {
            line = lines.getElement(i);
            if (line.hasConditions()) {
                if (line.getConditions().hasAppliedForce()){
                    n = getNumberOfForcesPerLine(line, unitLengthNDC);
                    numberOfForces += n;
                    numberOfForcesPerLine.add(n);
                }
            }
        }
        size = 6*numberOfForces;

        ObjectBuilder builder = new ObjectBuilder(size, Constants.FLOATS_PER_VERTEX);

        factor = Constants.LENGTH_LARGEST_MAGNITUDE_FORCE / (maxForce*unitLengthNDC);
        lengthSideLines = Constants.LENGTH_FORCES_SIDE_LINES_II / unitLengthNDC;
        n = 0;
        for (int i = 0; i < lines.getSize(); i++) {
            line = lines.getElement(i);
            if (line.hasConditions()) {
                if (line.getConditions().hasAppliedForce()){
                    builder.appendForce(line, factor, unitLengthNDC, lengthSideLines,
                            numberOfForcesPerLine.get(n));
                    n++;
                }
            }
        }

        return builder.build();
     }

    static GeneratedData createSectionLines(Surface surface, MeshResults meshResults) {
        final int dim = Constants.PROBLEM_DIMENSION;
        int size;
        Point point;
        Node node;
        float[] vertices = meshResults.getVerticesRef();

        size = 0;
        for (int i = 0; i < surface.getNPoints(); i++) {
            point = surface.getPoint(i);
            if (point.getNodeInfo().isPartOfANode()) size++;
        }
        size = size*2;

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        for (int i = 0; i < surface.getNPoints(); i++) {
            point = surface.getPoint(i);
            //Log.i("Drawing section", "" + node.getMeshId());
            if (point.getNodeInfo().isPartOfANode()) {
                node = point.getNodeInfo().getNode();
                builder.appendLine(
                        vertices[node.getMeshId() * dim],
                        vertices[node.getMeshId() * dim + 1],
                        -5f,
                        vertices[node.getMeshId() * dim],
                        vertices[node.getMeshId() * dim + 1],
                        5f,
                        5f
                );
            }
        }

        return builder.build();
    }

    static void createElementsForSolidOfRevolution(ArrayList<VertexArray> holderVertexArray,
                                                   ArrayList<List<ObjectBuilder.DrawCommand>> holderDrawList,
                                                   ArrayList<int[]> holderRotatingVerticesIds,
                                                   int[][] modelSgm,
                                                   int axisIdOnModel,
                                                   int numberOfSlices) {
        final int numberOfVerticesPerSlice;
        final int propertiesPerVertex;

        // Vertices that will be rotated. We exclude the axis' vertices.
        holderRotatingVerticesIds.add(getRotatingVerticesIds(modelSgm, axisIdOnModel));

        numberOfVerticesPerSlice = 2*holderRotatingVerticesIds.get(0).length;
        propertiesPerVertex = 6;

        holderVertexArray.add(new VertexArray(numberOfSlices*numberOfVerticesPerSlice*propertiesPerVertex));

        ArrayList<DrawCommand> drawList = new ArrayList<DrawCommand>();
        for (int i = 0; i < numberOfSlices; i++) {
            final int startVertex = i*numberOfVerticesPerSlice;
            drawList.add(new DrawCommand() {
                @Override
                public void draw() {
                    glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numberOfVerticesPerSlice);
                }
            });
        }
        holderDrawList.add(drawList);
    }

    static GeneratedData createSolidOfRevolution(MeshResults meshResults,
                                                 int axisIdOnModel,
                                                 int numberOfSlices,
                                                 int typeOfSurfaceResult,
                                                 ColorPalette colorPalette) {
        char direction;
        int size;
        int[] rotatingVerticesIds;
        int[][] modelSgm = meshResults.getModelSgmRef();
        float[] vertices = meshResults.getVerticesRef();
        float[] values;
        float startAngle, endAngle, angleInterval, smallestValue, largestValue;
        final int dimension = Constants.PROBLEM_DIMENSION;

        // Type of surface result
        direction = Constants.ONE_COMPONENT;
        values = null;
        smallestValue = largestValue = 0f;
        switch (typeOfSurfaceResult) {
            case Constants.DISPLACEMENTS:
                values = meshResults.getDisplacementRef();
                direction = Constants.ALL_COMPONENTS;
                smallestValue = Results.smallestMagnitude(dimension, values);
                largestValue = Results.largestMagnitude(dimension, values);
                break;
            case Constants.DISPLACEMENTS_ON_X:
                values = meshResults.getDisplacementRef();
                direction = Constants.X_COMPONENT;
                smallestValue = Results.smallestValue(dimension, values, 0);
                largestValue = Results.largestValue(dimension, values, 0);
                break;
            case Constants.DISPLACEMENTS_ON_Y:
                values = meshResults.getDisplacementRef();
                direction = Constants.Y_COMPONENT;
                smallestValue = Results.smallestValue(dimension, values, 1);
                largestValue = Results.largestValue(dimension, values, 1);
                break;
            case Constants.DEFORMATIONS:
                values = meshResults.getStrainRef();
                direction = Constants.ALL_COMPONENTS;
                smallestValue = Results.smallestMagnitude(dimension, values);
                largestValue = Results.largestMagnitude(dimension, values);
                break;
            case Constants.VON_MISES:
                values = meshResults.getVonMisesStressRef();
                direction = Constants.ONE_COMPONENT;
                smallestValue = Results.smallestValue(values);
                largestValue = Results.largestValue(values);
                break;
        }
        colorPalette.setMinMaxValues(smallestValue, largestValue);

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return null;

        // Vertices that will be rotated. We exclude the axis' vertices.
        rotatingVerticesIds = getRotatingVerticesIds(modelSgm, axisIdOnModel);

        size = numberOfSlices*2*rotatingVerticesIds.length;
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.appendSliceSurface(startAngle, endAngle, rotatingVerticesIds, vertices,
                    direction, values, colorPalette);
            startAngle += angleInterval;
        }

        return builder.build();
    }

    static void setVerticesValuesSolidOfRevolution(VertexArray vertexArray,
                                                   char direction,
                                                   float[] values,
                                                   int numberOfSlices,
                                                   int[] rotatingVerticesIds,
                                                   ColorPalette colorPalette) {

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return;

        ObjectBuilder builder = new ObjectBuilder(0, 0);

        for (int i = 0; i < numberOfSlices; i++)
            builder.setVerticesValuesSlice(i, rotatingVerticesIds.length*2, 6, vertexArray,
                    rotatingVerticesIds, direction, values, colorPalette);
    }

    static void setVerticesCoordinatesSolidOfRevolution(VertexArray vertexArray,
                                                        float[] vertices,
                                                        int[] rotatingVerticesIds,
                                                        int numberOfSlices) {
        float startAngle, endAngle, angleInterval;

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return;

        ObjectBuilder builder = new ObjectBuilder(0, 0);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.setVerticesCoordinatesSlice(i, rotatingVerticesIds.length * 2, 6, vertexArray,
                    startAngle, endAngle, rotatingVerticesIds, vertices);
            startAngle += angleInterval;
        }
    }

    static GeneratedData createMeshForSolidOfRevolution(MeshResults meshResults,
                                                        int axisIdOnModel,
                                                        int numberOfSlices) {
        int size;
        int[] rotatingVerticesIds;
        int[][] modelSgm = meshResults.getModelSgmRef();
        float[] vertices = meshResults.getVerticesRef();
        float startAngle, endAngle, angleInterval;

        // Minimum number of slices to construct the solid
        if (numberOfSlices < 4) return null;

        // Vertices that will be rotated. We exclude the axis' vertices.
        rotatingVerticesIds = getRotatingVerticesIds(modelSgm, axisIdOnModel);

        Log.i("SolidOfRevolution", "Rotating vertices");
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            Log.i("SolidOfRevolution", "" + i + " " + rotatingVerticesIds[i] + " " +
                    vertices[rotatingVerticesIds[i]*2+0] + " " + vertices[rotatingVerticesIds[i]*2+1]);
        }

        size = numberOfSlices*(4*(rotatingVerticesIds.length - 1) + 2);
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        angleInterval = 2f*(float)Math.PI / (float)numberOfSlices;
        startAngle = endAngle = 2f*(float)Math.PI - 0.5f*angleInterval;
        for (int i = 0; i < numberOfSlices; i++) {
            endAngle += angleInterval;
            builder.appendMeshForSliceSurface(startAngle, endAngle, rotatingVerticesIds, vertices);
            startAngle += angleInterval;
        }

        return builder.build();
    }

    static int[] getRotatingVerticesIds(int[][] modelSgm, int axisIdOnModel) {
        int[] previousModelEdge, currentModelEdge, rotatingVertices;
        int modelEdgeId, start, increment, sA, eA, sB, eB, count, numberOfVisitedModelEdges;
        Set<Integer> visitedModelEdgesIds = new HashSet();

        // Number of vertices that will be rotated
        count = 0;
        for (int i = 0; i < modelSgm.length; i++) count += modelSgm[i].length;
        count -= modelSgm[axisIdOnModel].length;

        rotatingVertices = new int[count];

        previousModelEdge = modelSgm[axisIdOnModel];

        visitedModelEdgesIds.add(axisIdOnModel);
        numberOfVisitedModelEdges = 1;

        // We get the edge's id next to the rotating axis
        modelEdgeId = getNextEdgeModelId(visitedModelEdgesIds, axisIdOnModel, modelSgm);

        count = 0;
        //while ((modelEdgeId != axisIdOnModel) && (numberOfVisitedModelEdges < modelSgm.length)) {
        while (numberOfVisitedModelEdges < modelSgm.length) {

            currentModelEdge = modelSgm[modelEdgeId];

            // Auxiliary variables to write less
            sA = 0;
            eA = previousModelEdge.length - 1;
            sB = 0;
            eB = currentModelEdge.length - 1;

            // We see which node is common to the edges and on which direction we should
            // go through the vertices in the edge
            if (previousModelEdge[sA] == currentModelEdge[sA]) {
                start = 0;
                increment = 1;
            } else if (previousModelEdge[sA] == currentModelEdge[eB]) {
                start = modelSgm[modelEdgeId].length - 1;
                increment = -1;
            } else if (previousModelEdge[eA] == currentModelEdge[sB]) {
                start = 0;
                increment = 1;
            } else {
                start = modelSgm[modelEdgeId].length - 1;
                increment = -1;
            }

            // We add the vertices ids in the right order
            for (int i = 0; i < currentModelEdge.length; i++) {
                rotatingVertices[count++] = currentModelEdge[start];
                start += increment;
            }
            numberOfVisitedModelEdges++;

            previousModelEdge = currentModelEdge;

            // We look for the next adjacent edge
            modelEdgeId = getNextEdgeModelId(visitedModelEdgesIds, modelEdgeId, modelSgm);
        }
        return rotatingVertices;
    }

    static int getNextEdgeModelId(Set<Integer> visitedEdgesIds,
                                  int currentEdgeModelId,
                                  int[][] modelSgm) {
        int nextEdgeModelId, sA, eA, sB, eB;
        int[] nextEdge;
        int[] currentEdge = modelSgm[currentEdgeModelId];

        sA = 0;
        eA = currentEdge.length - 1;
        for (nextEdgeModelId = 0; nextEdgeModelId < modelSgm.length; nextEdgeModelId++) {
            nextEdge = modelSgm[nextEdgeModelId];
            sB = 0;
            eB = nextEdge.length - 1;
            if ((!visitedEdgesIds.contains(nextEdgeModelId)) &&
                ((currentEdge[sA] == nextEdge[sB] || (currentEdge[sA] == nextEdge[eB])) ||
                 (currentEdge[eA] == nextEdge[sB] || (currentEdge[eA] == nextEdge[eB])))) {
                visitedEdgesIds.add(nextEdgeModelId);
                break;
            }
        }

        return nextEdgeModelId;
    }

    // Creates the info needed by OpenGL to draw all the lines
    static GeneratedData createVisualLines(Lines lines,
                                           final boolean isAxialSymmetry,
                                           Line rotationAxis) {
        int size;
        Line visualLine;

        // Two points per line
        size = lines.getSize() * 2;

        ObjectBuilder builder = new ObjectBuilder(size, 3);

        // New drawing lists
        builder.drawLists.add(new ArrayList<DrawCommand>()); // For the selected lines
        builder.drawLists.add(new ArrayList<DrawCommand>()); // For the rotation axis

        if (isAxialSymmetry && (null != rotationAxis)) {
            // First we append the non-selected lines
            for (int i = 0; i < lines.getSize(); i++) {
                visualLine = lines.getElement(i);
                if (rotationAxis.getId() == visualLine.getId()) continue;
                if (!visualLine.propertiesInCAD.isSelected()) builder.appendLine(
                        visualLine,
                        0,
                        Constants.LINE_WIDTH_NOT_SELECTED_VISUAL_LINE
                );
            }

            // Then we append the selected lines
            for (int i = 0; i < lines.getSize(); i++) {
                visualLine = lines.getElement(i);
                if (rotationAxis.getId() == visualLine.getId()) continue;
                if (visualLine.propertiesInCAD.isSelected()) builder.appendLine(
                        visualLine,
                        1,
                        Constants.LINE_WIDTH_SELECTED_VISUAL_LINE
                );
            }

            // The rotation axis
            if (null != rotationAxis) {
                builder.appendLine(
                        rotationAxis,
                        2,
                        Constants.LINE_WIDTH_SELECTED_VISUAL_LINE
                );
            }
        } else {
            // First we append the non-selected lines
            for (int i = 0; i < lines.getSize(); i++) {
                visualLine = lines.getElement(i);
                if (!visualLine.propertiesInCAD.isSelected()) builder.appendLine(
                        visualLine,
                        0,
                        Constants.LINE_WIDTH_NOT_SELECTED_VISUAL_LINE
                );
            }

            // Then we append the selected lines
            for (int i = 0; i < lines.getSize(); i++) {
                visualLine = lines.getElement(i);
                if (visualLine.propertiesInCAD.isSelected()) builder.appendLine(
                        visualLine,
                        1,
                        Constants.LINE_WIDTH_SELECTED_VISUAL_LINE
                );
            }
        }

        return builder.build();
    }

    // Creates the info needed by OpenGL to draw all the surfaces
    static GeneratedData createVisualSurfaces(Surfaces surfaces) {
        int size;
        Surface surface;

        // Total number of points needed to draw all the surfaces
        size = 0;
        for (int i = 0; i < surfaces.getSize(); i++){
            if (null != surfaces.getElement(i).getMesh())
                size += surfaces.getElement(i).getMesh().getNElements();
        }

        size *= Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;

        // Builder object
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        // Creates the list of points
        for (int i = 0; i < surfaces.getSize(); i++) {
            surface = surfaces.getElement(i);
            builder.appendSurface(surface);
        }

        return builder.build();
    }

    // Creates the info needed by OpenGL to draw a mesh
    static GeneratedData createVisualMesh(Mesh mesh) {
        int size, nMeshEdges;
        float x1, y1, x2, y2;
        float[] vertices = mesh.getVerticesRef();
        int[] connEdges = mesh.getConnEdgesRef();

        // Total of vertices
        nMeshEdges = mesh.getNEdges();
        size = nMeshEdges * 2;

        // Builder object
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        for (int i = 0; i < nMeshEdges; i++) {
            x1 = vertices[connEdges[i*2]*2];
            y1 = vertices[connEdges[i*2]*2+1];
            x2 = vertices[connEdges[i*2+1]*2];
            y2 = vertices[connEdges[i*2+1]*2+1];
            builder.appendLine(x1, y1, 0f, x2, y2, 0f, Constants.LINE_WIDTH_VISUAL_MESH);
        }

        return builder.build();
    }

    static GeneratedData createPartialFixedSupport(float nodeX,
                                                   float nodeY,
                                                   float base) {
        int size;
        int numberOfSidesPerWheel = 20;
        int numberOfWheels = 3;
        float spaceBetweenWheels = 0.05f*base;
        int numberOfSupportLines = 6;
        float radius;

        // Vertices(Triangle + wheels + surface line + support lines)
        size = 3 + numberOfWheels*sizeOfCircleInVertices(numberOfSidesPerWheel) +
                2 + 2*numberOfSupportLines;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        radius = 0.5f*(base/(float)numberOfWheels - spaceBetweenWheels);

        putTheTriangle(nodeX, nodeY, base, builder);
        putTheWheels(nodeX - 0.5f * base, nodeY - (float) Math.sin(Math.toRadians(60.0)) * base,
                radius, base, numberOfWheels, numberOfSidesPerWheel, builder);
        putSupportLines(nodeX - 0.6f*base, nodeY - (float)Math.sin(Math.toRadians(60.0))*base - 2f*radius,
                1.2f*base, numberOfSupportLines, builder);

        return builder.build();
    }

    static GeneratedData createFixedSupport(float nodeX,
                                            float nodeY,
                                            float base) {
        int size;
        int numberOfSupportLines = 6;

        // Vertices(Triangle + surface line + support lines)
        size = 3 + 2 + 2*numberOfSupportLines;
        ObjectBuilder builder = new ObjectBuilder(size, 3);

        putTheTriangle(nodeX, nodeY, base, builder);
        putSupportLines(nodeX - 0.6f * base, nodeY - (float) Math.sin(Math.toRadians(60.0)) * base,
                1.2f * base, numberOfSupportLines, builder);

        return builder.build();
    }

    private static void putTheTriangle(float startX,
                                       float startY,
                                       float base,
                                       ObjectBuilder builder) {
        Triangle triangle = new Triangle();
        SimplePoint point = new SimplePoint();
        float height = (float)Math.sin(Math.toRadians(60.0))*base;

        point.setCoordinates(startX, startY, 0f);
        triangle.copyCoordinates(0, point);

        point.setCoordinates(startX - 0.5f * base, startY - height, 0f);
        triangle.copyCoordinates(1, point);

        point.setCoordinates(startX + 0.5f * base, startY - height, 0f);
        triangle.copyCoordinates(2, point);

        builder.appendTriangle(triangle);
    }

    private static void putTheWheels(float startX,
                                     float startY,
                                     float radius,
                                     float base,
                                     int numberOfWheels,
                                     int numberOfSidesPerWheel,
                                     ObjectBuilder builder) {
        float distanceBetweenCenters = base/(float)numberOfWheels;
        float centerY = startY - radius;
        SimpleCircle circle = new SimpleCircle();

        circle.setRadius(radius);

        for (int i = 0; i < numberOfWheels; i++) {
            circle.setCenter(startX + (0.5f + (float)i)*distanceBetweenCenters, centerY, 0f);
            builder.appendCircle(circle, numberOfSidesPerWheel);
        }
    }

    private static void putSupportLines(float startX,
                                 float startY,
                                 float length,
                                 int numberOfSupportLines,
                                 ObjectBuilder builder) {
        float separation = length / (float) numberOfSupportLines;
        float height = 0.2f*length;

        // Line touching the wheels
        builder.appendLine(startX, startY, 0f, startX + length, startY, 0f, Constants.LINE_WIDTH_VISUAL_AXIS);

        // The lines to simulate ground under the line touching the wheels
        for (int i = 0; i < numberOfSupportLines; i++) {
            builder.appendLine(
                    startX + (float) i * separation, startY - height, 0f,
                    startX + (1f + (float) i) * separation, startY, 0f,
                    Constants.LINE_WIDTH_VISUAL_AXIS
            );
        }
    }

    public static GeneratedData createColorScale(ColorScale colorScale) {
        float triangleStripWidth, stripCenterX, stripCenterY;
        // Number of vertices to create the color scale using triangle strips
        int size = colorScale.getNumberOfDivisionsOnX()*
                (2*(colorScale.getNumberOfDivisionsOnY() + 1));

        // Builder
        ObjectBuilder builder = new ObjectBuilder(size, 6);

        // We have to cover all the colors along the color scale
        colorScale.colorPalette.setMinMaxValues(colorScale.getCenterY() - 0.5f*colorScale.getHeight(),
                colorScale.getCenterY() + 0.5f*colorScale.getHeight());

        // Append the triangle strips
        triangleStripWidth = colorScale.getWidth() / colorScale.getNumberOfDivisionsOnX();
        stripCenterY = colorScale.getCenterY();
        stripCenterX = colorScale.getCenterX() - 0.5f*colorScale.getWidth() + 0.5f*triangleStripWidth;
        for (int i = 0; i < colorScale.getNumberOfDivisionsOnX(); i++) {
            builder.appendTriangleStrip(stripCenterX, stripCenterY, triangleStripWidth,
                    colorScale.getHeight(), colorScale.getNumberOfDivisionsOnY() + 1,
                    colorScale.colorPalette);
            stripCenterX += triangleStripWidth;
        }

        return builder.build();
    }

    // Calculates the number of vertices needed to draw a polygon splitting it into triangles.
    // Its not been used a triangle fan.
    private static int sizeOfPolygonInVertices(int numPoints) { return (numPoints - 2)*3; }

    // Number of points needed to define a fan circle
    private static int sizeOfCircleInVertices(int numPoints) {
        return 1 + (numPoints + 1);
    }

    // Number of points needed to define a triangle strip
    private static int sizeOfOpenCylinderInVertices(int numPoints) {
        return (numPoints + 1) * 2;
    }

    private static int sizeOfTriangleStripInVertices(int numPoints) { return numPoints * 2; }

    private final float[] vertexData;
    private List<List<DrawCommand>> drawLists;
    //drawLists.add(new ArrayList<DrawCommand>());
            //(new ArrayList());
    private int offset = 0;

    // Constructor
    private ObjectBuilder(int sizeInVertices, int floatsPerVertex) {
        vertexData = new float[sizeInVertices * floatsPerVertex];
        this.drawLists = new ArrayList();
        this.drawLists.add(new ArrayList<DrawCommand>());
    }

    // Crates the the list of points to approximate a circle in the XY plane
    private void appendCircle(SimpleCircle circle, int numPoints) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = sizeOfCircleInVertices(numPoints);

        // Center point of fan
        vertexData[offset++] = circle.center.x;
        vertexData[offset++] = circle.center.y;
        vertexData[offset++] = circle.center.z;

        // Fan around center point. <= is used because we want to generate
        // the point at the starting angle twice to complete the fan.
        for (int i = 0; i <= numPoints; i++) {
            float angleInRadians = ((float) i / (float) numPoints) * ((float) Math.PI * 2f);

            vertexData[offset++] = circle.center.x + circle.radius * (float)Math.cos(angleInRadians);
            vertexData[offset++] = circle.center.y + circle.radius * (float)Math.sin(angleInRadians);
            vertexData[offset++] = circle.center.z;
        }
        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLE_FAN, startVertex, numVertices);
            }
        });
    }

    private void appendOpenCylinder(Cylinder cylinder, int numPoints) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = sizeOfOpenCylinderInVertices(numPoints);
        final float yStart = cylinder.center.y - (cylinder.height / 2f);
        final float yEnd = cylinder.center.y + (cylinder.height / 2f);

        // Generate strip around center point. <= is used because we want to
        // generate the points at the starting angle twice, to complete the
        // strip.
        for (int i = 0; i <= numPoints; i++) {
            float angleInRadians = ((float) i / (float) numPoints) * ((float) Math.PI * 2f);

            float xPosition = cylinder.center.x + cylinder.radius * ((float)Math.cos(angleInRadians));

            float zPosition = cylinder.center.z + cylinder.radius * ((float)Math.sin(angleInRadians));

            vertexData[offset++] = xPosition;
            vertexData[offset++] = yStart;
            vertexData[offset++] = zPosition;

            vertexData[offset++] = xPosition;
            vertexData[offset++] = yEnd;
            vertexData[offset++] = zPosition;
        }
        // Weird form to add an element to the draw list. Also the interface is used to define a new
        // type of data.
        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                // Here we use GL_TRIANGLE_STRIP to do a triangle strip
                glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numVertices);
            }
        });
    }

    private void appendTriangleStrip(float centerX, float centerY, float width, float height,
                                     int numPoints, ColorPalette colorPalette) {
        final int startVertex = offset / Constants.FLOATS_PER_COLOR_VERTEX;
        final int numVertices = sizeOfTriangleStripInVertices(numPoints);
        final float yStart = centerY - 0.5f*height;
        final float yEnd = centerY + 0.5f*height;
        final float xStart = centerX - 0.5f*width;
        final float xEnd = centerX + 0.5f*width;
        final float interval = height / (float)(numPoints - 1);
        float y;

        y = yStart;
        for (int i = 0; i < numPoints; i++) {
            colorPalette.setValues(y);
            vertexData[offset++] = xStart;
            vertexData[offset++] = y;
            vertexData[offset++] = 0f;
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getGreen();
            vertexData[offset++] = colorPalette.getBlue();

            colorPalette.setValues(y);
            vertexData[offset++] = xEnd;
            vertexData[offset++] = y;
            vertexData[offset++] = 0f;
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getGreen();
            vertexData[offset++] = colorPalette.getBlue();

            y += interval;
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                // Here we use GL_TRIANGLE_STRIP to do a triangle strip
                glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numVertices);
            }
        });
    }

    private void appendLine(Line line,
                            int listIndex,
                            final float lineWidth) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2;

        vertexData[offset++] = line.getFirstPoint().getX();
        vertexData[offset++] = line.getFirstPoint().getY();
        vertexData[offset++] = line.getFirstPoint().getZ();

        vertexData[offset++] = line.getLastPoint().getX();
        vertexData[offset++] = line.getLastPoint().getY();
        vertexData[offset++] = line.getLastPoint().getZ();

        drawLists.get(listIndex).add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(lineWidth);
                //glClearColor(); // HERE...!!!!!!
                glDrawArrays(GL_LINES, startVertex, numVertices);
            }
        });
    }

    /*
    private void appendSurface(Surface surface) {
        Vector<Node> nodes = surface.getSurfaceNodes();
        Triangle triangle = new Triangle();
        SimplePoint point = new SimplePoint();

        int[] connMtx = surface.getMesh().getConnMtxRef();
        float[] vertices = surface.getMesh().getVerticesRef();
        final int numberOfElements = surface.getMesh().getNElements();
        final int dim = Constants.PROBLEM_DIMENSION;
        int index;

        for (int i = 0; i < numberOfElements; i++) {
            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 0];
            point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
            triangle.copyCoordinates(0, point);

            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 1];
            point.setCoordinates(vertices[index * dim], vertices[index * dim + 1], 0f);
            triangle.copyCoordinates(1, point);

            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 2];
            point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
            triangle.copyCoordinates(2, point);

            appendTriangle(triangle);
        }
    }
    */

    private void appendSurface(Surface surface) {
        Triangle triangle = new Triangle();
        SimplePoint point = new SimplePoint();

        if (null == surface.getMesh()) return;

        int[] connMtx = surface.getMesh().getConnMtxRef();
        float[] vertices = surface.getMesh().getVerticesRef();
        final int numberOfElements = surface.getMesh().getNElements();
        final int dim = Constants.PROBLEM_DIMENSION;
        int index;

        for (int i = 0; i < numberOfElements; i++) {
            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 0];
            point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
            triangle.copyCoordinates(0, point);

            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 1];
            point.setCoordinates(vertices[index * dim], vertices[index * dim + 1], 0f);
            triangle.copyCoordinates(1, point);

            index = connMtx[i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT + 2];
            point.setCoordinates(vertices[index*dim], vertices[index*dim+1], 0f);
            triangle.copyCoordinates(2, point);

            appendTriangle(triangle);
        }
    }

    private void appendTriangle(Triangle triangle) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        SimplePoint point;

        for (int i = 0; i < numVertices; i++) {
            point = triangle.getVertex(i);
            vertexData[offset++] = point.x;
            vertexData[offset++] = point.y;
            vertexData[offset++] = point.z;
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLES, startVertex, numVertices);
            }
        });
    }

    private void appendColorTriangle(ColorTriangle triangle) {
        final int startVertex = offset / Constants.FLOATS_PER_COLOR_VERTEX;
        final int numVertices = Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        SimplePoint point;

        for (int i = 0; i < numVertices; i++) {
            point = triangle.getVertex(i);
            vertexData[offset++] = point.x;
            vertexData[offset++] = point.y;
            vertexData[offset++] = point.z;
            vertexData[offset++] = triangle.getRed(i);
            vertexData[offset++] = triangle.getGreen(i);
            vertexData[offset++] = triangle.getBlue(i);
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(GL_TRIANGLES, startVertex, numVertices);
            }
        });
    }

    public static void appendTriangle(final int startVertex,
                                      List<DrawCommand> drawList) {
        drawList.add(new ObjectBuilder.DrawCommand() {
            @Override
            public void draw() {
                glDrawArrays(
                        GL_TRIANGLES,
                        startVertex,
                        Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT
                );
            }
        });
    }

    private void appendLine(float x1, float y1, float z1, float x2, float y2, float z2,
                            final float width) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2;

        vertexData[offset++] = x1;
        vertexData[offset++] = y1;
        vertexData[offset++] = z1;

        vertexData[offset++] = x2;
        vertexData[offset++] = y2;
        vertexData[offset++] = z2;

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(width);
                glDrawArrays(GL_LINES, startVertex, numVertices);
            }
        });
    }

    private void appendLine(float x1, float y1, float z1, float x2, float y2, float z2,
                            final float width, int listIndex) {
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2;

        vertexData[offset++] = x1;
        vertexData[offset++] = y1;
        vertexData[offset++] = z1;

        vertexData[offset++] = x2;
        vertexData[offset++] = y2;
        vertexData[offset++] = z2;

        drawLists.get(listIndex).add(new DrawCommand() {
            @Override
            public void draw() {
                glLineWidth(width);
                glDrawArrays(GL_LINES, startVertex, numVertices);
            }
        });
    }

    private void appendForce(Point point, float factor, float lengthSideLines) {
        Conditions.Force nodalForce = point.getConditions().getForce();

        appendForce(point.getX(), point.getY(), point.getZ(), nodalForce.getForceX(),
                nodalForce.getForceY(), nodalForce.getForceZ(), factor, lengthSideLines);
    }

    private void appendForce(Line line, float factor, float unitLengthNDC, float lengthSideLines,
                             int numberOfForces) {
        Conditions.Force force = line.getConditions().getForce();
        float delta, deltaX, deltaY, x, y, z;

        float alpha = (float)Math.atan2(force.getForceY(), force.getForceX());
        float beta  = (float)Math.atan2(line.getLastPoint().getY() - line.getFirstPoint().getY(),
                line.getLastPoint().getX() - line.getFirstPoint().getX());
        float gamma = beta - alpha;

        if (0.1 < Math.abs(Math.sin(gamma))) {
            delta = (Constants.PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE/unitLengthNDC)/
                    (float)Math.abs(Math.sin(gamma));
        } else {
            delta = Constants.PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE/unitLengthNDC;
        }

        deltaX = delta*(float)Math.cos(beta);
        deltaY = delta*(float)Math.sin(beta);

        x = line.getFirstPoint().getX();
        y = line.getFirstPoint().getY();
        z = line.getFirstPoint().getZ();
        for (int i = 0; i < numberOfForces; i++) {
            appendForce(x, y, z, force.getForceX(), force.getForceY(), force.getForceZ(),
                    factor, lengthSideLines);
            x += deltaX;
            y += deltaY;
        }
    }

    private void appendForce(float x, float y, float z, float fx, float fy, float fz,
                             float factor, float lengthSideLines) {
        appendLine(x, y, z, x + fx*factor, y + fy*factor, z + fz*factor, 5f);

        float length = lengthSideLines;
        float f1, f2, a1, a2, b1, b2, magnitude;
        Geometry.Vector f, a, b, c, F;

        F = new Geometry.Vector(fx, fy, fz);
        f = new Geometry.Vector(fx, fy, fz);
        f.scale(0.8f);

        if (Math.abs(f.y) > (float)1e-10)
            a = new Geometry.Vector(1f, -f.x/f.y, 0f); // dotProduct(f,a) = 0
        else
            a = new Geometry.Vector(-f.y/f.x, 1f, 0f); // dotProduct(f,a) = 0
        a.normalize();
        a.scale(0.1f * F.length());

        b = new Geometry.Vector(f.x + a.x, f.y + a.y, f.z + a.z); // b = f + a

        c = new Geometry.Vector(b.x - F.x, b.y - F.y, b.z - F.z); // c = b - F
        c.normalize();
        c.scale(lengthSideLines);

        appendLine(
                x + fx * factor,
                y + fy * factor,
                z + fz * factor,
                x + fx * factor + c.x,
                y + fy * factor + c.y,
                z + fz * factor + c.z,
                5f
        );

        a.scale(-1f);
        b.setComponents(f.x + a.x, f.y + a.y, f.z + a.z); // b = f + a
        c.setComponents(b.x - F.x, b.y - F.y, b.z - F.z); // c = b - F
        c.normalize();
        c.scale(lengthSideLines);

        appendLine(
                x + fx * factor,
                y + fy * factor,
                z + fz * factor,
                x + fx * factor + c.x,
                y + fy * factor + c.y,
                z + fz * factor + c.z,
                5f
        );
    }

    private void appendSliceSurface(float startAngle,
                                    float endAngle,
                                    int[] rotatingVerticesIds,
                                    float[] vertices,
                                    char direction,
                                    float[] values,
                                    ColorPalette colorPalette) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;
        Geometry.Ray axis = new Geometry.Ray(new SimplePoint(), new Geometry.Vector());

        // Get the rotation axis
        final int firstId = rotatingVerticesIds[0];
        final int secondId = rotatingVerticesIds[rotatingVerticesIds.length-1];
        axis.point.setCoordinates(
                vertices[firstId * dim],
                vertices[firstId * dim + 1],
                0f
        );
        axis.vector.setComponents(
                vertices[secondId * dim] - vertices[firstId * dim],
                vertices[secondId * dim + 1] - vertices[firstId * dim + 1],
                0f
        );

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        // Append a triangle strip
        final int startVertex = offset / Constants.FLOATS_PER_COLOR_VERTEX;
        final int numVertices = 2*rotatingVerticesIds.length;
        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];

        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];
            v[2] = 0f;
            v[3] = 1f;

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            setValuesInColorPalette(rotatingVerticesIds[i], direction, dim, values, colorPalette);

            vertexData[offset++] = x[0];
            vertexData[offset++] = x[1];
            vertexData[offset++] = x[2];
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getBlue();
            vertexData[offset++] = colorPalette.getGreen();

            vertexData[offset++] = y[0];
            vertexData[offset++] = y[1];
            vertexData[offset++] = y[2];
            vertexData[offset++] = colorPalette.getRed();
            vertexData[offset++] = colorPalette.getBlue();
            vertexData[offset++] = colorPalette.getGreen();
        }

        drawLists.get(0).add(new DrawCommand() {
            @Override
            public void draw() {
                // Here we use GL_TRIANGLE_STRIP to do a triangle strip
                glDrawArrays(GL_TRIANGLE_STRIP, startVertex, numVertices);
            }
        });
    }

    private void setVerticesCoordinatesSlice(int numberOfSlice,
                                             int numberOfVerticesPerSlice,
                                             int propertiesPerVertex,
                                             VertexArray vertexArray,
                                             float startAngle,
                                             float endAngle,
                                             int[] rotatingVerticesIds,
                                             float[] vertices) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;
        Geometry.Ray axis = new Geometry.Ray(new SimplePoint(), new Geometry.Vector());

        // Get the rotation axis
        final int firstId = rotatingVerticesIds[0];
        final int secondId = rotatingVerticesIds[rotatingVerticesIds.length-1];
        axis.point.setCoordinates(
                vertices[firstId * dim],
                vertices[firstId * dim + 1],
                0f
        );
        axis.vector.setComponents(
                vertices[secondId * dim] - vertices[firstId * dim],
                vertices[secondId * dim + 1] - vertices[firstId * dim + 1],
                0f
        );

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];

        final int beginning = numberOfSlice*numberOfVerticesPerSlice*propertiesPerVertex;
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];
            v[2] = 0f;
            v[3] = 1f;

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            vertexArray.setValue(beginning + i*6*2 + 0, x[0]);
            vertexArray.setValue(beginning + i*6*2 + 1, x[1]);
            vertexArray.setValue(beginning + i*6*2 + 2, x[2]);
            vertexArray.setValue(beginning + i*6*2 + 6, y[0]);
            vertexArray.setValue(beginning + i*6*2 + 7, y[1]);
            vertexArray.setValue(beginning + i*6*2 + 8, y[2]);
        }
    }

    private void setVerticesValuesSlice(int numberOfSlice,
                                        int numberOfVerticesPerSlice,
                                        int propertiesPerVertex,
                                        VertexArray vertexArray,
                                        int[] rotatingVerticesIds,
                                        char direction,
                                        float[] values,
                                        ColorPalette colorPalette) {
        final int dim = Constants.PROBLEM_DIMENSION;

        final int beginning = numberOfSlice*numberOfVerticesPerSlice*propertiesPerVertex;
        for (int i = 0; i < rotatingVerticesIds.length; i++) {
            setValuesInColorPalette(rotatingVerticesIds[i], direction, dim, values, colorPalette);

            vertexArray.setValue(beginning + i*6*2 + 3, colorPalette.getRed());
            vertexArray.setValue(beginning + i*6*2 + 4, colorPalette.getGreen());
            vertexArray.setValue(beginning + i*6*2 + 5, colorPalette.getBlue());

            vertexArray.setValue(beginning + i*6*2 + 9,  colorPalette.getRed());
            vertexArray.setValue(beginning + i*6*2 + 10, colorPalette.getGreen());
            vertexArray.setValue(beginning + i*6*2 + 11, colorPalette.getBlue());
        }
    }

    private void setValuesInColorPalette(final int vertexId, final char direction, final int dim,
                                         float[] values, ColorPalette colorPalette) {
        switch (direction) {
            // Values in all directions
            case Constants.ALL_COMPONENTS:
                colorPalette.setValues(
                        values[vertexId*dim],
                        values[vertexId*dim + 1]
                );
                break;
            // Values in X
            case Constants.X_COMPONENT:
                colorPalette.setValues(values[vertexId*dim]);
                break;
            // Values in Y
            case Constants.Y_COMPONENT:
                colorPalette.setValues(values[vertexId*dim + 1]);
                break;
            case Constants.ONE_COMPONENT:
                colorPalette.setValues(values[vertexId]);
                break;
        }
    }

    private void appendMeshForSliceSurface(float startAngle,
                                           float endAngle,
                                           int[] rotatingVerticesIds,
                                           float[] vertices) {
        final float[] matFirstFace, matSecondFace;
        final int dim = Constants.PROBLEM_DIMENSION;
        Geometry.Ray axis = new Geometry.Ray(new SimplePoint(), new Geometry.Vector());

        // Get the rotation axis
        final int firstId = rotatingVerticesIds[0];
        final int secondId = rotatingVerticesIds[rotatingVerticesIds.length-1];
        axis.point.setCoordinates(
                vertices[firstId * dim],
                vertices[firstId * dim + 1],
                0f
        );
        axis.vector.setComponents(
                vertices[secondId * dim] - vertices[firstId * dim],
                vertices[secondId * dim + 1] - vertices[firstId * dim + 1],
                0f
        );

        // Matrices for the first and second faces of the slice
        matFirstFace = getMatrixToRotateAroundArbitraryAxis(startAngle, axis);
        matSecondFace = getMatrixToRotateAroundArbitraryAxis(endAngle, axis);

        // Append a triangle strip
        final int startVertex = offset / Constants.FLOATS_PER_VERTEX;
        final int numVertices = 2*rotatingVerticesIds.length;
        float[] v = new float[4];
        float[] x = new float[4];
        float[] y = new float[4];
        float[] z = new float[4];

        // First line
        v[0] = vertices[rotatingVerticesIds[0]*dim];
        v[1] = vertices[rotatingVerticesIds[0]*dim+1];
        v[2] = 0f;
        v[3] = 1f;

        multiplySpecialMv(x, matFirstFace, v);
        multiplySpecialMv(y, matSecondFace, v);

        appendLine(x[0], x[1], x[2], y[0], y[1], y[2], 5f);
        for (int j = 0; j < 4; j++) z[j] = x[j];

        for (int i = 1; i < rotatingVerticesIds.length; i++) {
            v[0] = vertices[rotatingVerticesIds[i]*dim];
            v[1] = vertices[rotatingVerticesIds[i]*dim+1];

            multiplySpecialMv(x, matFirstFace, v);
            multiplySpecialMv(y, matSecondFace, v);

            appendLine(z[0], z[1], z[2], x[0], x[1], x[2], 5f);
            appendLine(x[0], x[1], x[2], y[0], y[1], y[2], 5f);

            for (int j = 0; j < 4; j++) z[j] = x[j];
        }
    }

    private float[] getMatrixToRotateAroundArbitraryAxis(float angle, Geometry.Ray axis) {
        float[] m = new float[16];
        float length = axis.vector.length();

        float a = axis.point.x;
        float b = axis.point.y;
        float c = axis.point.z;

        // Normalize the axis' vector
        float u = axis.vector.x / length;
        float v = axis.vector.y / length;
        float w = axis.vector.z / length;

        // Set some intermediate values.
        float u2 = u*u;
        float v2 = v*v;
        float w2 = w*w;
        float cosT = (float)Math.cos(angle);
        float oneMinusCosT = 1f - cosT;
        float sinT = (float)Math.sin(angle);

        // Build the matrix entries element by element.
        m[0] = u2 + (v2 + w2) * cosT;
        m[1] = u*v * oneMinusCosT - w*sinT;
        m[2] = u*w * oneMinusCosT + v*sinT;
        m[3] = (a*(v2 + w2) - u*(b*v + c*w))*oneMinusCosT + (b*w - c*v)*sinT;

        m[4] = u*v * oneMinusCosT + w*sinT;
        m[5] = v2 + (u2 + w2) * cosT;
        m[6] = v*w * oneMinusCosT - u*sinT;
        m[7] = (b*(u2 + w2) - v*(a*u + c*w))*oneMinusCosT + (c*u - a*w)*sinT;

        m[8]  = u*w * oneMinusCosT - v*sinT;
        m[9]  = v*w * oneMinusCosT + u*sinT;
        m[10] = w2 + (u2 + v2) * cosT;
        m[11] = (c*(u2 + v2) - w*(a*u + b*v))*oneMinusCosT + (a*v - b*u)*sinT;

        m[12] = 0f;
        m[13] = 0f;
        m[14] = 0f;
        m[15] = 1f;

        return m;
    }

    private void multiplySpecialMv(float[] x, float[] M, float[] v) {
        x[0] = x[1] = x[2] = x[3] = 0f;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++) x[i] += M[i*4+j]*v[j];
    }

    private GeneratedData build() {
        GeneratedData generatedData = new GeneratedData(vertexData, drawLists.get(0));
        for (int i = 1; i < drawLists.size(); i++) generatedData.addDrawList(drawLists.get(i));
        return generatedData;
    }
}