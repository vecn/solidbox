package com.android.airhockey1.objects;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.Surfaces;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.programs.VaryingColorShaderProgram;
import com.android.airhockey1.util.Results;
import com.android.airhockey1.util.Results.ColorPalette;

import java.util.List;

/**
 * Created by ernesto on 12/02/16.
 */
public class VisualDisplacements {
    private final int STRIDE =
            (Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT) *
                    Constants.BYTES_PER_FLOAT;
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private int size;
    boolean isCreated;

    // To know the displacement's direction to be plotted: X, Y or A (all directions)
    private char direction;

    public VisualDisplacements(char direction) {
        size = 0;
        vertexArray = null;
        drawList = null;
        this.direction = direction;
        isCreated = false;
    }

    public VisualDisplacements(float[]vertices,
                               int[] connMtx,
                               float[] displacements,
                               ColorPalette colorPalette,
                               char direction) {
        size = vertices.length;
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createVisualDisplacements(vertices, connMtx, displacements,
                        colorPalette, direction);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        this.direction = direction;
        isCreated = true;
    }

    public void updateOpenGLData(float[] vertices,
                                 int[] connMtx,
                                 float[] displacements,
                                 ColorPalette colorPalette) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createVisualDisplacements(vertices, connMtx, displacements,
                        colorPalette, direction);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
        isCreated = true;
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        if (null != vertexArray) {
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (null != drawList) {
            for (ObjectBuilder.DrawCommand drawCommand : drawList) {
                drawCommand.draw();
            }
        }
    }

    public boolean hasBeenCreated(){
        return isCreated;
    }
}
