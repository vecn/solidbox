package com.android.airhockey1.objects;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.objects.ObjectBuilder.GeneratedData;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.util.Geometry;

import java.util.List;

/**
 * Created by ernesto on 2/10/16.
 */
public class AxisOrientation {
    private float x, y, z, radius;
    private final VertexArray vertexArray;
    private final List<ObjectBuilder.DrawCommand> drawList;

    public AxisOrientation(float centerX, float centerY, float width, float height, float scaleOnX,
                           float scaleOnY) {
        GeneratedData generatedData = ObjectBuilder.createAxisOrientation(centerX, centerY,
                width, height, scaleOnX, scaleOnY);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                Constants.POSITION_COMPONENT_COUNT,
                0);
    }

    // Draws everything in the draw list
    public void draw() {
        for (ObjectBuilder.DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }
}
