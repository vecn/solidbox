package com.android.airhockey1.objects;

/**
 * Created by ernesto on 12/01/16.
 */

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.objects.ObjectBuilder.GeneratedData;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.util.Geometry;

import java.util.List;

public class VisualCircle {

    private float x, y, z, radius;
    private final VertexArray vertexArray;
    private final List<ObjectBuilder.DrawCommand> drawList;

    public VisualCircle(float radius, int numPoints) {
        GeneratedData generatedData = ObjectBuilder.createVisualNode(
                new Geometry.SimplePoint(0f, 0f, 0f),
                radius,
                numPoints);

        this.x = 0f;
        this.y = 0f;
        this.z = 0f;
        this.radius = radius;

        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(
                0,
                colorProgram.getPositionAttributeLocation(),
                Constants.POSITION_COMPONENT_COUNT,
                0);
    }

    // Draws everything in the draw list
    public void draw() {
        for (ObjectBuilder.DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }

}
