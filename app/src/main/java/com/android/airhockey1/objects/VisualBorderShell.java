package com.android.airhockey1.objects;

import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.programs.VaryingColorShaderProgram;
import com.android.airhockey1.util.Results;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ernesto on 24/02/16.
 */
public class VisualBorderShell {
    private int STRIDE;
    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;
    private List<ObjectBuilder.DrawCommand> drawListBorder;
    ObjectBuilder.GeneratedData generatedData;
    private int numberOfElements;
    private int propertiesPerVertex;
    private boolean meshSet;
    private boolean valuesSet;
    private boolean drawListCreated;
    private boolean working;
    private float z1, z2;

    public VisualBorderShell() {
        vertexArray = null;
        drawList = new ArrayList();
        numberOfElements = 0;
        propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
        STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
        meshSet = false;
        valuesSet = false;
        drawListCreated = false;
        working = false;
        z1 = 0f;
        z2 = 0f;
    }

    public void setNumberOfElements(int numberOfElements, float z1, float z2) {
        if (numberOfElements <= 0) return;
        this.numberOfElements = numberOfElements;
        // Space to save the data for OpenGL
        vertexArray = new VertexArray(
                numberOfElements*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT*propertiesPerVertex);

        this.z1 = z1;
        this.z2 = z2;
    }

    public void setMesh(float[] vertices,
                        int[] connEdges) {

        working = true;

        int numberOfEdges, nPreviousVertices, beginIndex;
        float x1, y1, x2, y2;
        final int beginAttributeIndex = 0;
        final int dimension = Constants.PROBLEM_DIMENSION;

        numberOfEdges = connEdges.length / 2;

        for (int i = 0; i < numberOfEdges; i++) {
            x1 = vertices[connEdges[i*2]*dimension];
            y1 = vertices[connEdges[i*2]*dimension+1];
            x2 = vertices[connEdges[i*2+1]*dimension];
            y2 = vertices[connEdges[i*2+1]*dimension+1];

            nPreviousVertices = 2*i*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;

            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 0);
            vertexArray.setValue(beginIndex + 0, x1);
            vertexArray.setValue(beginIndex + 1, y1);
            vertexArray.setValue(beginIndex + 2, z2);

            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 1);
            vertexArray.setValue(beginIndex + 0, x1);
            vertexArray.setValue(beginIndex + 1, y1);
            vertexArray.setValue(beginIndex + 2, z1);

            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 2);
            vertexArray.setValue(beginIndex + 0, x2);
            vertexArray.setValue(beginIndex + 1, y2);
            vertexArray.setValue(beginIndex + 2, z1);


            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 3);
            vertexArray.setValue(beginIndex + 0, x2);
            vertexArray.setValue(beginIndex + 1, y2);
            vertexArray.setValue(beginIndex + 2, z1);

            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 4);
            vertexArray.setValue(beginIndex + 0, x2);
            vertexArray.setValue(beginIndex + 1, y2);
            vertexArray.setValue(beginIndex + 2, z2);

            beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + 5);
            vertexArray.setValue(beginIndex + 0, x1);
            vertexArray.setValue(beginIndex + 1, y1);
            vertexArray.setValue(beginIndex + 2, z2);
        }
        Log.i("Draw list", "Number of edges = " + numberOfEdges);

        meshSet = true;

        working = false;
    }

    public void setValues( char direction,
                           float[] values,
                           int[] connEdges,
                           Results.ColorPalette colorPalette) {
        working = true;

        final int numberOfEdges = connEdges.length / 2;
        for (int i = 0; i < numberOfEdges; i++) {

            changeColor(connEdges[i*2], i, 0, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);

            changeColor(connEdges[i*2], i, 1, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);

            changeColor(connEdges[i*2+1], i, 2, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);

            changeColor(connEdges[i*2+1], i, 3, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);

            changeColor(connEdges[i*2+1], i, 4, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);

            changeColor(connEdges[i*2], i, 5, values, colorPalette, direction,
                    Constants.POSITION_COMPONENT_COUNT, propertiesPerVertex, vertexArray);
        }
        Log.i("Draw list", "Number of edges = " + numberOfEdges);

        valuesSet = true;

        //vertexArray.printVertexArray();

        if (meshSet && valuesSet && (!drawListCreated)) createDrawList();

        working = false;
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            // To read the color of each vertex
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    public void bindData(ColorShaderProgram colorProgram) {
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    colorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (drawList.size() == 0) return;
        if (working) return;
        for (ObjectBuilder.DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }

    public boolean hasBeenSet(){
        return meshSet && valuesSet;
    }

    private void changeColor(int indexNode,
                             int edge,
                             int localVertex,
                             float[] values,
                             Results.ColorPalette colorPalette,
                             char direction,
                             int beginAttributeIndex,
                             int propertiesPerVertex,
                             VertexArray vertexArray) {
        final int dimension = Constants.PROBLEM_DIMENSION;
        int nPreviousVertices, beginIndex;

        switch (direction) {
            // Values in all directions
            case Constants.ALL_COMPONENTS:
                colorPalette.setValues(values[indexNode*dimension], values[indexNode*dimension + 1]);
                break;
            // Values in X
            case Constants.X_COMPONENT:
                colorPalette.setValues(values[indexNode*dimension]);
                break;
            // Values in Y
            case Constants.Y_COMPONENT:
                colorPalette.setValues(values[indexNode*dimension + 1]);
                break;
            case Constants.ONE_COMPONENT:
                colorPalette.setValues(values[indexNode]);
                break;
        }

        nPreviousVertices = 2*edge*Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        beginIndex = beginAttributeIndex + propertiesPerVertex*(nPreviousVertices + localVertex);
        vertexArray.setValue(beginIndex + 0, colorPalette.getRed());
        vertexArray.setValue(beginIndex + 1, colorPalette.getGreen());
        vertexArray.setValue(beginIndex + 2, colorPalette.getBlue());
    }

    private void createDrawList() {
        int startVertex = 0;
        drawList.clear();
        final int numberOfEdges = numberOfElements/2;
        for (int i = 0; i < numberOfEdges; i++) {
            ObjectBuilder.appendTriangle(startVertex, drawList);
            startVertex += Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
            ObjectBuilder.appendTriangle(startVertex,drawList);
            startVertex += Constants.NUMBER_OF_VERTICES_TO_DRAW_ELEMENT;
        }
        drawListCreated = true;
        Log.i("Draw list", "Number of edges = " + numberOfEdges);
    }
}
