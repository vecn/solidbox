package com.android.airhockey1.objects;

/**
 * Created by ernesto on 6/01/16.
 */
import java.util.List;

import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.objects.ObjectBuilder.DrawCommand;
import com.android.airhockey1.objects.ObjectBuilder.GeneratedData;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.util.Geometry.Cylinder;
import com.android.airhockey1.util.Geometry.SimplePoint;

public class Puck {
    private static final int POSITION_COMPONENT_COUNT = 3;

    public final float radius, height;

    private final VertexArray vertexArray;
    private final List<DrawCommand> drawList;

    public Puck(float radius, float height, int numPointsAroundPuck) {
        GeneratedData generatedData = ObjectBuilder.createPuck(new Cylinder(
                new SimplePoint(0f, 0f, 0f), radius, height), numPointsAroundPuck);
        this.radius = radius;
        this.height = height;

        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(0,
                colorProgram.getPositionAttributeLocation(),
                POSITION_COMPONENT_COUNT, 0);
    }

    // Draw everything in the draw list
    public void draw() {
        for (DrawCommand drawCommand : drawList) {
            drawCommand.draw();
        }
    }
}
