package com.android.airhockey1.objects;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.Lines;
import com.android.airhockey1.data.Nodes;
import com.android.airhockey1.data.Points;
import com.android.airhockey1.data.VertexArray;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.programs.VaryingColorShaderProgram;
import com.android.airhockey1.util.ColorScale;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Results;

import java.util.List;

import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 2/03/16.
 */
public class VisualObject {

    private VertexArray vertexArray;
    private List<ObjectBuilder.DrawCommand> drawList;

    public VisualObject() {
        vertexArray = null;
        drawList = null;
    }

    public void createPartialFixedSupport(float nodeX, float nodeY, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createPartialFixedSupport(nodeX, nodeY, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createFixedSupport(float nodeX, float nodeY, float base) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createFixedSupport(nodeX, nodeY, base);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createColorScale(ColorScale colorScale) {
        ObjectBuilder.GeneratedData generatedData
                = ObjectBuilder.createColorScale(colorScale);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createSectionLines(Geometry.Surface surface, MeshResults meshResults) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createSectionLines(surface, meshResults);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createSolidOfRevolution(MeshResults meshResults, int axisIdOnModel, int numberOfSlices,
                                        int typeOfSurfaceResult, Results.ColorPalette colorPalette) {
        ObjectBuilder.GeneratedData generatedData =
               ObjectBuilder.createSolidOfRevolution(meshResults, axisIdOnModel, numberOfSlices,
                       typeOfSurfaceResult, colorPalette);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    /*
    public void setVerticesCoordinatesSolidOfRevolution(MeshResults meshResults, int axisIdOnModel,
                                                           int numberOfSlices){
        ObjectBuilder.setVerticesCoordinatesSolidOfRevolution(vertexArray, meshResults,
                axisIdOnModel, numberOfSlices);
    }
    */

    public void setVerticesValuesSolidOfRevolution() {}

    public void createMeshForSolidOfRevolution(MeshResults meshResults, int axisIdOnModel, int numberOfSlices) {
        ObjectBuilder.GeneratedData generatedData =
                ObjectBuilder.createMeshForSolidOfRevolution(meshResults, axisIdOnModel, numberOfSlices);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createForces(Points points, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(points, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void updateForces(Points points, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(points, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void createForces(Lines lines, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(lines, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    public void updateForces(Lines lines, float unitLengthNDC, float maxForce) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createForces(lines, unitLengthNDC, maxForce);
        vertexArray = new VertexArray(generatedData.vertexData);
        drawList = generatedData.getDrawList(0);
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(ColorShaderProgram colorProgram) {
        if (null != vertexArray) {
            vertexArray.setVertexAttribPointer(
                    0,
                    colorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    0);
        }
    }

    // Tells to OpenGL how and where has to start to read the data (coordinates, color, etc)
    public void bindData(VaryingColorShaderProgram varyingColorProgram) {
        int propertiesPerVertex = Constants.POSITION_COMPONENT_COUNT + Constants.COLOR_COMPONENT_COUNT;
        int STRIDE = propertiesPerVertex * Constants.BYTES_PER_FLOAT;
        if (null != vertexArray) {
            // To read the position
            vertexArray.setVertexAttribPointer(
                    0,
                    varyingColorProgram.getPositionAttributeLocation(),
                    Constants.POSITION_COMPONENT_COUNT,
                    STRIDE);
            // To read the color of each vertex
            vertexArray.setVertexAttribPointer(
                    Constants.POSITION_COMPONENT_COUNT,
                    varyingColorProgram.getColorAttributeLocation(),
                    Constants.COLOR_COMPONENT_COUNT,
                    STRIDE);
        }
    }

    // Draws everything in the draw list
    public void draw() {
        if (null != drawList) {
            for (ObjectBuilder.DrawCommand drawCommand : drawList) {
                drawCommand.draw();
            }
        }
    }
}
