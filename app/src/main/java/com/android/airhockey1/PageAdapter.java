package com.android.airhockey1;

/**
 * Created by ernesto on 26/01/16.
 * The view pager adapter handles the swipe tabs feature:
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    TabFragment1 tab1;
    TabFragment2 tab2;
    TabFragment3 tab3;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        tab1 = new TabFragment1();
        tab2 = new TabFragment2();
        tab3 = new TabFragment3();
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                //TabFragment1 tab1 = new TabFragment1();
                return tab1;
            case 1:
                //TabFragment2 tab2 = new TabFragment2();
                return tab2;
            case 2:
                //TabFragment3 tab3 = new TabFragment3();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
