package com.android.airhockey1;

/**
 * Created by ernesto on 5/01/16.
 */
public class Constants {
    public static final int BYTES_PER_FLOAT = 4;

    // The dimension in which we are solving the FEM's problem.
    // In OpenGL we are always are working in 3D.
    public static final int PROBLEM_DIMENSION = 2;

    // Types of actions that can be done in the CAD
    public static final int INITIAL_VALUE_INT = -2;
    public static final int DOING_NOTHING     = -1;
    public static final int DRAWING_A_NODE    = 0;
    public static final int DRAWING_A_LINE    = 1;
    public static final int DRAWING_A_SURFACE = 2;
    public static final int SELECTING_A_NODE_TO_APPLY_FORCE = 3;
    public static final int SELECTING_A_NODE_TO_FIX_DISPLACEMENT = 4;
    public static final int DRAGGING_A_NODE = 5;
    public static final int SELECTING_A_SURFACE_FOR_UNIFY_BOOLEAN_OPERATION = 6;
    public static final int SELECTING_A_SURFACE_FOR_INTERSECT_BOOLEAN_OPERATION = 7;
    public static final int SELECTING_A_SURFACE_FOR_SUBSTRACT_BOOLEAN_OPERATION = 8;
    public static final int SELECTING_A_SURFACE_FOR_DIFFERENCE_BOOLEAN_OPERATION = 9;
    public static final int SELECTING_A_SURFACE_FOR_COMBINE_BOOLEAN_OPERATION = 10;
    public static final int DRAWING_A_RECTANGLE = 11;
    public static final int DRAWING_A_REGULAR_POLYGON = 12;
    public static final int DRAWING_A_CIRCLE = 13;
    public static final int PANNING = 14;
    public static final int ZOOMING = 15;
    public static final int DRAGGING_SOMETHING = 16;
    public static final int SPLITTING_SEGMENT = 17;
    public static final int EXTRUDING_SEGMENT = 18;
    public static final int SELECTING_ROTATION_AXIS = 19;
    public static final int SELECTING_A_LINE_TO_APPLY_FORCE = 20;
    public static final int SELECTING_A_POINT_TO_APPLY_FORCE = 21;
    public static final int SELECTING_A_POINT_TO_FIX_DISPLACEMENT = 22;

    public static final int EVENT_PRESS = 0;
    public static final int EVENT_DRAG = 1;

    // Size of the nodes shown on the screen (given in the normalized device coordinates), so
    // when drawing the nodes, or something else, we have to affect this lengths by a scaling factor.
    public static final int VISUAL_NODE_POINT_NUMPOINTS = 25;
    public static final float VISUAL_NODE_RADIUS = 0.05f;
    public static final float VISUAL_POINT_RADIUS = 0.01f;
    public static final float VISUAL_NODE_FACTOR_SELECTION = 2f;

    // Size of the lines shown on the screen (normalized device coordinates)
    public static final float VISUAL_LINE_WIDTH = 0.1f;
    public static final float VISUAL_LINE_FACTOR_SELECTION = 1.0f;

    // Name of the temporary file used to save the data (can be when the app is paused, stopped, etc.)
    public static final String APP_DIRECTORY = "SolidBox";

    // Some values to determine whether the user wants to select something or drag something
    public static final float MAX_TAP_DISTANCE = 0.05f; // In normalize device coordinates
    public static final long  MAX_TAP_DURATION = 150;  // Milliseconds

    // Number of floats to define the location and color of a vertex
    // (when the color changes at each vertex, for OpenGL)
    public static final int FLOATS_PER_COLOR_VERTEX = 6;

    // Number of floats to define the location
    // (in this case the same color is used for all the vertices, for OpenGL)
    public static final int FLOATS_PER_VERTEX = 3;

    // Number of components to describe the position and color of a vertex
    public static final int POSITION_COMPONENT_COUNT = 3;
    public static final int COLOR_COMPONENT_COUNT = 3;

    // Length's side of the largest square drawn on the screen. The length is given in virtual units
    // (world-space).
    public static final int REFERENCE_NUMBER_OF_UNITS = 4;

    // Tics length given in the normalized device coordinates.
    public static final float MAIN_TICS_LENGTH = 0.05f;

    public static final float PERPENDICULAR_DISTANCE_REDUCTION = 0.05f;

    // Line width used for OpenGL to draw several objects
    public static final float LINE_WIDTH_VISUAL_MESH = 2f;
    public static final float LINE_WIDTH_NOT_SELECTED_VISUAL_LINE = 10f;
    public static final float LINE_WIDTH_SELECTED_VISUAL_LINE = 10f;
    public static final float LINE_WIDTH_VISUAL_AXIS = 5f;
    public static final float LINE_WIDTH_AXIS_ORIENTATION = 5f;
    public static final float LINE_WIDTH_GRID = 3f;

    // Types of surface results
    public static final int NO_SURFACE_RESULT = -1;
    public static final int DISPLACEMENTS = 0;
    public static final int DISPLACEMENTS_ON_X = 1;
    public static final int DISPLACEMENTS_ON_Y = 2;
    public static final int DEFORMATIONS = 3;
    public static final int VON_MISES = 4;

    // Components to be considered
    public static final char ALL_COMPONENTS = 'A';
    public static final char X_COMPONENT = 'X';
    public static final char Y_COMPONENT = 'Y';
    public static final char Z_COMPONENT = 'Z';
    public static final char ONE_COMPONENT = 'O'; // The points just have one component

    // Type of element
    public static final int ELEMENT_TYPE_TRIANGLE = 0;
    public static final int ELEMENT_TYPE_QUADRILATERAL = 1;

    public static final int NUMBER_OF_VERTICES_TO_DRAW_ELEMENT = 3;

    // In normalized device coordinates
    public static final float DISTANCE_FOR_PI_RAD_ANGLE = 1f;

    // Types of FEM's problem
    public static final int PLANE_STRESS = 0;
    public static final int PLANE_STRAIN = 1;
    public static final int SOLID_OF_REVOLUTION = 2;

    // For the text
    // Font size when is loaded from the file. If it is bigger we get a better resolution.
    public static final int FONT_SIZE = 40;
    // Font size shown on the screen (of course in normalized device coordinates)
    public static final float FONT_SIZE_ON_SCREEN = 0.06f;

    // Supports
    public static final float SUPPORT_TRIANGLE_BASE = 0.12f;

    // A node may be part of the following shapes (the nodes are used in these figures to control
    // something)
    public static final int CONTROL_NODE_FOR_CIRCLE = 0;

    public static final float COLOR_SCALE_LEFT_MARGIN = 0.1f;
    public static final float COLOR_SCALE_WIDTH = 0.1f;
    public static final float COLOR_SCALE_HEIGHT = 0.8f;
    public static final int COLOR_SCALE_DIVISONS_ON_X = 1;
    public static final int COLOR_SCALE_DIVISONS_ON_Y = 20;

    public static final float LENGTH_LARGEST_MAGNITUDE_FORCE = 0.4f;
    public static final float LENGTH_FORCES_SIDE_LINES = 0.08f;
    public static final float LENGTH_FORCES_SIDE_LINES_II = 0.05f;
    public static final float PERPENDICULAR_DISTANCE_LINES_IN_LINEAR_FORCE = 0.08f;

    public static final float PANNING_FACTOR = 1f;
    public static final float ZOOMING_FACTOR = 1f;
    public static final float MAXIMUM_ZOOM = 100f;
    public static final float MINIMUM_ZOOM = 0.001f;
    public static final float SMALLEST_DISTANCE = 0.001f;

    public static final int MAXIMUM_QUANTITY_OF_POINTERS = 10;

    // Gestures
    public static final int GESTURE_NONE = 0;
    public static final int GESTURE_TAP = 1;
    public static final int GESTURE_DOUBLE_TAP = 2;
    public static final int GESTURE_DRAGGING = 3;

    public static final int INVALID_ID = -1;

    public static final int CAD_UPDATE_INVALID = 0;
    public static final int CAD_UPDATE_LINES = 0;
    public static final int CAD_UPDATE_FORCES = 1;
    public static final int CAD_UPDATE_SURFACES = 2;
    public static final int CAD_UPDATE_AXIS = 3;

    public static final int BUTTON_SELECTED_INSERT_SUPPORT = 0;
    public static final int BUTTON_SELECTED_INSERT_NODAL_FORCE = 1;
    public static final int BUTTON_SELECTED_INSERT_LINEAR_FORCE = 2;
    public static final int BUTTON_SELECTED_INSERT_NODE = 3;
    public static final int BUTTON_SELECTED_INSERT_LINE = 4;
    public static final int BUTTON_SELECTED_INSERT_SURFACE = 5;

    public static final int TYPE_SHAPE_INVALID_VALUE = -1;
    public static final int TYPE_SHAPE_CIRCLE = 0;
    public static final int TYPE_SHAPE_RECTANGLE = 1;
    public static final int TYPE_SHAPE_REGULAR_POLYGON = 2;
}
