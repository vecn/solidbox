package com.android.airhockey1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.util.CustomViewPager;
import com.android.airhockey1.util.FileDialog;
import com.android.airhockey1.util.Geometry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Vector;

//public class AirHockeyActivity extends AppCompatActivity implements View.OnClickListener {
public class AirHockeyActivity extends AppCompatActivity
        implements TabFragment2.OnSolveButtonPressedListener {

    Toolbar toolbar;
    TabLayout tabLayout;
    CustomViewPager viewPager;
    PageAdapter adapter;
    boolean areTabsVisible;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_air_hockey);

        // To see an example of how to create tabs in an app follow the example in
        // http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/

        // We get the toolbar from the layout. We are using a toolbar instead of an action bar
        // (remember set <item name="windowActionBar">false</item> and
        // <item name="windowNoTitle">true</item> in she file styles.xml
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areTabsVisible) {
                    tabLayout.setVisibility(View.VISIBLE);
                    areTabsVisible = false;
                } else {
                    tabLayout.setVisibility(View.GONE);
                    areTabsVisible = true;
                }
            }
        });

        // Adds the tabs to the tabLayout
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CAD"));
        tabLayout.addTab(tabLayout.newTab().setText("Results"));
        //tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        areTabsVisible = true;
        tabLayout.setVisibility(View.GONE);

        // The viewPager handles the swipe tabs features
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setPagingEnabled(false);

        // Number of offscreen pages to keep in memory (this way the viewPager does not destroy
        // any of the three tabs)
        viewPager.setOffscreenPageLimit(2);

        adapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        // The viewPager is attached to a tab selected listener.
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //*
        // Check if the system supports OpenGL ES 2.0.
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        // Even though the latest emulator supports OpenGL ES 2.0,
        // it has a bug where it doesn't set the reqGlEsVersion so
        // the above check doesn't work. The below will detect if the
        // app is running on an emulator, and assume that it supports
        // OpenGL ES 2.0.
        final boolean supportsEs2 =
                configurationInfo.reqGlEsVersion >= 0x20000
                        || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                        && (Build.FINGERPRINT.startsWith("generic")
                        || Build.FINGERPRINT.startsWith("unknown")
                        || Build.MODEL.contains("google_sdk")
                        || Build.MODEL.contains("Emulator")
                        || Build.MODEL.contains("Android SDK built for x86")));

        if (!supportsEs2) {

            // This is where you could create an OpenGL ES 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2. Since
            // we're not doing anything, the app will crash if the device
            // doesn't support OpenGL ES 2.0. If we publish on the market, we
            // should also add the following to AndroidManifest.xml:
            //
            // <uses-feature android:glEsVersion="0x00020000"
            // android:required="true" />
            //
            // This hides our app from those devices which don't support OpenGL
            // ES 2.0.

            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            return;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_air_hockey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        AlertDialog dialog;
        View viewDialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;
        final int[] status = new int[1];

        switch (item.getItemId())
        {

            case R.id.menu_save:

                alertDialogBuilder = new AlertDialog.Builder(this);
                inflater = getLayoutInflater();

                // Inflate and set the layout for the dialog
                viewDialog = inflater.inflate(R.layout.save, null);
                alertDialogBuilder.setView(viewDialog);
                alertDialogBuilder.setMessage("Saving project");

                // The text fields and the checkboxes
                final EditText fileName = (EditText) viewDialog.findViewById(R.id.FileName);
                status[0] = -1;

                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (isExternalStorageWritable()) {
                            if (adapter.tab1.writeToFile(fileName.getText().toString())) status[0] = 0;
                            else                                                         status[0] = 1;
                        } else {
                            status[0] = 2;
                        }
                    }
                });

                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                return true;

            case R.id.menu_open:

                File mPath = TabFragment1.getStorageDir("/" + Constants.APP_DIRECTORY + "/");
                FileDialog fileDialog = new FileDialog(this, mPath, "");
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {
                        Log.i("FILES", "selected file " + file.toString());
                        if (!adapter.tab1.readFromFile(file.toString())) {
                            Log.i("FILES", "Could not read the file!!");
                        }
                    }
                });

                fileDialog.showDialog();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"), 1);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.i("FILES", "File Uri: " + uri.toString());
                    // Get the path
                    String path = getPath(this, uri);
                    Log.i("FILES", "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    //*/

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // This method wil be called when the create mesh button is pressed
    public void onSolveButtonPressed() {

        TabFragment1 geometryFragment = adapter.tab1;

        TabFragment2 resultsFragment = adapter.tab2;
        resultsFragment.setGeometry(geometryFragment.getGeometry());
        resultsFragment.setProblemData(geometryFragment.getProblemData());

        resultsFragment.createAndSolveProblem();
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
