package com.android.airhockey1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.Nodes;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.ProblemData;
import com.android.airhockey1.data.Surfaces;
import com.android.airhockey1.data.Tuple;
import com.android.airhockey1.util.Conditions;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.ModelHelper;
import com.android.airhockey1.util.Results.ColorPalette;
import com.android.airhockey1.util.Results.ColorPaletteRainbow;
import com.android.airhockey1.util.Results.ColorPaletteSunset;
import com.android.airhockey1.util.ResultsGLSurfaceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

import nb.geometricBot.Model;
import nb.geometricBot.ModelStatus;
import nb.pdeBot.BoundaryConditions;
import nb.pdeBot.Material;
import nb.pdeBot.finiteElement.MeshResults;
import nb.pdeBot.finiteElement.solidMechanics.Analysis2D;
import nb.pdeBot.finiteElement.solidMechanics.StaticElasticity2D;

/**
 * Created by ernesto on 26/01/16.
 */
public class TabFragment2 extends Fragment implements View.OnClickListener {

    OnSolveButtonPressedListener mCallback;

    private ResultsGLSurfaceView glSurfaceView;

    private GeometryData geometry;
    private Model model;
    private BoundaryConditions nodalFixedDisplacements;
    private BoundaryConditions nodalForces;
    private BoundaryConditions linearFixedDisplacements;
    private BoundaryConditions linerForces;
    private Material material;
    private Analysis2D analysis2D;
    MeshResults meshResults;
    ColorPalette colorPalette;

    Button buttonSolve;
    ImageButton buttonShowMesh;
    ImageButton buttonShowDisplacements;
    ImageButton buttonShowDisplacementsOnX;
    ImageButton buttonShowDisplacementsOnY;
    ImageButton buttonShowDeformations;
    ImageButton buttonClearRotation;

    boolean showMesh;
    int typeOfSurfaceResultsToShow;

    ProblemData problemData;

    PlotParameters plotParameters;
    float[] originalVertices;
    float displacementsScaleFactor;

    // Container Activity must implement this interface
    public interface OnSolveButtonPressedListener {
        void onSolveButtonPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
        //*
        if (glSurfaceView.rendererSet) {
            glSurfaceView.onPause();
        }
        //*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //*
        if (glSurfaceView.rendererSet) {
            glSurfaceView.onResume();
        }
        //*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented the callback interface.
        // If not, it throws an exception
        try {
            mCallback = (OnSolveButtonPressedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCreateMeshButtonPressed interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_2, container, false);

        // Initial plot parameters
        plotParameters = new PlotParameters();
        plotParameters.setZoomFactor(1f);
        plotParameters.setDepth(1f);
        plotParameters.setCenter(0f, 0f, 0f);

        problemData = null;

        // Creates the glSurfaceView
        glSurfaceView = (ResultsGLSurfaceView) view.findViewById(R.id.glSurfaceViewSecond);
        glSurfaceView.setParameters(plotParameters);

        buttonSolve    = (Button) view.findViewById(R.id.Solve);
        buttonShowMesh = (ImageButton) view.findViewById(R.id.ShowMesh);
        buttonShowDisplacements    = (ImageButton) view.findViewById(R.id.ShowDisplacements);
        buttonShowDisplacementsOnX = (ImageButton) view.findViewById(R.id.ShowDisplacementsOnX);
        buttonShowDisplacementsOnY = (ImageButton) view.findViewById(R.id.ShowDisplacementsOnY);
        buttonShowDeformations     = (ImageButton) view.findViewById(R.id.ShowDeformations);
        buttonClearRotation        = (ImageButton) view.findViewById(R.id.ClearRotation);

        showMesh = true;
        typeOfSurfaceResultsToShow = Constants.DISPLACEMENTS;

        buttonSolve.setOnClickListener(this);
        buttonShowMesh.setOnClickListener(this);
        buttonShowDisplacements.setOnClickListener(this);
        buttonShowDisplacementsOnX.setOnClickListener(this);
        buttonShowDisplacementsOnY.setOnClickListener(this);
        buttonShowDeformations.setOnClickListener(this);
        buttonClearRotation.setOnClickListener(this);

        geometry = null;
        model = null;
        nodalForces = null;
        linerForces = null;
        nodalFixedDisplacements = null;
        linearFixedDisplacements = null;
        material = null;
        analysis2D = null;
        meshResults = null;
        colorPalette = new ColorPaletteRainbow();
        //colorPalette = new ColorPaletteSunset();

        originalVertices = null;
        displacementsScaleFactor = 1f;

        // To enable menus for this fragment so they appear in the menu toolbar
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_results, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog dialog;
        final AlertDialog.Builder alertDialogBuilder;
        LayoutInflater inflater;
        View viewDialog;



        // Handles item selection
        switch (item.getItemId()) {

            case R.id.displacements_scale_factor:
                alertDialogBuilder = new AlertDialog.Builder(getActivity());
                //alertDialogBuilder.setMessage("Forces");
                inflater = getActivity().getLayoutInflater();
                // Inflate and set the layout for the dialog. Pass null as the parent view because
                // its going in the dialog layout
                viewDialog = inflater.inflate(R.layout.displacement_scale_factor_dialog, null);
                alertDialogBuilder.setView(viewDialog);
                // Buttons and text fields
                final EditText captureFactor =
                        (EditText) viewDialog.findViewById(R.id.DisplacementScaleFactor);
                // Ok button
                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Change the mesh coordinates with the new factor
                        displacementsScaleFactor = Float.parseFloat(captureFactor.getText().toString());
                        addDisplacementsOnTheMesh(displacementsScaleFactor, originalVertices, meshResults);
                        glSurfaceView.renderer.changeVerticesCoordinates(geometry.getSurface(0),
                                meshResults);
                    }
                });
                // Cancel button
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });

                dialog = alertDialogBuilder.create();
                dialog.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.Solve:
                mCallback.onSolveButtonPressed();
                Log.i("Clicking", "Mesh button");
                break;

            case R.id.ShowMesh:
                if (showMesh){
                    glSurfaceView.renderer.drawMesh();
                    showMesh = false;
                }
                else{
                    glSurfaceView.renderer.hideMesh();
                    showMesh = true;
                }
                break;

            case R.id.ShowDisplacements:
                glSurfaceView.renderer.setSurfaceResult(meshResults, colorPalette, Constants.DISPLACEMENTS);
                break;

            case R.id.ShowDisplacementsOnX:
                glSurfaceView.renderer.setSurfaceResult(meshResults, colorPalette, Constants.DISPLACEMENTS_ON_X);
                break;

            case R.id.ShowDisplacementsOnY:
                glSurfaceView.renderer.setSurfaceResult(meshResults, colorPalette, Constants.DISPLACEMENTS_ON_Y);
                break;

            case R.id.ShowDeformations:
                glSurfaceView.renderer.setSurfaceResult(meshResults, colorPalette, Constants.DEFORMATIONS);
                break;

            case R.id.ClearRotation:
                glSurfaceView.renderer.clearTransformations();
                break;
        }
    }

    public void setGeometry(GeometryData geometry) {
        Log.i("Mesh", "Setting the geometry");
        this.geometry = geometry;
    }

    public void setProblemData(ProblemData problemData) {
        this.problemData = problemData;
    }

    public void createAndSolveProblem() {
        constructModel();
        constructBoundaryConditions();
        defineMaterial();
        defineProblemData();
        checkModel();
        solve();
        findPointsIdsOnTheMesh();
        //findContourEdgesOnTheMesh();
        savesCopyOfVertices();
        addDisplacementsOnTheMesh(displacementsScaleFactor, originalVertices, meshResults);
        centerMesh(model, meshResults);
        drawInitialResults();
    }

    private void constructModel() {
        Surfaces surfaces = geometry.getSurfaces();

        if (surfaces.getSize() <= 0){
            model = null;
            return;
        }

        if (surfaces.getSize() > 1)
            Toast.makeText(getActivity(), "Careful! You have more than one surface.", Toast.LENGTH_SHORT).show();

        //model = createModelFromSurface(surfaces.getElement(0));
        model = surfaces.getElement(0).getModel();
        ModelHelper.printModel("-------> Model", "Solve", model);
    }

    /*
    // Creates the problem's geometry
    public static Model createModelFromSurface(Geometry.Surface surface) {
        int numberOfVertices, id1, id2;
        int[] connEdges;
        float[] coordinates;
        float[] holes = null;
        Geometry.Line line;
        Model model;
        Vector<Geometry.Node> nodes;

        // New empty model
        model = new Model();

        // Get the size (number of vertices)
        numberOfVertices = surface.getSize();

        // Nodes in order clockwise or counterclockwise
        nodes = surface.getSurfaceNodes();

        // Memory
        connEdges   = new int[numberOfVertices*2];
        coordinates = new float[numberOfVertices*2];

        // Gets the coordinates and the connectivity
        for (int j = 0; j < numberOfVertices; j++) {
            // Connectivity
            connEdges[j*2]     = j;
            connEdges[j*2 + 1] = j + 1;
            // Coordinates
            coordinates[j*2]    = nodes.get(j).GetX();
            coordinates[j*2+ 1] = nodes.get(j).GetY();
        }
        // The last side
        connEdges[(numberOfVertices - 1)*2]       = numberOfVertices - 1;
        connEdges[(numberOfVertices - 1)*2 + 1]   = 0;
        coordinates[(numberOfVertices - 1)*2]     = nodes.get(numberOfVertices-1).GetX();
        coordinates[(numberOfVertices - 1)*2 + 1] = nodes.get(numberOfVertices-1).GetY();

        model.setEdges(connEdges);
        model.setVertices(coordinates);
        model.setHoles(null);

        return model;
    }
    */

    public static int numberOfSurfacesInTheModel(Model model) {
        int edges[] = model.getEdgesRef();
        int[] aux = new int[edges.length];

        for (int i = 0; i < edges.length; i++) aux[i] = edges[i];

        int numberOfSurfaces = 0;
        int visitedVertices = 0;

        int i, vertex, lastVertex;
        boolean resetIndex;
        while (visitedVertices < edges.length) {
            i = 0;
            while (aux[i++] == -1); i--;
            vertex = aux[i]; aux[i] = -1;
            lastVertex = aux[i+1]; aux[i+1] = -1;
            visitedVertices += 2;
            resetIndex = false;
            while (vertex != lastVertex) {
                i++;
                if (resetIndex || (i == edges.length)) {
                    i = 0;
                    resetIndex = false;
                }
                if (aux[i] == vertex) {
                    if ((i%2) == 0) {
                        vertex = aux[i+1];
                        aux[i] = -1;
                        aux[i+1] = -1;
                    } else {
                        vertex = aux[i-1];
                        aux[i] = -1;
                        aux[i-1] = -1;
                    }
                    visitedVertices += 2;
                    resetIndex = true;
                }
            }
            numberOfSurfaces++;
        }
        Log.i("Counting surfaces", "Number of surfaces = " + numberOfSurfaces);
        return numberOfSurfaces;
    }

    public static int addSurfacesFromTheModel(Model model,
                                              Vector<Geometry.Line> lastAddedLines,
                                              Geometry.Surface[] surfaces) {
        int edges[] = model.getEdgesRef();
        int[] aux = new int[edges.length];
        Geometry.Surface surface;

        for (int i = 0; i < edges.length; i++) aux[i] = edges[i];

        int numberOfSurfaces = 0;
        int visitedVertices = 0;
        int countSurface = 0;

        int i, vertex, lastVertex;
        boolean resetIndex;

        Log.i("Valid surface", "--------------------------------");
        while (visitedVertices < edges.length) {
            surface = new Geometry.Surface();
            i = 0;
            while (aux[i++] == -1); i--;
            vertex = aux[i]; aux[i] = -1;
            lastVertex = aux[i+1]; aux[i+1] = -1;
            visitedVertices += 2;
            resetIndex = false;
            surface.addLine(lastAddedLines.get(i / 2));
            /*
            Log.i("Adding line", "" + lastAddedLines.get(i / 2).getFirstPoint().getId() +
                            "  " + lastAddedLines.get(i / 2).getFirstPoint().getX() +
                            "  " + lastAddedLines.get(i / 2).getFirstPoint().getY() +
                            "  " + lastAddedLines.get(i / 2).getLastPoint().getId() +
                            "  " + lastAddedLines.get(i / 2).getLastPoint().getX() +
                            "  " + lastAddedLines.get(i / 2).getLastPoint().getY()
            );
            */
            while (vertex != lastVertex) {
                i++;
                if (resetIndex || (i == edges.length)) {
                    i = 0;
                    resetIndex = false;
                }
                if (aux[i] == vertex) {
                    if ((i%2) == 0) {
                        vertex = aux[i+1];
                        aux[i] = -1;
                        aux[i+1] = -1;
                        surface.addLine(lastAddedLines.get(i/2));
                        /*
                        Log.i("Adding line", "" + lastAddedLines.get(i/2).getFirstPoint().getId() +
                                        "  " + lastAddedLines.get(i/2).getFirstPoint().getX() +
                                        "  " + lastAddedLines.get(i/2).getFirstPoint().getY() +
                                        "  " + lastAddedLines.get(i/2).getLastPoint().getId() +
                                        "  " + lastAddedLines.get(i/2).getLastPoint().getX() +
                                        "  " + lastAddedLines.get(i/2).getLastPoint().getY()
                        );
                        */
                    } else {
                        vertex = aux[i-1];
                        aux[i] = -1;
                        aux[i-1] = -1;
                        surface.addLine(lastAddedLines.get((i-1)/2));
                        /*
                        Log.i("Adding line", "" + lastAddedLines.get((i-1)/2).getFirstPoint().getId() +
                                        "  " + lastAddedLines.get((i-1)/2).getFirstPoint().getX() +
                                        "  " + lastAddedLines.get((i-1)/2).getFirstPoint().getY() +
                                        "  " + lastAddedLines.get((i-1)/2).getLastPoint().getId() +
                                        "  " + lastAddedLines.get((i-1)/2).getLastPoint().getX() +
                                        "  " + lastAddedLines.get((i-1)/2).getLastPoint().getY()
                        );
                        */
                    }
                    visitedVertices += 2;
                    resetIndex = true;
                }
            }
            surfaces[countSurface] = surface;
            countSurface++;
            numberOfSurfaces++;
        }
        return numberOfSurfaces;
    }

    private void findPointsIdsOnTheMesh() {
        // IMPORTANT: in all places we are assuming that the model for FEM is built with the
        // first surface
        geometry.getSurface(0).setCorrespondingMeshIdsOnPoints(meshResults);
    }

    private void defineMaterial() {
        material = new Material(2.11e6, 0.3);
    }

    private void defineProblemData() {
        analysis2D = Analysis2D.PLANE_STRESS;
        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                analysis2D = Analysis2D.PLANE_STRESS;
                break;
            case Constants.PLANE_STRAIN:
                analysis2D = Analysis2D.PLANE_STRAIN;
                break;
            case Constants.SOLID_OF_REVOLUTION:
                analysis2D = Analysis2D.SOLID_OF_REVOLUTION;
                analysis2D.setRevolutionEdgeID(
                        geometry.getSurface(0).getEgeIdOnModel(problemData.solidOfRevolution.getAxis()));
                break;
        }
        analysis2D.setThickness(problemData.plate.getThickness());
    }

    // Creates the boundary conditions
    private void constructBoundaryConditions() {
        constructBoundaryConditionsDirichlet();
        constructBoundaryConditionsNeumann();
    }

    private void constructBoundaryConditionsNeumann() {
        nodalForces = getImposedNodalForces();
        linerForces = getImposedLinearForces();
        printBoundaryConditions("-------> Nodal forces", nodalForces);
        printBoundaryConditions("-------> Linear forces", linerForces);
    }

    private void constructBoundaryConditionsDirichlet() {
        nodalFixedDisplacements = getImposedNodalDisplacements();
        linearFixedDisplacements = getImposedLinearDisplacements();
        printBoundaryConditions("-------> Fixed nodal displacements", nodalFixedDisplacements);
        printBoundaryConditions("-------> Fixed linear displacements", linearFixedDisplacements);
    }

    private BoundaryConditions getImposedNodalDisplacements() {
        Geometry.Point point;
        Conditions.FixedDisplacement fixedDisplacement;
        BoundaryConditions imposedDisplacements = new BoundaryConditions();

        // Number of nodes with a fixed displacement
        int size = 0;
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions())
                if (point.getConditions().hasFixedDisplacement()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        int count = 0;
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasFixedDisplacement()) {
                    fixedDisplacement = point.getConditions().getFixedDisplacement();
                    if (fixedDisplacement.isFixedX() && fixedDisplacement.isFixedY()) {
                        ids[count] = point.getId();
                        dof[count] = 'a';
                        val[count * 2] = fixedDisplacement.getDisplacementX();
                        val[count * 2 + 1] = fixedDisplacement.getDisplacementY();
                    } else if (fixedDisplacement.isFixedX()) {
                        ids[count] = point.getId();
                        dof[count] = 'x';
                        val[count * 2] = fixedDisplacement.getDisplacementX();
                    } else {
                        ids[count] = point.getId();
                        dof[count] = 'y';
                        val[count * 2 + 1] = fixedDisplacement.getDisplacementY();
                    }
                    count++;
                }
            }
        }
        imposedDisplacements.set(ids, dof, val);

        return imposedDisplacements;
    }

    private BoundaryConditions getImposedLinearDisplacements (){
        // Missing: here construct the displacements imposed over lines...
        // ...
        // ...
        return new BoundaryConditions();
    }

    private BoundaryConditions getImposedNodalForces() {
        Geometry.Point point;
        Conditions.Force force;
        BoundaryConditions imposedForces = new BoundaryConditions();

        // Number of nodes with a fixed displacement
        int size = 0;
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions())
                if(point.getConditions().hasAppliedForce()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        int count = 0;
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasAppliedForce()) {
                    force = point.getConditions().getForce();
                    ids[count] = point.getId();
                    // Always two forces are specified (some of them may be zero)
                    dof[count] = 'a';
                    val[count * 2] = force.getForceX();
                    val[count * 2 + 1] = force.getForceY();
                    count++;
                }
            }
        }
        imposedForces.set(ids, dof, val);

        return imposedForces;
    }

    private BoundaryConditions getImposedLinearForces() {
        Geometry.Line line;
        Conditions.Force force;
        BoundaryConditions imposedForces = new BoundaryConditions();

        // Number of nodes with a fixed displacement
        int size = 0;
        for (int i = 0; i < geometry.numberOfLines(); i++) {
            line = geometry.getLine(i);
            if (line.hasConditions())
                if(line.getConditions().hasAppliedForce()) size++;
        }

        int ids[] = new int[size];
        char dof[] = new char[size];
        double val[] = new double[size*2];

        int count = 0;
        for (int i = 0; i < geometry.numberOfLines(); i++) {
            line = geometry.getLine(i);
            if (line.hasConditions()) {
                if (line.getConditions().hasAppliedForce()) {
                    force = line.getConditions().getForce();
                    ids[count] = line.getId();
                    // Always two forces are specified (some of them may be zero)
                    dof[count] = 'a';
                    val[count * 2] = force.getForceX();
                    val[count * 2 + 1] = force.getForceY();
                    count++;
                }
            }
        }
        imposedForces.set(ids, dof, val);

        return imposedForces;
    }

    private void solve() {
        // Solve the FEM problem
        meshResults = StaticElasticity2D.solve(model, material, analysis2D,
                nodalFixedDisplacements, nodalForces,
                linearFixedDisplacements, linerForces,
                problemData.getNumberOfMeshNodes());
        //updateResults.setAllTrue();
    }

    private void savesCopyOfVertices() {
        float[] vertices = meshResults.getVerticesRef();
        originalVertices = new float[vertices.length];
        for (int i = 0; i < vertices.length; i++) originalVertices[i] = vertices[i];
    }

    private void drawInitialResults(){
        // Set the mesh and the surface result
        glSurfaceView.renderer.setInitialSurfaceResult(meshResults, colorPalette, problemData,
                geometry.getSurface(0), typeOfSurfaceResultsToShow);
        if (showMesh){
            glSurfaceView.renderer.drawMesh();
            showMesh = false;
        }
    }

    private void printBoundaryConditions(String name, BoundaryConditions boundaryConditions) {
        int[] ids = boundaryConditions.getIdsRef();
        char[] dofs = boundaryConditions.getDofRef();
        double[] values = boundaryConditions.getValuesRef();
        Log.i("Data", name);
        Log.i("Data", "Size = " + boundaryConditions.getN());
        for (int i = 0; i < boundaryConditions.getN(); i++){
            switch (dofs[i]){
                case 'a':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2] + "  " + values[i*2+1]);
                    break;
                case 'x':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2]);
                    break;
                case 'y':
                    Log.i("Data", "" + ids[i] + "  " + dofs[i] + "  " + values[i*2+1]);
                    break;
            }
        }
    }

    private void addDisplacementsOnTheMesh(float displacementScaleFactor,
                                           float[] originalVertices,
                                           MeshResults meshResults) {
        int numberOfVertices = meshResults.getNVertices();
        float[] displacements = meshResults.getDisplacementRef();
        float[] vertices = meshResults.getVerticesRef();

        for (int i = 0; i < vertices.length; i++)
            vertices[i] = originalVertices[i] + displacementScaleFactor*displacements[i];
    }

    private void centerMesh(Model model, MeshResults meshResults) {
        final int dim = Constants.PROBLEM_DIMENSION;
        float centerX = Geometry.Surface.calculateAverageX(model.getVerticesRef());
        float centerY = Geometry.Surface.calculateAverageY(model.getVerticesRef());
        float[] vertices = meshResults.getVerticesRef();
        int numOfMeshVertices = vertices.length / dim;

        for (int i = 0; i < numOfMeshVertices; i++) {
            vertices[i*dim+0] -= centerX;
            vertices[i*dim+1] -= centerY;
        }
    }

    private void findContourEdgesOnTheMesh() {
        final float tol = (float) 1e-10;
        final int dim = Constants.PROBLEM_DIMENSION;
        float[] verticesR = meshResults.getVerticesRef();
        int[] connEdgesR = meshResults.getConnEdgesRef();
        int[] contourEdgesR = meshResults.getContourEdgesRef();
        List<Integer> contourEdgesIdsR = new ArrayList();
        float[] verticesM = model.getVerticesRef();
        int[] connEdgesM = model.getEdgesRef();
        float distance, smallestDistance;
        Geometry.SimplePoint point = new Geometry.SimplePoint();
        Geometry.Ray ray = new Geometry.Ray(new Geometry.SimplePoint(), new Geometry.Vector());

        // Calculates the perpendicular distance to each model edge to get the mesh edges on the
        // contour
        for (int i = 0; i < meshResults.getNEdges(); i++) {
            point.setCoordinates(
                    0.5f*(verticesR[connEdgesR[i*2]*dim] + verticesR[connEdgesR[i*2+1]*dim]),
                    0.5f*(verticesR[connEdgesR[i*2]*dim+1] + verticesR[connEdgesR[i*2+1]*dim+1]),
                    0f
            );

            for (int j = 0; j < model.getNEdges(); j++) {
                ray.point.setCoordinates(
                        verticesM[connEdgesM[j*2]*dim],
                        verticesM[connEdgesM[j*2]*dim+1],
                        0f
                );
                ray.vector.setComponents(
                        verticesM[connEdgesM[j*2+1]*dim] - verticesM[connEdgesM[j*2]*dim],
                        verticesM[connEdgesM[j*2+1]*dim+1] - verticesM[connEdgesM[j*2]*dim+1],
                        0f
                );

                distance = Geometry.distanceBetween(point, ray);
                if (distance < tol) {
                    contourEdgesIdsR.add(i);
                    break;
                }
            }
        }

        // Copy the contour lines' ids
        contourEdgesR = new int[contourEdgesIdsR.size()];
        for (int i = 0; i < contourEdgesIdsR.size(); i++) contourEdgesR[i] = contourEdgesIdsR.get(i);

        Log.i("Contour edges", "Number of edges = " + meshResults.getNEdges());
        Log.i("Contour edges", "Number of contour edges = " + contourEdgesIdsR.size());
        for (int i = 0; i < contourEdgesR.length; i++) {
            Log.i("Contour edges", "" + contourEdgesR[i]);
        }
    }

    void checkModel() {
        ModelStatus modelStatus = model.verify();
        Log.i("MODEL", "Status = " + modelStatus);
    }
}