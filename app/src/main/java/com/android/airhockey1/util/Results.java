package com.android.airhockey1.util;

import android.util.Log;

import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 12/02/16.
 */
public class Results {

    public static void printDisplacements(float[] displacements) {
        final int dimension = 2;
        int size;

        size = displacements.length / 2;

        for (int i = 0; i < size; i++) {
            Log.i("Displacements", "" +
                            displacements[i*dimension] + "  " +
                            displacements[i*dimension+1]
            );
        }
    }

    public static abstract class ColorPalette {
        protected float maxValue;
        protected float minValue;
        protected final float oneSixth = 1f/6f;
        protected float myValue;

        public ColorPalette() {
            minValue = maxValue = myValue = 0f;
        }

        public void setMinMaxValues(float minValue, float maxValue){
            this.minValue = minValue;
            this.maxValue = maxValue;
            if (Math.abs(maxValue - minValue) < 1e-10){
                this.minValue = 0f;
                this.maxValue = 1f;
            }
            //Log.i("COLOR SCALE", "Setting min and max values.");
        }

        protected void calculateMyValue(int nComponents, float[] values) {
            myValue = 0f;
            for (int i = 0; i < nComponents; i++) myValue += (values[i]*values[i]);
            myValue = (float) Math.sqrt(myValue);

            myValue = (myValue - minValue) / (maxValue - minValue);
        }

        protected void calculateMyValue(float value1, float value2) {
            myValue = (float) Math.sqrt(value1*value1 + value2*value2);

            myValue = (myValue - minValue) / (maxValue - minValue);
        }

        protected void calculateMyValue(float value) {
            myValue = (value - minValue) / (maxValue - minValue);
        }

        public float getMaxValue() { return maxValue; }
        public float getMinValue() {
            return minValue;
        }

        public abstract void setValues(float value1, float value2);
        public abstract void setValues(float value);
        public abstract float getRed();
        public abstract float getGreen();
        public abstract float getBlue();

    }

    public static class ColorPaletteA extends ColorPalette {

        public ColorPaletteA() {
            super();
        }

        public void setValues(int nComponents, float[] values) {
            calculateMyValue(nComponents, values);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }

        public void setValues(float value){ calculateMyValue(value); }

        public float getRed() {
            float color;

            if (myValue > 0.5f) color = 0f;
            else                color = 2f *(0.5f - myValue);

            return color;
        }


        public float getGreen() {
            float color;

            if      (myValue < oneSixth)        color = 0f;
            else if (myValue < 0.5f)            color = 3f * (myValue - oneSixth);
            else if (myValue < (1f - oneSixth)) color = 3f * (5f*oneSixth - myValue);
            else                                color = 0f;

            return color;
        }

        public float getBlue() {
            float color;

            if (myValue < 0.5f) color = 0f;
            else                color = 2f * (myValue - 0.5f);

            return color;
        }
    }

    public static class ColorPaletteRainbow extends ColorPalette {
        private final float[][] colors = {
                {0.00f,   0,   0, 128},
                {0.10f,   0,   0, 255},
                {0.20f,   0, 128, 255},
                {0.37f,   0, 255,  25},
                {0.50f,   0, 255,   0},
                {0.63f, 255, 255,   0},
                {0.80f, 255, 128,   0},
                {0.90f, 255,   0,   0},
                {1.00f, 100,   0,   0}
        };

        public ColorPaletteRainbow() {
            super();
            final float factor = 1f / 255f;
            for (int i = 0; i < colors.length; i++)
                for (int j = 1; j < colors[i].length; j++)
                    colors[i][j] *= factor;
        }

        private int locateLowerLimit(float x) {
            if (x > 1f) return colors.length - 2;
            if (x < 0f) return 0;
            int i = 0;
            while (i < (colors.length - 1)) {
                if ((x >= colors[i][0]) && (x <= colors[i+1][0])) break;
                i++;
            }
            if (i == (colors.length - 1)) i--;
            return i;
        }

        private float calculateColor(int colorIndex) {
            int i = locateLowerLimit(myValue);
            if (myValue > 1f) return colors[colors.length-1][colorIndex];
            if (myValue < 0f) return colors[0][colorIndex];
            return colors[i][colorIndex] + (colors[i+1][colorIndex] - colors[i][colorIndex]) /
                    (colors[i+1][0] - colors[i][0]) * (myValue - colors[i][0]);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }
        public void setValues(float value) {
            calculateMyValue(value);
        }

        public float getRed(){
            return calculateColor(1);
        }

        public float getGreen() {
            return calculateColor(2);
        }

        public float getBlue() {
            return calculateColor(3);
        }
    }

    public static class ColorPaletteSunset extends ColorPalette {
        private final float[][] colors = {
                {0.00f,   0,   0,   0},
                {0.15f,  20,   0, 100},
                {0.30f, 100,   0, 200},
                {0.80f, 220, 100,   0},
                {1.00f, 255, 255,   0}
        };

        public ColorPaletteSunset() {
            super();
            final float factor = 1f / 255f;
            for (int i = 0; i < colors.length; i++)
                for (int j = 1; j < colors[i].length; j++)
                    colors[i][j] *= factor;
        }

        private int locateLowerLimit(float x) {
            if (x > 1f) return colors.length - 2;
            if (x < 0f) return 0;
            int i = 0;
            while (i < (colors.length - 1)) {
                if ((x >= colors[i][0]) && (x <= colors[i+1][0])) break;
                i++;
            }
            if (i == (colors.length - 1)) i--;
            return i;
        }

        private float calculateColor(int colorIndex) {
            int i = locateLowerLimit(myValue);
            if (myValue > 1f) return colors[colors.length-1][colorIndex];
            if (myValue < 0f) return colors[0][colorIndex];
            return colors[i][colorIndex] + (colors[i+1][colorIndex] - colors[i][colorIndex]) /
                    (colors[i+1][0] - colors[i][0]) * (myValue - colors[i][0]);
        }

        public void setValues(float value1, float value2) {
            calculateMyValue(value1, value2);
        }
        public void setValues(float value) {
            calculateMyValue(value);
        }

        public float getRed(){
            return calculateColor(1);
        }

        public float getGreen() {
            return calculateColor(2);
        }

        public float getBlue() {
            return calculateColor(3);
        }
    }

    public static class ColorRGB {
        protected  float R, G, B;

        public ColorRGB(){
            R = G = B = 0f;
        }

        public void setRed(float value)   { R = value; }
        public void setGreen(float value) { G = value; }
        public void setBlue(float value)  { B = value; }

        public float getRed()   { return R; }
        public float getGreen() { return G; }
        public float getBlue()  { return B; }
    }

    public static class ColorRGBT extends ColorRGB {
        private float T;

        public ColorRGBT(){
            super();
            T = 1f;
        }

        public void setTransparency(float value) { T = value; }
        public float getTransparency() { return T; }
    }

    public static class ColorTriangle extends Geometry.Triangle {
        private ColorRGBT[] colorRGBT;

        public ColorTriangle() {
            super();
            colorRGBT = new ColorRGBT[numberOfEdges];
            for (int i = 0; i < numberOfEdges; i++) colorRGBT[i] = new ColorRGBT();
        }

        public void setColorRGBT(int vertex, float R, float G, float B, float T){
            if (vertex < 0) return;
            if (vertex > (numberOfEdges -1)) return;
            colorRGBT[vertex].setRed(R);
            colorRGBT[vertex].setGreen(G);
            colorRGBT[vertex].setBlue(B);
            colorRGBT[vertex].setTransparency(T);
        }

        public float getRed(int vertex) {
            if (vertex < 0) return 0f;
            if (vertex > (numberOfEdges -1)) return 0f;
            return colorRGBT[vertex].getRed();
        }

        public float getGreen(int vertex) {
            if (vertex < 0) return 0f;
            if (vertex > (numberOfEdges -1)) return 0f;
            return colorRGBT[vertex].getGreen();
        }

        public float getBlue(int vertex) {
            if (vertex < 0) return 0f;
            if (vertex > (numberOfEdges -1)) return 0f;
            return colorRGBT[vertex].getBlue();
        }

        public void printOpenGLData() {
            for (int i = 0; i < numberOfEdges; i++) {
                Log.i("Color triangle", "" + i + "  " +
                        vertices.get(i).x + "  " +
                        vertices.get(i).y + "  " +
                        vertices.get(i).z + "  " +
                        colorRGBT[i].getRed() + "  " +
                        colorRGBT[i].getGreen() + "  " +
                        colorRGBT[i].getBlue());
            }
        }
    }

    public static float smallestMagnitude(int dimension, float[] values) {
        float magnitude, smallestMagnitude;
        final int size = values.length / dimension;

        if (values.length < dimension) return 0f;

        smallestMagnitude = 0f;
        for (int i = 0; i < dimension; i++) smallestMagnitude += (values[i]*values[i]);
        smallestMagnitude = (float) Math.sqrt(smallestMagnitude);

        for (int i = 0; i < size; i++) {
            magnitude = 0f;
            for (int j = 0; j < dimension; j++)
                magnitude += (values[i*dimension+j]*values[i*dimension+j]);
            magnitude = (float) Math.sqrt(magnitude);
            if (magnitude < smallestMagnitude) smallestMagnitude = magnitude;
        }

        return smallestMagnitude;
    }

    public static float smallestValue(int dimension, float[] values, int relativeIndex) {
        float smallestValue;
        int size = values.length / dimension;

        if (relativeIndex > dimension - 1) return 0f;
        if (values.length < dimension) return 0f;

        smallestValue = values[relativeIndex];
        for (int i = 0; i < size; i++)
            if (smallestValue > values[i*dimension+relativeIndex])
                smallestValue = values[i*dimension+relativeIndex];

        return smallestValue;
    }

    public static float smallestValue(float[] values) {
        float smallestValue;
        if (values.length == 0) return 0f;

        smallestValue = values[0];
        for (int i = 0; i < values.length; i++)
            if (smallestValue > values[i]) smallestValue = values[i];

        return smallestValue;
    }

    public static float largestValue(float[] values) {
        float largestValue;
        if (values.length == 0) return 0f;

        largestValue = values[0];
        for (int i = 0; i < values.length; i++)
            if (largestValue < values[i]) largestValue = values[i];

        return largestValue;
    }

    public static float largestValue(int dimension, float[] values, int relativeIndex) {
        float largestValue;
        int size = values.length / dimension;

        if (relativeIndex > dimension - 1) return 0f;
        if (values.length < dimension) return 0f;

        largestValue = values[relativeIndex];
        for (int i = 0; i < size; i++)
            if (largestValue < values[i*dimension+relativeIndex])
                largestValue = values[i*dimension+relativeIndex];

        return largestValue;
    }

    public static float largestMagnitude(int dimension, float[] values) {
        float magnitude, largestMagnitude;
        final int size = values.length / dimension;

        largestMagnitude = 0f;
        if (values.length > dimension)
            for (int i = 0; i < dimension; i++) largestMagnitude += (values[i]*values[i]);
        largestMagnitude = (float) Math.sqrt(largestMagnitude);

        for (int i = 0; i < size; i++) {
            magnitude = 0f;
            for (int j = 0; j < dimension; j++) magnitude += (values[i*dimension+j]*values[i*dimension+j]);
            magnitude = (float) Math.sqrt(magnitude);
            if (magnitude > largestMagnitude) largestMagnitude = magnitude;
        }

        return largestMagnitude;
    }

    public static String getLabel(float number) {
        String label = Float.toString(number);
        if ((number < 1e-3) ||(number > 1000)) {
            // Use exponential notation
        }
        return label;
    }
}
