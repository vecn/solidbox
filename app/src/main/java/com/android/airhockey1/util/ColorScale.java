package com.android.airhockey1.util;

import com.android.airhockey1.Constants;

/**
 * Created by ernesto on 7/03/16.
 */
public class ColorScale {
    float width;
    float height;
    float centerX;
    float centerY;
    int numberOfDivisionsOnX;
    int numberOfDivisionsOnY;
    public Results.ColorPalette colorPalette;

    public ColorScale() {
        width = height = 0f;
        centerX = centerY = 0f;
        numberOfDivisionsOnX = numberOfDivisionsOnY = 0;
        colorPalette = null;
    }

    public void setColorPalette(Results.ColorPalette colorPalette){
        this.colorPalette = colorPalette;
    }

    public void setDimensions(float width, float height) {
        this.width = width;
        this.height = height;
    }

    public void setCenter(float centerX, float centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    // We'll create a mesh using triangle strips for OpenGL
    public void setNumberOfDivisions(int numberOfDivisionsOnX, int numberOfDivisionsOnY){
        this.numberOfDivisionsOnX = numberOfDivisionsOnX;
        this.numberOfDivisionsOnY = numberOfDivisionsOnY;
    }

    public float getWidth() { return width; }
    public float getHeight() { return height; }
    public float getCenterX() { return centerX; }
    public float getCenterY() { return centerY; }
    public int getNumberOfDivisionsOnX() { return numberOfDivisionsOnX; }
    public int getNumberOfDivisionsOnY() { return numberOfDivisionsOnY; }
    public float getMaxValue() { return colorPalette.getMaxValue(); }
    public float getMinValue() { return colorPalette.getMinValue(); }
}
