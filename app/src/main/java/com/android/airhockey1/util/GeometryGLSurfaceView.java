package com.android.airhockey1.util;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.android.airhockey1.AirHockeyRenderer;
import com.android.airhockey1.Constants;
import com.android.airhockey1.MeshRenderer;
import com.android.airhockey1.data.DataStructureA;
import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.ProblemData;
import com.android.airhockey1.util.Geometry.SimplePoint;
import com.android.airhockey1.util.Geometry.Node;
import com.android.airhockey1.util.Geometry.Line;
import com.android.airhockey1.util.Geometry.Surface;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;

import nb.geometricBot.Mesh;
import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 17/02/16.
 */
public class GeometryGLSurfaceView extends GLSurfaceView {

    public static class Pointer {
        private int ID;
        private long startTime, lastTime;
        private Geometry.SimplePoint startPoint, previousLastPoint, lastPoint;
        private boolean hasStopped;
        private boolean wasUsedLikeATap;
        private float totalDistance;

        public Pointer(int ID) {
            this.ID = ID;
            startTime = lastTime = 0;
            startPoint = new SimplePoint();
            previousLastPoint = new SimplePoint();
            lastPoint  = new SimplePoint();
            hasStopped = false;
            wasUsedLikeATap = false;
            totalDistance = 0f;
        }

        public int getID() { return ID; }

        public void startIt(float x, float y, long time) {
            startPoint.setCoordinates( x, y, 0f);
            previousLastPoint.setCoordinates(startPoint);
            lastPoint.setCoordinates(startPoint);
            startTime = time;
            totalDistance = 0f;
        }

        public void continueIt(float x, float y, long time) {
            previousLastPoint.setCoordinates(lastPoint);
            lastPoint.setCoordinates(x, y , 0f);
            lastTime = time;
            totalDistance += (float) Math.sqrt(Math.pow(lastPoint.x - previousLastPoint.x, 2) +
                    Math.pow(lastPoint.y - previousLastPoint.y, 2));
        }

        public void stopIt(float x, float y, long time) {
            continueIt(x, y, time);
            hasStopped = true;
        }

        public boolean hasStopped() { return hasStopped; }
        public long getPressDuration() { return lastTime - startTime; }
        public float getStartX() { return startPoint.x; }
        public float getStartY() { return startPoint.y; }
        public float getLastX() { return lastPoint.x; }
        public float getLastY() { return lastPoint.y; }
        public float getPreviousLastX() { return previousLastPoint.x; }
        public float getPreviousLastY() { return previousLastPoint.y; }
        public boolean wasUsedLikeATap() { return wasUsedLikeATap; }

        public void setWasUsedLikeATap(boolean wasUsedLikeATap) {
            this.wasUsedLikeATap = wasUsedLikeATap;
        }

        public float getMovedDistance() {
            return totalDistance;
        }
    }

    public static class Pointers {
        private ArrayList<Pointer> pointers;

        public Pointers() {
            pointers = new ArrayList();
        }

        public Pointer getPointerByID(int ID) {
            Pointer pointer = null;
            for (int i = 0; i < pointers.size(); i++) {
                pointer = pointers.get(i);
                if (ID == pointer.getID()) break;
            }
            return pointer;
        }

        public void removePointerByID(int ID) {
            Pointer pointer;
            for (int i = 0; i < pointers.size(); i++) {
                pointer = pointers.get(i);
                if (ID == pointer.getID()){
                    pointers.remove(i);
                    break;
                }
            }
        }

        public void removePointer(Pointer pointer) {
            removePointerByID(pointer.getID());
        }

        public void removeStoppedPointers() {
            Pointer pointer;
            for (int i = 0; i < pointers.size(); i++) {
                pointer = pointers.get(i);
                if (pointer.hasStopped()) pointers.remove(i);
            }
        }

        public void addPointer(Pointer pointer) {
            pointers.add(pointer);
        }

        public int getSize() { return pointers.size(); }
        public Pointer getPointer(int index) { return pointers.get(index); }
    }

    public static class Tap {
        SimplePoint p;

        public Tap() {
            p = new SimplePoint();
        }

        public Tap(float x, float y){
            p = new SimplePoint();
            setPosition(x, y);
        }

        public void setPosition(float x, float y){
            p.setCoordinates(x, y, 0f);
        }

        public SimplePoint getPosition() { return p; }
    }

    public static class Drag {
        int ID;
        SimplePoint begin, end;
        Node node;
        Line line;
        boolean hasNodeBeenSet;
        boolean hasLineBeenSet;
        boolean hasStopped;

        public Drag(int ID) {
            this.ID = ID;
            begin = new SimplePoint();
            end = new SimplePoint();
            node = null;
            line = null;
            hasNodeBeenSet = hasLineBeenSet = false;
            hasStopped = false;
        }

        public void setBeginningPosition(float x, float y) {
            begin.setCoordinates(x, y, 0f);
        }

        public void setEndingPosition(float x, float y) {
            end.setCoordinates(x, y, 0f);
        }

        public SimplePoint getBeginningPosition() { return begin; }
        public SimplePoint getEndingPosition() { return end; }

        public void setNodeRelatedToDrag(Node node) {
            this.node = node;
            hasNodeBeenSet = true;
        }
        public void setLineRelatedToDrag(Line line) {
            this.line = line;
            hasLineBeenSet = true;
        }

        public void removeNodeRelatedToDrag() {
            node = null;
            hasNodeBeenSet = false;
        }

        public Node getNode() { return node; }
        public Line getLine() { return line; }

        public boolean hasNodeBeenSet() { return hasNodeBeenSet; }
        public boolean hasLineBeenSet() { return hasLineBeenSet; }

        public int getID() { return ID; }

        public void stop() { hasStopped = true; }
        public boolean hasStopped() { return hasStopped; }

    }

    public static class Drags {
        private ArrayList<Drag> drags;
        private Set<Integer> idsSet;

        public Drags() {
            drags = new ArrayList();
            idsSet = new HashSet();
        }

        public Drag getDragByID(int ID) {
            Drag drag = null;
            if (!idsSet.contains(ID)) return drag;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (ID == drag.getID()) break;
            }
            return drag;
        }

        public void removeDragByID(int ID) {
            Drag drag;
            if (!idsSet.contains(ID)) return;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (ID == drag.getID()){
                    drags.remove(i);
                    idsSet.remove(ID);
                    break;
                }
            }
        }

        public void removeDrag(Drag drag) {
            removeDragByID(drag.getID());
        }

        public void removeDrag(int index) {
            if (index < 0) return;
            if (index > (drags.size() - 1)) return;
            Drag drag = drags.get(index);
            idsSet.remove(drag.getID());
            drags.remove(index);
        }

        public void addDrag(Drag drag) {
            if (idsSet.contains(drag.getID())) return;
            drags.add(drag);
            idsSet.add(drag.getID());
        }

        public int size() { return drags.size(); }
        public Drag getDrag(int index) {
            return drags.get(index);
        }

        public boolean contains(int ID) {
            return idsSet.contains(ID);
        }

        public void removeStoppedDrags() {
            Drag drag;
            for (int i = 0; i < drags.size(); i++) {
                drag = drags.get(i);
                if (drag.hasStopped()) removeDrag(i);
            }
        }
    }

    private Context context;
    public AirHockeyRenderer renderer;
    public boolean rendererSet = false;
    private boolean newAction = false;
    private int typeOfAction;
    private int currentEvent;
    private int previousEvent;
    private int mActivePointerId;
    long referenceTime;
    private final int INVALID_POINTER_ID = -1;
    Pointers pointers;
    Queue<Tap> taps;
    Drags drags;

    private GestureDetector mGestureDetector;
    private GestureDetector.SimpleOnGestureListener mGestureListener;

    private ScaleGestureDetector mScaleGestureDetector;
    private ScaleGestureDetector.OnScaleGestureListener mScaleGestureListener;

    public GeometryGLSurfaceView(Context context,
                                 AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        Log.i("Constructor", "Creating glSurfaceView");

        // Request an OpenGL ES 2.0 compatible context.
        setEGLContextClientVersion(2);

        renderer = null;
        rendererSet = false;

        typeOfAction = Constants.DOING_NOTHING;

        currentEvent = previousEvent = Constants.INITIAL_VALUE_INT;

        referenceTime = 0;

        mActivePointerId = INVALID_POINTER_ID;

        pointers = new Pointers();
        taps = new LinkedList();
        drags = new Drags();

        final View v = this.getRootView();

        //*
        setOnTouchListener(new OnTouchListener() {
            Tap tap;
            Drag drag;
            SimplePoint p, p1, p2;
            // When a user touches that view, we'll receive a call to onTouch()
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event != null) {
                    // Adds pointers or updates them
                    handlePointer(event, v.getWidth(), v.getHeight(), pointers);

                    // Identifies the taps (quick or long, no matters) and drags
                    identifyTapsAndDrags(pointers, taps, drags);

                    /*
                    Log.i("ACTIONS", "" + 1);
                    Log.i("ACTIONS", "" + 1);
                    Log.i("ACTIONS", "Pointers size = " + pointers.getSize());
                    Log.i("ACTIONS", "Taps size     = " + taps.size());
                    Log.i("ACTIONS", "Drags size    = " + drags.size());
                    for (int i = 0; i < drags.size(); i++) {
                        Log.i("ACTIONS", "Drag ID = " + drags.getDrag(i).getID());
                    }
                    //*/

                    // Reports all the taps
                    while (taps.size() > 0) {
                        tap = taps.poll();
                        p = tap.getPosition();
                        renderer.handleTouchPress(p.x, p.y);
                    }

                    // Reports all the drags
                    renderer.handleTouchDrags(drags);

                    // Removes the pointers and drags that have being stopped
                    pointers.removeStoppedPointers();
                    drags.removeStoppedDrags();

                    // Ask if the action that the user wanted to do was performed correctly

                    return true;
                } else {
                    return false;
                }
            }
        });
        //*/

        /*
        // We can listen in on view's touch events by calling setOnTouchListener()
        setOnTouchListener(new OnTouchListener() {
            // When a user touches that view, we'll receive a call to onTouch()
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null) {

                    // In milliseconds
                    long pressDuration = 0;

                    float pressedX = 0f;
                    float pressedY = 0f;
                    float distance = 0f;

                    //switch (event.getAction()) {
                    switch (MotionEventCompat.getActionMasked(event)) {

                        // Triggered by the firs pointer touching the screen
                        case MotionEvent.ACTION_DOWN: {
                            Log.i("ACTIONS", "ACTION_DOWN");

                            // Active pointer (finger)
                            mActivePointerId = MotionEventCompat.getPointerId(event, 0);

                            // We have to do here because apparently not always an ACTION_MOVE event
                            // ends with an ACTION_UP event.
                            if (previousEvent == MotionEvent.ACTION_MOVE) renderer.stopDragging();
                            manageEvents(MotionEvent.ACTION_DOWN);

                            referenceTime = System.currentTimeMillis();
                            Log.i("ACTIONS", "Start time = " + referenceTime);

                            break;
                        }

                        // Triggered by the last pointer leaving the screen
                        case MotionEvent.ACTION_UP: {
                            Log.i("ACTIONS", "ACTION_UP");

                            mActivePointerId = INVALID_ID;

                            // Convert touch coordinates into normalized device
                            // coordinates, keeping in mind that Android's Y
                            // coordinates are inverted.
                            final int pointerIndex = MotionEventCompat.getActionIndex(event);
                            pressedX = MotionEventCompat.getX(event, pointerIndex);
                            pressedY = MotionEventCompat.getY(event, pointerIndex);
                            final float normalizedX = (pressedX / (float) v.getWidth()) * 2 - 1;
                            final float normalizedY = -((pressedY / (float) v.getHeight()) * 2 - 1);

                            if (previousEvent == MotionEvent.ACTION_MOVE) renderer.stopDragging();
                            manageEvents(MotionEvent.ACTION_UP);

                            pressDuration = System.currentTimeMillis() - referenceTime;
                            if (newAction) {
                                renderer.setCurrentAction(typeOfAction);
                                newAction = false;
                            }

                            if (pressDuration < Constants.MAX_CLICK_DURATION) {
                                // Click event has occurred
                                // We dispatch calls to OpenGL thread
                                queueEvent(new Runnable() {
                                    @Override
                                    public void run() {
                                        renderer.handleTouchPress(normalizedX, normalizedY);
                                    }
                                });
                            }
                            break;
                        }

                        // Can be triggered by all the pointers
                        case MotionEvent.ACTION_MOVE: {
                            //Log.i("ACTIONS", "ACTION_MOVE");

                            manageEvents(MotionEvent.ACTION_MOVE);

                            // Gets the first pointer that touched the screen
                            if (mActivePointerId == INVALID_ID)
                                break; // First we check if we have a valid ID
                            final int pointerIndex = MotionEventCompat.findPointerIndex(event, mActivePointerId);

                            // Convert touch coordinates into normalized device
                            // coordinates, keeping in mind that Android's Y
                            // coordinates are inverted.
                            pressedX = MotionEventCompat.getX(event, pointerIndex);
                            pressedY = MotionEventCompat.getY(event, pointerIndex);
                            final float normalizedX = (pressedX / (float) v.getWidth()) * 2 - 1;
                            final float normalizedY = -((pressedY / (float) v.getHeight()) * 2 - 1);

                            pressDuration = System.currentTimeMillis() - referenceTime;
                            if (pressDuration > Constants.MAX_CLICK_DURATION) {
                                // We dispatch calls to OpenGL thread
                                queueEvent(new Runnable() {
                                    @Override
                                    public void run() {
                                        typeOfAction = Constants.DRAGGING_SOMETHING;
                                        renderer.setCurrentAction(typeOfAction);
                                        renderer.handleTouchDrag(normalizedX, normalizedY, typeOfAction);
                                    }
                                });
                            }

                            break;
                        }

                        // Non-primary pointer goes up
                        case MotionEvent.ACTION_POINTER_UP: {
                            mActivePointerId = INVALID_ID;


                            break;
                        }

                        // Extra pointer touching the screen after the primary pointer
                        case MotionEvent.ACTION_POINTER_DOWN: {
                            break;
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }
        });
        //*/

    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        Log.i("TOUCH EVENT", "Ernesto");
        /*
        boolean retVal = mScaleGestureDetector.onTouchEvent(e);
        retVal = mGestureDetector.onTouchEvent(e) || retVal;
        // Why to call super.onTouchEvent ??
        return retVal || super.onTouchEvent(e);
        //*/
        return true;
    }

    public static void identifyTapsAndDrags(Pointers pointers, Queue<Tap> taps, Drags drags ) {
        Pointer pointer;
        float movedDistance;
        Drag drag;

        for (int i = 0; i < pointers.getSize(); i++) {
            pointer = pointers.getPointer(i);

            // If the pointer has just being started then a tap has occurred
            if (!pointer.wasUsedLikeATap()) {
                taps.add(new Tap(pointer.getStartX(), pointer.getStartY()));
                pointer.setWasUsedLikeATap(true);
            }

            // Now we check if the pointer has moved enough to consider it as dragging
            movedDistance = pointer.getMovedDistance();
            if (movedDistance > Constants.MAX_TAP_DISTANCE) {
                // If the drag is not contained in the collection we add it
                if (!drags.contains(pointer.getID())) {
                    drag = new Drag(pointer.getID());
                    drags.addDrag(drag);
                } else {
                    drag = drags.getDragByID(pointer.getID());
                }

                // New positions
                drag.setBeginningPosition(pointer.getPreviousLastX(), pointer.getPreviousLastY());
                drag.setEndingPosition(pointer.getLastX(), pointer.getLastY());

                /*
                Log.i("PANNING", "#########################");
                Log.i("PANNING", "ID = " + drag.getID());
                Log.i("PANNING", "Previous" + pointer.getPreviousLastX() + "  " + pointer.getPreviousLastY());
                Log.i("PANNING", "Current " + pointer.getLastX() + "  " + pointer.getLastY());
                //*/

                // If pointer related to the drag has stopped, we also stop the drag
                if (pointer.hasStopped()) drag.stop();
            }
        }

    }

    public static void handlePointer(MotionEvent event, float width, float height, Pointers pointers) {
        int pointerID, pointerIndex;
        Pointer pointer = null;
        float x, y;

        // Get the index for this action
        pointerIndex = MotionEventCompat.getActionIndex(event);
        pointerID = MotionEventCompat.getPointerId(event, pointerIndex);
        // Convert touch coordinates into normalized device coordinates, keeping in mind that
        // Android's Y coordinates are inverted.
        x =   (MotionEventCompat.getX(event, pointerIndex) / width) * 2f - 1f;
        y = -((MotionEventCompat.getY(event, pointerIndex) / height) * 2f - 1f);

        // Determines if the pointer must be added to the array or finished
        //Log.i("ACTIONS", "Pointer ID = " + pointerID);
        switch (MotionEventCompat.getActionMasked(event)) {

            // Triggered by the firs pointer touching the screen
            case MotionEvent.ACTION_DOWN: {
                Log.i("ACTIONS", "ACTION_DOWN");

                // Creates a new pointer and sets the start time and the initial position
                pointer = new Pointer(pointerID);
                pointer.startIt(x, y, System.currentTimeMillis());
                pointers.addPointer(pointer);

                break;
            }

            // Extra pointer touching the screen after the primary pointer
            case MotionEvent.ACTION_POINTER_DOWN: {
                Log.i("ACTIONS", "ACTION_POINTER_DOWN");

                // Creates a new pointer and sets the start time and the initial position
                pointer = new Pointer(pointerID);
                pointer.startIt(x, y, System.currentTimeMillis());
                pointers.addPointer(pointer);

                break;
            }

            // Triggered by the last pointer leaving the screen
            case MotionEvent.ACTION_UP: {
                Log.i("ACTIONS", "ACTION_UP");

                // Stop the pointer
                pointer = pointers.getPointerByID(pointerID);
                pointer.stopIt(x, y, System.currentTimeMillis());

                break;
            }

            // Non-primary pointer goes up
            case MotionEvent.ACTION_POINTER_UP: {
                Log.i("ACTIONS", "ACTION_POINTER_UP");

                // Stop the pointer
                pointer = pointers.getPointerByID(pointerID);
                pointer.stopIt(x, y, System.currentTimeMillis());

                break;
            }

            // Can be triggered by all the pointers
            case MotionEvent.ACTION_MOVE: {
                //Log.i("ACTIONS", "-----------------------------");
                Log.i("ACTIONS", "ACTION_MOVE");
                //Log.i("ACTIONS", "Number of pointers = " + event.getPointerCount());

                for (int i = 0; i < event.getPointerCount(); i++) {
                    x =   (MotionEventCompat.getX(event, i) / width) * 2f - 1f;
                    y = -((MotionEventCompat.getY(event, i) / height) * 2f - 1f);
                    pointerID = MotionEventCompat.getPointerId(event, i);
                    //Log.i("ACTIONS", "Pointer ID = " + pointerID);
                    // The pointer is still alive
                    pointer = pointers.getPointerByID(pointerID);
                    pointer.continueIt(x, y, System.currentTimeMillis());
                }

                break;
            }
        }

        /*
        // We get the pointer and save the start time and the initial position
        pointerIndex = MotionEventCompat.getActionIndex(event);
        pointerID = MotionEventCompat.getPointerId(event, pointerIndex);
        x = MotionEventCompat.getX(event, pointerIndex);
        y = MotionEventCompat.getY(event, pointerIndex);
        */
    }

    private void manageEvents(int eventHappening) {
        previousEvent = currentEvent;
        currentEvent = eventHappening;
    }

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    // To know what the user wants to do
    public void setTypeOfAction(int type) {
        newAction = true;
        typeOfAction = type;
        // We report what the user wants to do
        renderer.setCurrentAction(type);
    }

    public void setParameters(GeometryData geometry,
                              PlotParameters plotParameters,
                              ProblemData problemData) {
        // Once we have received the data needed we can create the renderer
        renderer = new AirHockeyRenderer(context, geometry, plotParameters, problemData);

        // We set this renderer
        setRenderer(renderer);
        rendererSet = true;

        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void setCurrentForce(Conditions.Force currentForce) {
        renderer.setCurrentForce(currentForce);
    }

    public void setCurrentFixedDisplacement(Conditions.FixedDisplacement fixedDisplacement) {
        renderer.setCurrentFixedDisplacement(fixedDisplacement);
    }
}
