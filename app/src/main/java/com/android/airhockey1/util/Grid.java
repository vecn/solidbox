package com.android.airhockey1.util;

/**
 * Created by ernesto on 15/02/16.
 */
public class Grid {
    private float mainTicsIntervalSize;

    public Grid() {
        mainTicsIntervalSize = 1f;
    }

    public void setMainTicsIntervalSize(float mainTicsIntervalSize) {
        this.mainTicsIntervalSize = mainTicsIntervalSize;
    }
}
