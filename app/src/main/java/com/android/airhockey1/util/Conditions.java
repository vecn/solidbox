package com.android.airhockey1.util;

import com.android.airhockey1.data.InterfaceDataStructure;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by ernesto on 4/02/16.
 */

public class Conditions {

    static public class Force {
        private float X, Y, Z;

        public Force() {
            X = Y = Z = 0f;
        }

        public void setForceX(float forceX) {
            X = forceX;
        }

        public void setForceY(float forceY) {
            Y = forceY;
        }

        public void setForceZ(float forceZ) {
            Z = forceZ;
        }

        public float getForceX() {
            return X;
        }

        public float getForceY() {
            return Y;
        }

        public float getForceZ() {
            return Z;
        }

        public void copyForce(Force force) {
            X = force.getForceX();
            Y = force.getForceY();
            Z = force.getForceZ();
        }

        public float getMagnitude(){
            return (float)Math.sqrt(X*X + Y*Y + Z*Z);
        }

        public void clear() {
            X = Y = Z = 0f;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(X);
                dos.writeFloat(Y);
                dos.writeFloat(Z);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                X = dis.readFloat();
                Y = dis.readFloat();
                Z = dis.readFloat();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    static public class FixedDisplacement {
        private boolean isFixedX, isFixedY, isFixedZ;
        private float displacementX, displacementY, displacementZ;

        public FixedDisplacement() {
            isFixedX = isFixedY = isFixedZ = false;
            displacementX = displacementY = displacementZ = 0f;
        }

        public void fixX(float displacementX) {
            this.isFixedX = true;
            this.displacementX = displacementX;
        }

        public void fixY(float displacementY) {
            this.isFixedY = true;
            this.displacementY = displacementY;
        }

        public void fixZ(float displacementZ) {
            this.isFixedZ = true;
            this.displacementZ = displacementZ;
        }

        public boolean isFixedX() {
            return isFixedX;
        }
        public boolean isFixedY() {
            return isFixedY;
        }

        public boolean isFixedZ() {
            return isFixedZ;
        }

        public void setDisplacementX(float displacementX){
            this.displacementX = displacementX;
        }

        public void setDisplacementY(float displacementY) {
            this.displacementY = displacementY;
        }

        public void setDisplacementZ(float displacementZ) {
            this.displacementZ = displacementZ;
        }

        public float getDisplacementX() {
            return displacementX;
        }

        public float getDisplacementY() {
            return displacementY;
        }

        public float getDisplacementZ() {
            return displacementZ;
        }

        public void copyFixedDisplacement(FixedDisplacement fixedDisplacement) {
            isFixedX = fixedDisplacement.isFixedX();
            isFixedY = fixedDisplacement.isFixedY();
            isFixedZ = fixedDisplacement.isFixedZ();
            displacementX = fixedDisplacement.getDisplacementX();
            displacementY = fixedDisplacement.getDisplacementY();
            displacementZ = fixedDisplacement.getDisplacementZ();
        }

        public void clear(){
            isFixedX = isFixedY = isFixedZ = false;
            displacementX = displacementY = displacementZ = 0f;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeBoolean(isFixedX);
                if (isFixedX) dos.writeFloat(displacementX);

                dos.writeBoolean(isFixedY);
                if (isFixedY) dos.writeFloat(displacementY);

                dos.writeBoolean(isFixedZ);
                if (isFixedZ) dos.writeFloat(displacementZ);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                isFixedX = dis.readBoolean();
                displacementX = 0f;
                if (isFixedX) displacementX = dis.readFloat();

                isFixedY = dis.readBoolean();
                displacementY = 0f;
                if (isFixedY) displacementY = dis.readFloat();

                isFixedZ = dis.readBoolean();
                displacementZ = 0f;
                if (isFixedZ) displacementZ = dis.readFloat();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    static public class AppliedConditions implements InterfaceDataStructure {
        // To know the element with restrictions. It could be a node, line or surface
        private int id;

        private Force force;
        private FixedDisplacement fixedDisplacement;

        boolean hasAppliedForce;
        boolean hasFixedDisplacement;

        public AppliedConditions(int idNode) {
            this.id = idNode;

            force = null;
            fixedDisplacement = null;

            hasAppliedForce = false;
            hasFixedDisplacement = false;
        }

        public int getId() {
            return id;
        }

        public void setId(int idNode) {
            this.id = idNode;
        }

        public void copyForce(Force force){
            if (!hasAppliedForce) this.force = new Force();
            this.force.copyForce(force);
            hasAppliedForce = true;
        }

        public void copyFixedDisplacement(FixedDisplacement fixedDisplacement) {
            if (!hasFixedDisplacement) this.fixedDisplacement = new FixedDisplacement();
            this.fixedDisplacement.copyFixedDisplacement(fixedDisplacement);
            hasFixedDisplacement = true;
        }

        public Force getForce() { return force; }
        public FixedDisplacement getFixedDisplacement() { return fixedDisplacement; }

        public boolean hasAppliedForce() { return hasAppliedForce; }
        public boolean hasFixedDisplacement() { return hasFixedDisplacement; }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeBoolean(hasAppliedForce);
                if (hasAppliedForce) force.writeToFile(dos);
                dos.writeBoolean(hasFixedDisplacement);
                if (hasFixedDisplacement) fixedDisplacement.writeToFile(dos);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                hasAppliedForce = dis.readBoolean();
                if (hasAppliedForce) {
                    force = new Force();
                    force.readFromFile(dis);
                }

                hasFixedDisplacement = dis.readBoolean();
                if (hasFixedDisplacement) {
                    fixedDisplacement = new FixedDisplacement();
                    fixedDisplacement.readFromFile(dis);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

}
