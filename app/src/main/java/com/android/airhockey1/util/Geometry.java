package com.android.airhockey1.util;

import android.provider.ContactsContract;
import android.util.Log;

import com.android.airhockey1.AirHockeyRenderer;
import com.android.airhockey1.Constants;
import com.android.airhockey1.MeshRenderer;
import com.android.airhockey1.TabFragment2;
import com.android.airhockey1.data.Circles;
import com.android.airhockey1.data.GeometryData;
import com.android.airhockey1.data.InterfaceDataStructure;
import com.android.airhockey1.data.Lines;
import com.android.airhockey1.data.Nodes;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.Points;
import com.android.airhockey1.data.Rectangles;
import com.android.airhockey1.data.RegularPolygons;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.Vector;

import nb.geometricBot.GeometricBot;
import nb.geometricBot.Mesh;
import nb.geometricBot.Model;
import nb.geometricBot.ModelStatus;
import nb.pdeBot.finiteElement.MeshResults;

import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.setIdentityM;

/**
 * Created by ernesto on 6/01/16.
 */
public class Geometry {

    public static class PropertiesInCAD {
        private boolean isSelected;

        public PropertiesInCAD(){
            isSelected = false;
        }
        public void select(){
            isSelected = true;
        }
        public void unselect(){
            isSelected = false;
        }
        public boolean isSelected(){
            return isSelected;
        }
    }

    public static class PropertiesInMesh {
        private int id;

        public PropertiesInMesh() {
            id = Constants.INVALID_ID;
        }

        public void setId(int id) { this.id = id; }
        public int getId() { return id; }

        public void copy(PropertiesInMesh propertiesInMesh) {
            id = propertiesInMesh.id;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeInt(id);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                id = dis.readInt();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public static class SurfaceProperties {
        private boolean isItPartOfASurface;
        private Surface surface;

        public SurfaceProperties() {
            isItPartOfASurface = false;
            surface = null;
        }

        public boolean isItPartOfASurface() { return isItPartOfASurface; }
        public Surface getSurface() { return surface; }

        public void setSurface(Surface surface) {
            isItPartOfASurface = true;
            this.surface = surface;
        }

        public void reset() {
            isItPartOfASurface = false;
            surface = null;
        }

        public boolean isItASpecialSurface() { return surface.shapeInfo.specialShape; }
        public int getTypeOfSpecialShape() { return surface.shapeInfo.typeOfSpecialShape; }
        public Circle getCircle() { return surface.shapeInfo.circle; }
        public Rectangle getRectangle() { return surface.shapeInfo.rectangle; }
        public RegularPolygon getRegularPolygon() { return surface.shapeInfo.regularPolygon; }
    }

    public static class LineProperties {
        private boolean isItPartOfALine;
        private Line line;

        public LineProperties() {
            isItPartOfALine = false;
            line = null;
        }

        public boolean isItPartOfALine() { return isItPartOfALine; }
        public Line getLine() { return line; }

        public void setLine(Line line) {
            isItPartOfALine = true;
            this.line = line;
        }

        public void reset() {
            isItPartOfALine = false;
            line = null;
        }
    }

    public static class Point implements InterfaceDataStructure {

        public class NodeInfo {
            private boolean isANode;
            private Node node;

            NodeInfo() {
                isANode = false;
                node = null;
            }

            public void setPartOfANode(Node node) {
                this.isANode = true;
                this.node = node;
            }

            public boolean isPartOfANode() {
                return isANode;
            }

            public Node getNode() {
                return node;
            }

            public void copyNodeInfo(NodeInfo nodeInfo) {
                isANode = nodeInfo.isANode;
                node = nodeInfo.node;
            }

            public boolean writeToFile(DataOutputStream dos) {
                try {
                    dos.writeBoolean(isANode);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            public boolean readFromFile(DataInputStream dis, Node node) {
                try {
                    isANode = dis.readBoolean();
                    if (isANode) this.node = node;
                    else         this.node = null;
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }

        private int id;
        private float x, y, z;
        private NodeInfo nodeInfo;
        public PropertiesInMesh propertiesInMesh;
        private boolean hasConditions;
        private Conditions.AppliedConditions conditions;
        public SurfaceProperties surfaceProperties;

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeInt(id);
                dos.writeFloat(x);
                dos.writeFloat(y);
                dos.writeFloat(z);
                dos.writeBoolean(hasConditions);
                if (hasConditions) {
                    conditions.writeToFile(dos);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                id = dis.readInt();
                x = dis.readFloat();
                y = dis.readFloat();
                z = dis.readFloat();
                hasConditions = dis.readBoolean();
                if (hasConditions) {
                    conditions = new Conditions.AppliedConditions(id);
                    conditions.readFromFile(dis);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public Point() {
            id = 0;
            x = y = z = 0f;
            nodeInfo = new NodeInfo();
            propertiesInMesh = new PropertiesInMesh();
            hasConditions = false;
            conditions = null;
            surfaceProperties = new SurfaceProperties();
        }

        public Point(int id) {
            this.id = id;
            x = y = z = 0f;
            nodeInfo = new NodeInfo();
            propertiesInMesh = new PropertiesInMesh();
            hasConditions = false;
            conditions = null;
            surfaceProperties = new SurfaceProperties();
        }

        public Point(float x, float y, float z, int id){
            this.id = id;
            this.x = x;
            this.y = y;
            this.z = z;
            nodeInfo = new NodeInfo();
            propertiesInMesh = new PropertiesInMesh();
            hasConditions = false;
            conditions = null;
            surfaceProperties = new SurfaceProperties();
        }

        public Point(float x, float y, float z){
            this.id = -1;
            this.x = x;
            this.y = y;
            this.z = z;
            nodeInfo = new NodeInfo();
            propertiesInMesh = new PropertiesInMesh();
            hasConditions = false;
            conditions = null;
            surfaceProperties = new SurfaceProperties();
        }

        public int getId() { return id; }
        public void setId(int id) { this.id = id; }

        public float getX() { return x; }
        public float getY() { return y; }
        public float getZ() { return z; }

        public void setCoordinates(float x, float y, float z){
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public NodeInfo getNodeInfo(){
            return nodeInfo;
        }

        public void copyPoint(Point p) {
            id = p.id;
            x = p.x;
            y = p.y;
            z = p.z;
            nodeInfo.copyNodeInfo(p.nodeInfo);
            propertiesInMesh.copy(p.propertiesInMesh);
        }

        public void move(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void translate(float x, float y, float z) {
            this.x += x;
            this.y += y;
            this.z += z;
        }

        public void copyForce(Conditions.Force force){
            if (!hasConditions) conditions = new Conditions.AppliedConditions(id);
            hasConditions = true;
            conditions.copyForce(force);
        }

        public void copyFixedDisplacement(Conditions.FixedDisplacement fixedDisplacement) {
            // This may be used when the user wants to clear the imposed displacements on a node
            if ((!fixedDisplacement.isFixedX()) && (!fixedDisplacement.isFixedY()) &&
                    (!fixedDisplacement.isFixedZ())) {
                if (hasConditions) {
                    if (!conditions.hasAppliedForce) {
                        conditions = null;
                        hasConditions = false;
                    }
                }
                return;
            }

            if (!hasConditions) conditions = new Conditions.AppliedConditions(id);
            hasConditions = true;
            conditions.copyFixedDisplacement(fixedDisplacement);
        }

        public Conditions.AppliedConditions getConditions() { return conditions; }
        public boolean hasConditions() { return hasConditions; }
    }

    public static class Node implements InterfaceDataStructure {
        private Point point;
        private int id;
        public PropertiesInCAD propertiesInCAD;

        public boolean writeToFile(DataOutputStream dos) {
            try {
                // Not all the properties are written, they will be written later (when
                // defining the surfaces, etc)
                dos.writeInt(id);
                dos.writeInt(point.getId());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis, Points points) {
            int pointID;
            try {
                id = dis.readInt();
                pointID = dis.readInt();
                point = points.getElementById(pointID);
                if (point != null) point.nodeInfo.setPartOfANode(this);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public Node(Point point) {
            this.point = point;
            if (null != point) point.nodeInfo.setPartOfANode(this);
            propertiesInCAD = new PropertiesInCAD();
        }

        public Node() {
            point = new Point();
            point.nodeInfo.setPartOfANode(this);
            propertiesInCAD = new PropertiesInCAD();
        }

        public Node(int id){
            this.id = id;
            point = new Point();
            point.nodeInfo.setPartOfANode(this);
            propertiesInCAD = new PropertiesInCAD();
        }

        public Node(float x, float y, float z, int id){
            this.id = id;
            point = new Point(x, y, z);
            point.nodeInfo.setPartOfANode(this);
            propertiesInCAD = new PropertiesInCAD();
        }

        public Node(float x, float y, float z){
            this.id = -1;
            point = new Point(x, y, z);
            point.nodeInfo.setPartOfANode(this);
            propertiesInCAD = new PropertiesInCAD();
        }

        public SimplePoint getSimplePoint() { return  new SimplePoint(point.x, point.y, point.z); }

        public Point getPoint() { return point; }

        public int getId() { return id; }
        public void setId(int id) { this.id = id; }

        public float GetX() { return point.getX(); }
        public float GetY() { return point.getY(); }
        public float GetZ() { return point.getZ(); }

        public void SetCoordinates(float x, float y, float z) {
            point.setCoordinates(x, y, z);
        }

        public void move(float x, float y, float z) {
            point.move(x, y, z);
        }
        public void translate(float x, float y, float z) {
            point.translate(x, y, z);
        }

        public int getMeshId() { return point.propertiesInMesh.getId(); }
    }

    // This class has to be renamed as rect
    public static class Line implements InterfaceDataStructure {
        protected int Id;
        protected Point p1, p2;
        public PropertiesInCAD propertiesInCAD;
        public SurfaceProperties surfaceProperties;
        private Conditions.AppliedConditions conditions;
        private boolean hasConditions;

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeInt(Id);
                dos.writeInt(p1.getId());
                dos.writeInt(p2.getId());
                dos.writeBoolean(hasConditions);
                if (hasConditions) {
                    conditions.writeToFile(dos);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis, Points points) {
            int p1ID, p2ID;
            try {
                Id = dis.readInt();
                p1ID = dis.readInt();
                p2ID = dis.readInt();
                p1 = points.getElementById(p1ID);
                p2 = points.getElementById(p2ID);
                hasConditions = dis.readBoolean();
                if (hasConditions) {
                    conditions = new Conditions.AppliedConditions(Id);
                    conditions.readFromFile(dis);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public Line() {
            Id = Constants.INVALID_ID;
            propertiesInCAD = new PropertiesInCAD();
            surfaceProperties = new SurfaceProperties();
            p1 = p2 = null;
            conditions = null;
            hasConditions = false;
        }

        public Line(int Id) {
            this.Id = Id;
            propertiesInCAD = new PropertiesInCAD();
            surfaceProperties = new SurfaceProperties();
            p1 = p2 = null;
            conditions = null;
            hasConditions = false;
        }

        public Line(Point p1, Point p2, int Id) {
            this.Id = Id;
            this.p1 = p1;
            this.p2 = p2;
            this.propertiesInCAD = new PropertiesInCAD();
            this.surfaceProperties = new SurfaceProperties();
            conditions = null;
            hasConditions = false;
        }

        public Line(Point p1, Point p2) {
            this.Id = Constants.INVALID_ID;
            this.p1 = p1;
            this.p2 = p2;
            this.propertiesInCAD = new PropertiesInCAD();
            this.surfaceProperties = new SurfaceProperties();
            conditions = null;
            hasConditions = false;
        }

        public void setPoints(Point p1, Point p2) {
            this.p1 = p1;
            this.p2 = p2;
        }

        public void setFirstPoint(Point point) {
            this.p1 = point;
        }
        public void setLastPoint(Point point) {
            this.p2 = point;
        }

        public Point getFirstPoint() { return p1; }
        public Point getLastPoint() { return p2; }

        public int getId() { return Id; }
        public void setId(int id) { this.Id = id; }

        public Conditions.AppliedConditions getConditions() { return conditions; }

        public boolean hasConditions() { return hasConditions; }

        public void copyForce(Conditions.Force force){
            if (!hasConditions) conditions = new Conditions.AppliedConditions(Id);
            hasConditions = true;
            conditions.copyForce(force);
        }

        public void copyFixedDisplacement(Conditions.FixedDisplacement fixedDisplacement) {
            if (!hasConditions) conditions = new Conditions.AppliedConditions(Id);
            hasConditions = true;
            conditions.copyFixedDisplacement(fixedDisplacement);
        }

    }
    //*
    public static class Surface implements InterfaceDataStructure {
        public class ShapeInfo {
            boolean specialShape;
            int typeOfSpecialShape;
            Circle circle;
            Rectangle rectangle;
            RegularPolygon regularPolygon;

            public ShapeInfo() {
                specialShape = false;
                typeOfSpecialShape = Constants.TYPE_SHAPE_INVALID_VALUE;
                circle = null;
                rectangle = null;
                regularPolygon = null;
            }

            public boolean isSpecialShape() { return  specialShape; }
            public int getTypeOfSpecialShape() { return  typeOfSpecialShape; }
            public Circle getCircle() { return  circle; }
            public Rectangle getRectangle() { return  rectangle; }
            public RegularPolygon getRegularPolygon() { return  regularPolygon; }
        }

        protected int Id;
        protected java.util.Vector<Line> lines;
        protected Set<Integer> pointsIds;
        protected java.util.Vector<Point> points;
        public ShapeInfo shapeInfo;

        private Model model;
        private Mesh mesh;

        public boolean writeToFile(DataOutputStream dos) {
            Point point;
            Line line;
            Iterator<Point> nodeIterator;
            Iterator<Line> lineIterator;

            try {
                dos.writeInt(Id);

                // Writes points' ids
                dos.writeInt(points.size());
                nodeIterator = points.iterator();
                while (nodeIterator.hasNext()) {
                    point = nodeIterator.next();
                    dos.writeInt(point.getId());
                }

                // Writes lines' ids
                dos.writeInt(lines.size());
                lineIterator = lines.iterator();
                while (lineIterator.hasNext()) {
                    line = lineIterator.next();
                    dos.writeInt(line.getId());
                }

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis,
                                    Points geometryPoints,
                                    Lines geometryLines) {
            int numberOfPoints, numberOfLines, id;
            Line line;
            try {

                Id = dis.readInt();

                numberOfPoints = dis.readInt();
                for (int i = 0; i < numberOfPoints; i++) {
                    id = dis.readInt();
                    points.add(geometryPoints.getElementById(id));
                    pointsIds.add(id);
                }

                numberOfLines = dis.readInt();
                for (int i = 0; i < numberOfLines; i++) {
                    id = dis.readInt();
                    line = geometryLines.getElementById(id);
                    if (null != line) lines.add(line);
                }

                commit();
                setModel(createModel(this));
                meshSurface();

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public void deleteWholeSurfaceFromGeometry(GeometryData geometry) {
            geometry.deleteSurface(this);
            // Removes the points/nodes and lines from the geometry
            for (int i = 0; i < lines.size(); i++)  geometry.deleteLine(lines.get(i));
            for (int i = 0; i < points.size(); i++) geometry.deletePoint(points.get(i));
        }

        public void clear() {
            Id = Constants.INVALID_ID;
            lines.clear();
            points.clear();
            pointsIds.clear();
            mesh = null;
            model = null;
            shapeInfo.typeOfSpecialShape = Constants.TYPE_SHAPE_INVALID_VALUE;
            shapeInfo.specialShape = false;
        }

        public Surface() {
            Id = Constants.INVALID_ID;
            lines  = new java.util.Vector();
            points = new java.util.Vector();
            pointsIds = new HashSet();
            mesh = null;
            model = null;
            shapeInfo = new ShapeInfo();
        }

        public void setModel(Model model,
                             java.util.Vector<Point> lastAddedPoints,
                             java.util.Vector<Line> lastAddedLines) {
            Point point;

            this.model = model;
            // Copy the points' references
            this.points = new java.util.Vector(lastAddedPoints);
            // Copy the lines' references
            this.lines  = new java.util.Vector(lastAddedLines);

            for (int i = 0; i < lines.size(); i++) {
                point = lines.get(i).getFirstPoint();
                if (!pointsIds.contains(point.getId())) pointsIds.add(new Integer(point.getId()));
                point = lines.get(i).getLastPoint();
                if (!pointsIds.contains(point.getId())) pointsIds.add(new Integer(point.getId()));
            }
        }

        public void setModel(Model model) {
            this.model = model;
        }

        public void addLine(Line line) {
            Boolean isLineAlreadyIncluded = false;
            Iterator<Line> it = lines.iterator();
            while (it.hasNext()) {
                if (it.next().getId() == line.getId()) {
                    isLineAlreadyIncluded = true;
                    break;
                }
            }
            if (!isLineAlreadyIncluded) {
                lines.add(line);
                checkNewPoint(line.getFirstPoint());
                checkNewPoint(line.getLastPoint());
            }
        }

        private void checkNewPoint(Point point) {
            if (!pointsIds.contains(point.getId())){
                pointsIds.add(new Integer(point.getId()));
                points.add(point);
            }
        }

        /*
        public void addDisconnectedNode(Node node){
            // There is no need to add this node's point to the set of points because it is not
            // connected to any line
            disconnectedNodes.add(node);
        }
        //*/

        public void meshSurface() {
            // Use a coarse mesh
            mesh = GeometricBot.createMesh(model);
        }

        public static void printMesh(String TAG, String label, Mesh mesh) {
            Log.i(TAG, label);
            final int dim = Constants.PROBLEM_DIMENSION;

            Log.i(TAG, "Vertices:");
            float[] vertices = mesh.getVerticesRef();
            for (int i = 0; i < mesh.getNVertices(); i++)
                Log.i(TAG, "    " + vertices[i*dim] + "  " + vertices[i*dim+1]);
        }

        // This method has to be always called when a valid surface is finished
        public boolean commit() {
            Line line;

            // Checks if it is a valid surface
            //if (!isAValidSurface()) return false;

            // Tells to every line that they are part of this surface\
            for (int i = 0; i < lines.size(); i++) {
                line = lines.get(i);

                line.surfaceProperties.setSurface(this);
                line.getFirstPoint().surfaceProperties.setSurface(this);
                line.getLastPoint().surfaceProperties.setSurface(this);
            }

            return true;
        }

        public void releaseResources() {
            Line line;
            // Tells to every line that they no longer belong to this surface
            for (int i = 0; i < lines.size(); i++) {
                line = lines.get(i);

                line.surfaceProperties.reset();
                line.getFirstPoint().surfaceProperties.reset();
                line.getLastPoint().surfaceProperties.reset();
            }
        }

        public void removeLine(Line line) {
            for (int i = 0; i < lines.size(); i++) {
                if (line.getId() ==  lines.get(i).getId()) {
                    // Tells the line that no longer belongs to this surface
                    line.surfaceProperties.reset();
                    // Removes the line from the array
                    lines.remove(i);
                    break;
                }
            }
        }

        public boolean isAValidSurface() {
            boolean isValid = false;
            model = createModel(points, lines);
            ModelStatus modelStatus = model.verify();
            Log.i("MODEL", "Status = " + modelStatus);
            if (modelStatus == ModelStatus.OK) isValid = true;
            return isValid;
        }

        // This function should be always called after modifying the surface
        public static Model checkModel(Model model, Surface surface) {
            ModelStatus modelStatus = model.verify();

            // This arrays will be used to store only the needed points and lines to create a
            // valid model.
            java.util.Vector<Point> newPoints = new java.util.Vector();
            java.util.Vector<Line> newLines = new java.util.Vector();

            switch (modelStatus) {
                case REPEATED_VERTICES:
                    // Some vertices may have the same coordinates or they may be very close
                    collapseModel(newPoints, newLines, surface);
                    model = createModel(newPoints, newLines);
                    break;

                case OK:
                    break;
            }

            return model;
        }

        public static void collapseModel(java.util.Vector<Point> newPoints,
                                         java.util.Vector<Line> newLines,
                                         Surface surface) {

            float minDistance = Constants.SMALLEST_DISTANCE;
            // To store the group each vertex belongs to
            java.util.Vector<Integer> groups = new java.util.Vector();
            // To store the center of each group
            java.util.Vector<Point> groupCenters = new java.util.Vector();

            java.util.Vector<Line> lines = surface.getLines();

            // The minDistance has to be in accordance to the maximum zoom in value and also
            // has to be changed until the model is valid. In this case we are only testing with
            // one minDistance value.
            identifyGroupOfPoints(minDistance, groups, groupCenters, surface);

            // New set of points without "repeated" points
            Iterator<Point> pointIterator = groupCenters.iterator();
            Point point;
            int count = 0;
            while (pointIterator.hasNext()) {
                point = pointIterator.next();
                newPoints.add(new Point(point.getX(), point.getY(), point.getZ(), count));
                count++;
            }

            // New set of lines
            Iterator<Line> lineIterator = lines.iterator();
            Line line;
            int g1, g2;
            while (lineIterator.hasNext()) {
                line = lineIterator.next();
                g1 = groups.get(line.getFirstPoint().getId());
                g2 = groups.get(line.getLastPoint().getId());
                if (g1 != g2)
                    newLines.add(new Line(newPoints.get(g1), newPoints.get(g2)));
            }

            /*
            Log.i("NEW MODEL", "New points size = " + newPoints.size());
            for (int i = 0; i < newPoints.size(); i++) {
                Log.i("NEW MODEL", "" + newPoints.get(i).getId() + "   " + newPoints.get(i).getX() + "   " + newPoints.get(i).getY() + "   " + newPoints.get(i).getZ());
            }

            Log.i("NEW MODEL", "New lines size = " + newLines.size());
            for (int i = 0; i < newLines.size(); i++) {
                Log.i("NEW MODEL", "" + newLines.get(i).getFirstPoint().getId() + "   " + newLines.get(i).getLastPoint().getId());
            }
            */
        }

        private static void identifyGroupOfPoints(final float minDistance,
                                                  java.util.Vector<Integer> groups,
                                                  java.util.Vector<Point> groupCenters,
                                                  Surface surface) {
            int currentGroup;
            float distance;
            SimplePoint center = new SimplePoint();
            SimplePoint simplePoint = new SimplePoint();
            List<Point> listPoints = new LinkedList();
            ListIterator<Point> iterator;
            Point point;

            java.util.Vector<Point> points = surface.getPoints();

            // Initialize some variables
            groups.setSize(points.size());
            for (int i = 0; i < points.size(); i++) {
                point = points.get(i);
                listPoints.add(point);
                groups.set(point.getId(), -1);
            }

            // Find the vertices very close to each other and make groups. Each vertex will belong
            // to a group so may be some groups with one vertex
            currentGroup = 0;
            while (listPoints.size() > 0) {
                iterator = listPoints.listIterator();
                point = iterator.next();

                // A new group
                groupCenters.add(point);
                center.setCoordinates(point.getX(), point.getY(), point.getZ());
                // The first group's point
                groups.set(point.getId(), currentGroup);

                // Adds the vertices close enough to the center of this group
                iterator.remove();
                while (iterator.hasNext()){
                    point = iterator.next();
                    simplePoint.setCoordinates(point.getX(), point.getY(), point.getZ());
                    distance = distanceBetweenPoints(center, simplePoint);
                    if (distance < minDistance) {
                        groups.set(point.getId(), currentGroup);
                        iterator.remove(); // Here we may have to move the iterator to the previous element
                    }
                }

                currentGroup++;
            }

            Log.i("GROUPS", "Groups" );
            for (int i = 0; i < groups.size(); i++) {
                Log.i("GROUPS", "Node " + i + " is in group " + groups.get(i));
            }
        }

        public static void collapseBoundaryConditions(java.util.Vector<Integer> groups) {

        }

        public static Model createModel(Surface surface) {

            Model model = createModel(surface.getPoints(), surface.getLines());
            ModelHelper.printModel("MODEL", "Model", model);
            Log.i("MODEL", "Model status = " + model.verify());
            model = checkModel(model, surface);
            ModelHelper.printModel("MODEL", "Model after", model);
            return model;
        }

        public static float calculateAverageX(float[] vertices) {
            return calculateAverage(vertices, 0);
        }

        public static float calculateAverageY(float[] vertices) {
            return calculateAverage(vertices, 1);
        }

        public static float calculateAverage(float[] vertices, final int offset) {
            int dim = Constants.PROBLEM_DIMENSION;
            int numOfVertices = vertices.length / dim;
            // Finds the average on X and on Y to center the model
            float sum = 0f;
            for (int j = 0; j < numOfVertices; j++) sum += vertices[j*dim+offset];
            return (sum / (float)numOfVertices);
        }

        public static Model createModel(java.util.Vector<Point> points, java.util.Vector<Line> lines) {
            int numberOfVertices;
            int[] connEdges;
            float[] coordinates;
            float[] holes = null;

            Model model;

            model = new Model();

            connEdges   = new int[lines.size()*2];
            coordinates = new float[points.size()*2];

            // Centers the model
            for (int j = 0; j < points.size(); j++) {
                // Coordinates
                coordinates[j * 2]     = points.get(j).getX();
                coordinates[j * 2 + 1] = points.get(j).getY();
            }

            // Finds the nodes' ids for each point of every line
            int index;
            for (int j = 0; j < lines.size(); j++) {
                // Connectivity
                index = 0;
                while (points.get(index++).getId() != lines.get(j).getFirstPoint().getId()); index--;
                connEdges[j * 2] = index;

                index = 0;
                while (points.get(index++).getId() != lines.get(j).getLastPoint().getId()); index--;
                connEdges[j * 2 + 1] = index;
            }

            model.setEdges(connEdges);
            model.setVertices(coordinates);
            model.setHoles(null);

            return model;
        }

        // This has to be in accordance with the way the model was built.
        public int getEgeIdOnModel(Line line) {
            int modelId, i;
            for (i = 0; i < lines.size(); i++) if (lines.get(i).getId() == line.getId()) break;
            modelId = i;
            return modelId;
        }

        // Get the points' ids on the mesh, once the model and its mesh have been created
        public void setCorrespondingMeshIdsOnPoints(MeshResults meshResults) {
            int[] modelVtx = meshResults.getModelVtxRef();
            // This depends on the way the vertices were defined when the model was created
            for (int i = 0; i < points.size(); i++)
                points.get(i).propertiesInMesh.setId(modelVtx[i]);
        }

        public Model getModel() {
            return model;
        }
        public Mesh getMesh() {
            return mesh;
        }
        public void setId(int Id) {
            this.Id = Id;
        }
        public int getId() {
            return Id;
        }

        public int getNEdges() { return lines.size(); }
        public int getNPoints() { return points.size(); }

        public Line getLine(int index) {
            // Index out of bounds
            if (index < 0) return null;
            if (index > lines.size() - 1) return null;
            return lines.get(index);
        }

        public Point getPoint(int index) {
            // Index out of bounds
            if (index < 0) return null;
            if (index > points.size() - 1) return null;
            return points.get(index);
        }

        public java.util.Vector<Line> getLines() {
            return lines;
        }
        public java.util.Vector<Point> getPoints() { return points; }

        public void print(String TAG, String label) {
            Log.i(TAG, label);
            Log.i(TAG, "Vertices");
            for (int i = 0; i < points.size(); i++) {
                Log.i(TAG, "" + points.get(i).getX() + "  " + points.get(i).getY());
            }
            Log.i(TAG, "Lines");
            for (int i = 0; i < lines.size(); i++) {
                Log.i(TAG, "" + lines.get(i).getFirstPoint().getId() + "  " +
                                lines.get(i).getLastPoint().getId());
            }
        }

        // Return the previous line to a given line in clockwise or counterclockwise order
        public Line getPreviousLine(Line line) {
            Line previousLine = null;
            boolean wasFound = false;

            for (int i = 0; i < lines.size(); i++) {
                previousLine = lines.get(i);
                if (previousLine.getId() == line.getId()) continue;
                if ((previousLine.getFirstPoint().getId() ==  line.getFirstPoint().getId()) ||
                        (previousLine.getLastPoint().getId() == line.getFirstPoint().getId())) {
                    wasFound = true;
                    break;
                }
            }

            if (!wasFound) previousLine = null;
            return previousLine;
        }

        // Return the next line to a given line in clockwise or counterclockwise order
        public Line getNextLine(Line line) {
            Line nextLine = null;
            boolean wasFound = false;

            for (int i = 0; i < lines.size(); i++) {
                nextLine = lines.get(i);
                if (nextLine.getId() == line.getId()) continue;
                if ((nextLine.getFirstPoint().getId() ==  line.getLastPoint().getId()) ||
                        (nextLine.getLastPoint().getId() == line.getLastPoint().getId())) {
                    wasFound = true;
                    break;
                }
            }

            if (!wasFound) nextLine = null;
            return nextLine;
        }

        public void handleControlNodeMovement(Node node, float x, float y, boolean hasDragStopped) {
            node.SetCoordinates(x, y, 0f);
            if (hasDragStopped) {
                setModel(createModel(this));
                meshSurface();
            }
        }
    }
    //*/

    // Determines if a polygon has a counter-clockwise order (the polygon lies in the xy plane)
    public static boolean doesItHaveCounterClockwiseOrder(java.util.Vector<Node> vector) {
        int i;
        float sum = 0f;
        for (i = 1; i < vector.size(); i++) {
            sum += (vector.get(i).GetX() - vector.get(i-1).GetX()) *
                   (vector.get(i).GetY() + vector.get(i-1).GetY());
        }
        sum += (vector.get(0).GetX() - vector.get(i).GetX()) *
               (vector.get(0).GetY() + vector.get(i).GetY());

        // Counter-clockwise order when sum is negative
        return sum < 0f;
    }

    public static void setCounterClockwiseOrder(java.util.Vector<Node> vector) {
        Node node;
        int moves = (vector.size() - 1) / 2;
        for (int i = 1; i < moves + 1; i++) {
            node = vector.get(vector.size() - i);
            vector.set(vector.size() - i, vector.get(i));
            vector.set(i, node);
        }
    }

    public static class SimplePoint {
        //public final float x, y, z;
        public float x, y, z;

        public SimplePoint(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public SimplePoint(SimplePoint simplePoint) {
            this.x = simplePoint.x;
            this.y = simplePoint.y;
            this.z = simplePoint.z;
        }

        public SimplePoint() {
            x= y = z = 0f;
        }

        public SimplePoint translateY(float distance) {
            //return new SimplePoint(x, y + distance, z);
            y += distance;
            return this;
        }

        public SimplePoint translate(Vector vector) {
            x += vector.x;
            y += vector.y;
            z += vector.z;
            return this;
        }

        public void translate(float dx, float dy, float dz) {
            x += dx;
            y += dy;
            z += dz;
        }

        public void setCoordinates(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void setCoordinates(SimplePoint simplePoint) {
            x = simplePoint.x;
            y = simplePoint.y;
            z = simplePoint.z;
        }

        public void setCoordinates(Point point) {
            x = point.getX();
            y = point.getY();
            z = point.getZ();
        }

        public float distanceFromOrigin() {
            return (float)Math.sqrt(x*x + y*y + z*z);
        }

        public void print(final String TAG, final String label) {
            Log.i(TAG, label + x + "   " + y + "   " + z);
        }
    }

    public static class SimpleLine {
        public final SimplePoint start;
        public final SimplePoint end;

        public SimpleLine(SimplePoint start, SimplePoint end) {
            this.start = start;
            this.end = end;
        }
    }

    public static class SimpleCircle {
        public SimplePoint center;
        public float radius;

        public SimpleCircle(SimplePoint center, float radius) {
            this.center = center;
            this.radius = radius;
        }

        public SimpleCircle() {
            center = new SimplePoint(0f, 0f, 0f);
            radius = 0f;
        }

        public void setCenter(float cx, float cy, float cz) {
            center.setCoordinates(cx, cy, cz);
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public SimpleCircle scale(float scale) {
            return new SimpleCircle(center, radius * scale);
        }
    }

    public static class PolygonSimple {
        protected int numberOfEdges;
        protected java.util.Vector<SimplePoint> vertices;

        public PolygonSimple(int numberOfEdges) {
            this.numberOfEdges = numberOfEdges;
            this.vertices = new java.util.Vector(numberOfEdges);
            for (int i = 0; i < numberOfEdges; i++) this.vertices.add(new SimplePoint());
        }

        public void copyCoordinates(int index, SimplePoint point) {
            vertices.get(index).setCoordinates(point.x, point.y, point.z);
        }

        public void setCoordinates(int index, float x, float y, float z) {
            vertices.get(index).setCoordinates(x, y, z);
        }

        public SimplePoint getVertex(int index) {
            return vertices.get(index);
        }
    }

    public static class Triangle extends PolygonSimple {
        public Triangle() {
            super(3);
        }
    }

    public static class Cylinder {
        public final SimplePoint center;
        public final float radius;
        public final float height;

        public Cylinder(SimplePoint center, float radius, float height) {
            this.center = center;
            this.radius = radius;
            this.height = height;
        }
    }

    public static class Ray {
        //public final SimplePoint point;
        //public final Vector vector;
        public final SimplePoint point;
        public final Vector vector;

        public Ray(SimplePoint point, Vector vector) {
            this.point = point;
            this.vector = vector;
        }
    }

    public static class Vector  {
        //public final float x, y, z;
        public float x, y, z;

        public Vector(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector() {
            x = y = z = 0f;
        }

        public float length() {
            return (float) Math.sqrt(x * x + y * y + z * z);
        }

        public Vector crossProduct(Vector other) {
            return new Vector(
                    (y * other.z) - (z * other.y),
                    (z * other.x) - (x * other.z),
                    (x * other.y) - (y * other.x));
        }

        public float dotProduct(Vector other) {
            return x * other.x + y * other.y + z * other.z;
        }

        public Vector scale(float f) {
            //return new Vector(x * f, y * f, z * f);
            x *= f;
            y *= f;
            z *= f;
            return this;
        }

        public void setComponents(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public void setComponents(Vector v) {
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
        }

        public void normalize() {
            float length = length();
            x /= length;
            y /= length;
            z /= length;
        }

        public void print(final String TAG, final String label) {
            Log.i(TAG, label + x + "   " + y + "   " + z);
        }

    }

    public static Vector vectorBetween(SimplePoint from, SimplePoint to) {
        return new Vector(to.x - from.x, to.y - from.y, to.z - from.z);
    }

    public static Vector inverseDirection(Vector vector) {
        return new Vector(-vector.x, -vector.y, -vector.z);
    }

    public static class Sphere {
        public final SimplePoint center;
        public final float radius;

        public Sphere(SimplePoint center, float radius) {
            this.center = center;
            this.radius = radius;
        }
    }

    public static boolean intersects(Sphere sphere, Ray ray) {
        return perpendicularDistanceBetween(sphere.center, ray) < sphere.radius;
    }

    public static float cosineBetweenVectors(Vector a, Vector b) {
        return a.dotProduct(b) / (a.length() * b.length());
    }

    public static boolean intersects(SimpleCircle circle, SimplePoint p) {
        float dx = p.x - circle.center.x;
        float dy = p.y - circle.center.y;
        float dz = p.z - circle.center.z;
        float distance = (float) Math.sqrt(dx*dx + dy*dy + dz*dz);
        return distance < circle.radius;
    }

    public static float perpendicularDistanceBetween(SimplePoint point, Ray ray) {
        Vector p1ToPoint = vectorBetween(ray.point, point);
        Vector p2ToPoint = vectorBetween(ray.point.translate(ray.vector), point);

        // The length of the cross product gives the area of an imaginary
        // parallelogram having the two vectors as sides. A parallelogram can be
        // thought of as consisting of two triangles, so this is the same as
        // twice the area of the triangle defined by the two vectors.
        // http://en.wikipedia.org/wiki/Cross_product#Geometric_meaning
        float areaOfTriangleTimesTwo = p1ToPoint.crossProduct(p2ToPoint).length();
        float lengthOfBase = ray.vector.length();

        // The area of a triangle is also equal to (base * height) / 2. In
        // other words, the height is equal to (area * 2) / base. The height
        // of this triangle is the distance from the point to the ray.
        return areaOfTriangleTimesTwo / lengthOfBase;
    }

    // Note that this formula treats Ray as if it extended infinitely past
    // either point.
    public static float distanceBetween(SimplePoint point, Ray ray) {
        Vector p1ToPoint = vectorBetween(ray.point, point);
        Vector p2ToPoint = vectorBetween(ray.point.translate(ray.vector), point);

        // The length of the cross product gives the area of an imaginary
        // parallelogram having the two vectors as sides. A parallelogram can be
        // thought of as consisting of two triangles, so this is the same as
        // twice the area of the triangle defined by the two vectors.
        // http://en.wikipedia.org/wiki/Cross_product#Geometric_meaning
        float areaOfTriangleTimesTwo = p1ToPoint.crossProduct(p2ToPoint).length();
        float lengthOfBase = ray.vector.length();

        // The area of a triangle is also equal to (base * height) / 2. In
        // other words, the height is equal to (area * 2) / base. The height
        // of this triangle is the distance from the point to the ray.
        float distanceFromPointToRay = areaOfTriangleTimesTwo / lengthOfBase;

        // Now checks the distances from the point to the begin and end of the ray
        //float distanceFromPointToRaysBegin = p1ToPoint.length();
        //float distanceFromPointToRaysEnd = p2ToPoint.length();
        float dotProductP1ToPointAndRaysVector = p1ToPoint.dotProduct(ray.vector);
        float dotProductP2ToPointAndRaysVector = p2ToPoint.dotProduct(inverseDirection(ray.vector));

        if (dotProductP1ToPointAndRaysVector < 0f) distanceFromPointToRay = p1ToPoint.length();
        if (dotProductP2ToPointAndRaysVector < 0f) distanceFromPointToRay = p2ToPoint.length();

        return distanceFromPointToRay;
    }

    public static float distanceBetweenPoints(SimplePoint a, SimplePoint b) {
        if ((null == a) || (null == b)) return -1f;
        return (float)Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)
                + Math.pow(a.z - b.z, 2));
    }

    public static SimplePoint middlePoint(SimplePoint a, SimplePoint b) {
        if ((null == a) || (null == b)) return null;
        return new SimplePoint(0.5f*(a.x + b.x), 0.5f*(a.y + b.y), 0.5f*(a.z + b.z));
    }

    // http://en.wikipedia.org/wiki/Line-plane_intersection
    // This also treats rays as if they were infinite. It will return a
    // point full of NaNs if there is no intersection point.
    public static SimplePoint intersectionPoint(Ray ray, Plane plane) {
        Vector rayToPlaneVector = vectorBetween(ray.point, plane.point);

        float scaleFactor = rayToPlaneVector.dotProduct(plane.normal) / ray.vector.dotProduct(plane.normal);

        SimplePoint intersectionPoint = ray.point.translate(ray.vector.scale(scaleFactor));
        return intersectionPoint;
    }

    public static class Plane {
        public final SimplePoint point;
        public final Vector normal;

        public Plane(SimplePoint point, Vector normal) {
            this.point = point;
            this.normal = normal;
        }
    }

    public static class Rectangle extends Surface {
        private float width;
        private float height;
        private float centerX, centerY;
        private Node lowerNode;
        private Node upperNode;

        public void deleteWholeShapeFromGeometry(GeometryData geometry) {
            // Deletes the surfaces,lines and points/nodes
            deleteWholeSurfaceFromGeometry(geometry);
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(width);
                dos.writeFloat(height);
                dos.writeFloat(centerX);
                dos.writeFloat(centerY);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                width = dis.readFloat();
                height = dis.readFloat();
                centerX = dis.readFloat();
                centerY = dis.readFloat();

                create();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public Rectangle() {
            super();
            width = height = 0f;
            centerX = centerY = 0f;
            lowerNode = upperNode = null;

            shapeInfo.specialShape = true;
            shapeInfo.typeOfSpecialShape = Constants.TYPE_SHAPE_RECTANGLE;
            shapeInfo.rectangle = this;
        }

        public Rectangle(float centerX, float centerY, float width, float height) {
            // Initial values
            super();
            this.width = width;
            this.height = height;
            this.centerX = centerX;
            this.centerY = centerY;
            lowerNode = null;
            upperNode = null;

            create();
        }

        public void create() {
            createPoints();
            createSides();
            createSurface();

            // We report this figure is not any figure, is a rectangle
            shapeInfo.specialShape = true;
            shapeInfo.typeOfSpecialShape = Constants.TYPE_SHAPE_RECTANGLE;
            shapeInfo.rectangle = this;
        }

        public void changeDimensions(float centerX, float centerY, float width, float height) {
            this.width = width;
            this.height = height;
            this.centerX = centerX;
            this.centerY = centerY;
            changeCoordinatesNodes();
            // We have to create a new mesh
            //meshSurface(); MISSING STEPS!!!!
        }

        // Used when the the data has been read from a file
        private void rebuild(Node lowerNode, Node upperNode, Points points) {
            rebuildPoints(lowerNode, upperNode, points);
        }

        private void rebuildPoints(Node lowerNode, Node upperNode, Points geometryPoints) {
            Point point;
            Node node;

            // Lower control node
            this.lowerNode = lowerNode;
            points.add(lowerNode.getPoint());
            pointsIds.add(lowerNode.getPoint().getId());

            point = new Point(centerX + 0.5f * width, centerY - 0.5f * height, 0f);
            geometryPoints.addElement(point); // This set a right id for the new point
            points.add(point);
            pointsIds.add(point.getId());

            // Upper control node
            this.upperNode = upperNode;
            points.add(upperNode.getPoint());
            pointsIds.add(upperNode.getPoint().getId());

            point = new Point(centerX - 0.5f * width, centerY + 0.5f * height, 0f);
            geometryPoints.addElement(point); // This set a right id for the new point
            points.add(point);
            pointsIds.add(point.getId());
        }

        private void createPoints() {
            // The ids of the nodes' points have to be specified because they are used in other places.
            // Even though this order is temporal. The ids are changed when we add the nodes to
            // the geometry.
            Point point;
            Node node;

            // Lower control node
            node = new Node(centerX - 0.5f * width, centerY - 0.5f * height, 0f);
            node.setId(0);
            node.getPoint().setId(0);
            points.add(node.getPoint());
            pointsIds.add(0);
            lowerNode = node;

            point = new Point(centerX + 0.5f * width, centerY - 0.5f * height, 0f);
            point.setId(1);
            points.add(point);
            pointsIds.add(1);

            // Upper control node
            node = new Geometry.Node(centerX + 0.5f * width, centerY + 0.5f * height, 0f);
            node.setId(2);
            node.getPoint().setId(2);
            points.add(node.getPoint());
            pointsIds.add(2);
            upperNode = node;

            point = new Point(centerX - 0.5f * width, centerY + 0.5f * height, 0f);
            point.setId(3);
            points.add(point);
            pointsIds.add(3);
        }

        private void changeCoordinatesNodes() {
            points.get(0).setCoordinates(centerX - 0.5f * width, centerY - 0.5f * height, 0f);
            points.get(1).setCoordinates(centerX + 0.5f*width, centerY - 0.5f * height, 0f);
            points.get(2).setCoordinates(centerX + 0.5f * width, centerY + 0.5f * height, 0f);
            points.get(3).setCoordinates(centerX - 0.5f*width, centerY + 0.5f*height, 0f);
        }

        private void createSides() {
            // The ids have to be specified because they are used in other places.
            // Even though this order is temporal. The ids are changed when we add the segments to
            // the geometry.
            Line line;

            line = new Geometry.Line(points.get(0), points.get(1));
            line.setId(0);
            lines.add(line);

            line = new Geometry.Line(points.get(1), points.get(2));
            line.setId(1);
            lines.add(line);

            line = new Geometry.Line(points.get(2), points.get(3));
            line.setId(2);
            lines.add(line);

            line = new Geometry.Line(points.get(3), points.get(0));
            line.setId(3);
            lines.add(line);
        }

        private void createSurface() {
            // To report that the construction of the surface has finished
            commit();
            // Mesh it to later draw it with OpenGL
            setModel(createModel(this));
            meshSurface();
        }

        public void addToGeometry(GeometryData geometry) {
            // When we add the nodes and the sides to the geometry, the ids will change.
            for (int i = 0; i < lines.size(); i++) {
                geometry.addPoint(points.get(i));
                geometry.addLine(lines.get(i));
            }
            geometry.addSurface(this);
            geometry.addRectangle(this);
            // Build again the set of ids, because they changed
            pointsIds.clear();
            Iterator<Point> it = points.iterator();
            while (it.hasNext()) pointsIds.add(it.next().getId());
        }

        public void handleControlNodeMovement(Node node, float x, float y, boolean hasDragStopped) {
            // If the center node is being moved then, we move the circle
            if (node.getId() == lowerNode.getId()) handleLowerNodeDrag(x, y);
            // If the size node is being moved, then we increase o decrease the circle's size
            if (node.getId() == upperNode.getId()) handleUpperNodeDrag(x, y);

            if (hasDragStopped) {
                setModel(createModel(this));
                meshSurface();
            }
        }

        private void handleLowerNodeDrag(float x, float y) {
            Point p;

            lowerNode.SetCoordinates(x, y, lowerNode.GetZ());

            p = points.get(1);
            p.setCoordinates(p.x, y, p.z);

            p = points.get(3);
            p.setCoordinates(x, p.y, p.z);
        }

        private void handleUpperNodeDrag(float x, float y) {
            Point p;

            upperNode.SetCoordinates(x, y , upperNode.GetZ());

            p = points.get(1);
            p.setCoordinates(x, p.y, p.z);

            p = points.get(3);
            p.setCoordinates(p.x, y, p.z);
        }
    }

    public static class RegularPolygon extends Surface {
        private float innerAngle;
        private int numberOfSides;
        private Node centerNode;
        private Node apothemNode;

        public void deleteWholeShapeFromGeometry(GeometryData geometry) {
            // Deletes the surfaces,lines and points/nodes
            deleteWholeSurfaceFromGeometry(geometry);
            geometry.deleteNode(centerNode);
            geometry.deleteNode(apothemNode);
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeInt(numberOfSides);
                dos.writeFloat(centerNode.GetX());
                dos.writeFloat(centerNode.GetY());
                dos.writeFloat(apothemNode.GetX());
                dos.writeFloat(apothemNode.GetY());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            float centerX, centerY, apothemX, apothemY;
            try {
                numberOfSides = dis.readInt();
                centerX = dis.readFloat();
                centerY = dis.readFloat();
                apothemX = dis.readFloat();
                apothemY = dis.readFloat();
                centerNode = new Node(centerX, centerY, 0);
                apothemNode = new Node(apothemX, apothemY, 0);
                create();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public RegularPolygon() {
            super();
            numberOfSides = 0;
            centerNode = null;
            apothemNode = null;
        }

        public RegularPolygon(int numberOfSides,
                              float centerX, float centerY,
                              float apothemX, float apothemY ) {
            super();
            this.numberOfSides = numberOfSides;
            centerNode = new Node(centerX, centerY, 0);
            apothemNode = new Node(apothemX, apothemY, 0);

            create();
        }

        private void create() {
            innerAngle = (float)(2.0*Math.PI / (double)numberOfSides);

            createPoints();
            createSides();
            createSurface();

            // We report this figure is not any figure, is a regular polygon
            shapeInfo.specialShape = true;
            shapeInfo.typeOfSpecialShape = Constants.TYPE_SHAPE_REGULAR_POLYGON;
            shapeInfo.regularPolygon = this;

            centerNode.getPoint().surfaceProperties.setSurface(this);
            apothemNode.getPoint().surfaceProperties.setSurface(this);
        }

        private void createPoints() {
            float angle, radius, apothem;
            Point point;

            apothem = (float)Math.sqrt(Math.pow(apothemNode.GetX() - centerNode.GetX(), 2) +
                    Math.pow(apothemNode.GetY() - centerNode.GetY(), 2));

            angle = (float)Math.atan2(apothemNode.GetY() - centerNode.GetY(), apothemNode.GetX() - centerNode.GetX());
            radius = apothem / (float)Math.cos(0.5*innerAngle);

            angle += 0.5f*innerAngle;
            for (int i = 0; i < numberOfSides; i++) {
                point = new Point(radius*(float)Math.cos(angle) + centerNode.GetX(),
                        radius*(float)Math.sin(angle) + centerNode.GetY(), 0f);
                // We have to specify a correct id for the nodes' points
                point.setId(i);
                points.add(point);
                pointsIds.add(i);
                angle += innerAngle;
            }
        }

        private void createSides() {
            Line line;
            for (int i = 0; i < numberOfSides - 1; i++) {
                line = new Line(points.get(i), points.get(i+1));
                line.setId(i);
                lines.add(line);
            }
            line = new Line(points.get(numberOfSides-1), points.get(0));
            line.setId(numberOfSides - 1);
            lines.add(line);
        }

        private void createSurface() {
            // To report that the construction of the surface has finished
            commit();
            setModel(createModel(this));
            meshSurface();
        }

        public void addToGeometry(GeometryData geometry){
            geometry.addNode(centerNode);
            geometry.addNode(apothemNode);
            for (int i = 0; i < numberOfSides; i++){
                geometry.addPoint(points.get(i));
                geometry.addLine(lines.get(i));
            }
            geometry.addSurface(this);
            geometry.addRegularPolygon(this);
            // Build again the set of ids, because they changed
            pointsIds.clear();
            Iterator<Point> it = points.iterator();
            while (it.hasNext()) pointsIds.add(it.next().getId());
        }

        public void handleControlNodeMovement(Node node, float x, float y, boolean hasDragStopped) {
            // If the center node is being moved then, we move the circle
            if (node.getId() == centerNode.getId()) handleCenterNodeDrag(x, y);
            // If the size node is being moved, then we increase o decrease the circle's size
            if (node.getId() == apothemNode.getId()) handleApothemNodeDrag(x, y);

            if (hasDragStopped) {
                setModel(createModel(this));
                meshSurface();
            }
        }

        private void handleCenterNodeDrag(float x, float y) {
            float dx, dy;
            dx = x - centerNode.GetX();
            dy = y - centerNode.GetY();

            centerNode.translate(dx, dy, 0);
            apothemNode.translate(dx, dy, 0);

            Iterator<Point> it = points.iterator();
            while (it.hasNext()) it.next().translate(dx, dy, 0);
        }

        private void handleApothemNodeDrag(float x, float y) {
            float angle, radius, apothem;

            apothemNode.SetCoordinates(x, y, apothemNode.GetZ());

            apothem = (float)Math.sqrt(Math.pow(apothemNode.GetX() - centerNode.GetX(), 2) +
                    Math.pow(apothemNode.GetY() - centerNode.GetY(), 2));

            angle = (float)Math.atan2(apothemNode.GetY() - centerNode.GetY(),
                    apothemNode.GetX() - centerNode.GetX());
            radius = apothem / (float)Math.cos(0.5*innerAngle);

            angle += 0.5f*innerAngle;
            for (int i = 0; i < numberOfSides; i++) {
                points.get(i).setCoordinates(radius*(float)Math.cos(angle) + centerNode.GetX(),
                        radius*(float)Math.sin(angle) + centerNode.GetY(), 0f);
                angle += innerAngle;
            }
        }
    }

    public static class Arc extends Line {
        private float radius;
        private float angle;
        private Node centerNode;
        private Node sizeNode;
        private int numberOfEdges;
        private Line[] edges;
        private Point[] points;

        public Arc(float angleInDegrees, int numberOfEdges, float centerX, float centerY,
                   float pointX, float pointY) {
            super();

            if (angleInDegrees < -360f) angleInDegrees = -360f;
            if (angleInDegrees > 360f) angleInDegrees = 360f;
            if (numberOfEdges < 1) numberOfEdges = 1;

            this.numberOfEdges = numberOfEdges;
            this.angle = (float)Math.toRadians(angleInDegrees);
            this.centerNode = new Node(centerX, centerY, 0f);
            this.sizeNode = new Node(pointX, pointY, 0f);
            this.radius = (float)Math.sqrt(Math.pow(pointX - centerX, 2) + Math.pow(pointY - centerY, 2));
            memory();
            create();
        }

        public Line[] getEdges() { return edges; }
        public Point[] getPoints() { return points; }
        public int getNumberOfEdges() { return numberOfEdges; }
        public Node getCenterNode() { return centerNode; }
        public Node getSizeNode() { return sizeNode; }

        private void memory() {
            edges = new Line[numberOfEdges];
            points = new Point[numberOfEdges]; // The first point is a node
            for (int i = 0; i < numberOfEdges; i++) {
                points[i] = new Point();
                edges[i] = new Line();
            }
        }

        // Creates the edges, points and nodes for the arc
        private void create() {
            float currentAngle, stepAngle, x, y;

            stepAngle = angle / (float)numberOfEdges;
            currentAngle = (float)Math.atan2(sizeNode.GetY() - centerNode.GetY(),
                                             sizeNode.GetX() - centerNode.GetX());

            // The first edge is defined by a node and a point
            currentAngle += stepAngle;
            x = centerNode.GetX() + radius*(float)Math.cos(currentAngle);
            y = centerNode.GetY() + radius*(float)Math.sin(currentAngle);
            points[0].setCoordinates(x, y, 0f);
            edges[0].setPoints(sizeNode.getPoint(), points[0]);

            for (int i = 1; i < numberOfEdges; i++) {
                currentAngle += stepAngle;
                x = centerNode.GetX() + radius * (float)Math.cos(currentAngle);
                y = centerNode.GetY() + radius*(float)Math.sin(currentAngle);
                points[i].setCoordinates(x, y, 0f);
                edges[i].setPoints(points[i-1], points[i]);
            }
        }

        public void updatePosition(float centerX, float centerY) {
            float dx, dy;

            dx = centerX - centerNode.GetX();
            dy = centerY - centerNode.GetY();

            centerNode.SetCoordinates(centerNode.GetX() + dx, centerNode.GetY() + dy, 0f);
            sizeNode.SetCoordinates(sizeNode.GetX() + dx, sizeNode.GetY() + dy, 0f);

            for (int i = 0; i < numberOfEdges; i++)
                points[i].setCoordinates(points[i].getX() + dx, points[i].getY() + dy, 0f);
        }

        // Here we are using the same number of sides
        public void updateSize(float newSizeX, float newSizeY) {
            float currentAngle, stepAngle, x, y;
            sizeNode.SetCoordinates(newSizeX, newSizeY, 0f);

            radius = (float)Math.sqrt(Math.pow(sizeNode.GetX() - centerNode.GetX(), 2) +
                                      Math.pow(sizeNode.GetY() - centerNode.GetY(), 2));

            stepAngle = angle / (float)numberOfEdges;
            currentAngle = (float)Math.atan2(sizeNode.GetY() - centerNode.GetY(),
                                             sizeNode.GetX() - centerNode.GetX());

            currentAngle += stepAngle;
            x = centerNode.GetX() + radius*(float)Math.cos(currentAngle);
            y = centerNode.GetY() + radius*(float)Math.sin(currentAngle);
            points[0].setCoordinates(x, y, 0f);

            for (int i = 1; i < numberOfEdges; i++) {
                currentAngle += stepAngle;
                x = centerNode.GetX() + radius * (float)Math.cos(currentAngle);
                y = centerNode.GetY() + radius*(float)Math.sin(currentAngle);
                points[i].setCoordinates(x, y, 0f);
            }
        }
    }

    public static class Circle extends Surface {
        private float radius;
        private Arc arc;
        private int numberOfSides;
        private Node centerNode;
        private Node sizeNode;
        private Line lastEdge;

        public void deleteWholeShapeFromGeometry(GeometryData geometry) {
            // Deletes the surfaces,lines and points/nodes
            deleteWholeSurfaceFromGeometry(geometry);
            // Deletes the control nodes that do not are in the shape's boundary
            geometry.deleteNode(centerNode);
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(centerNode.GetX());
                dos.writeFloat(centerNode.GetY());
                dos.writeFloat(sizeNode.GetX());
                dos.writeFloat(sizeNode.GetY());
                dos.writeInt(numberOfSides);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            SimplePoint centerPoint = new SimplePoint();
            SimplePoint sizePoint   = new SimplePoint();
            float x, y;
            try {
                x = dis.readFloat();
                y = dis.readFloat();
                centerPoint.setCoordinates(x, y, 0);
                x = dis.readFloat();
                y = dis.readFloat();
                sizePoint.setCoordinates(x, y ,0);
                numberOfSides = dis.readInt();

                create(centerPoint, sizePoint);

                this.centerNode = arc.centerNode;
                this.sizeNode = arc.sizeNode;
                // We tell the nodes they belong to this circle
                this.centerNode.getPoint().surfaceProperties.setSurface(this);
                this.sizeNode.getPoint().surfaceProperties.setSurface(this);

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public Circle() {
            super();
            radius = 0;
            numberOfSides = 0;
            arc = null;
            centerNode = null;
            sizeNode = null;
            lastEdge = null;
        }

        public Circle(SimplePoint centerPoint, SimplePoint sizePoint, int numberOfSides) {
            this.radius = (float)Math.sqrt(Math.pow(sizePoint.x - centerPoint.x, 2) +
                                           Math.pow(sizePoint.y - centerPoint.y, 2));
            this.numberOfSides = numberOfSides;

            create(centerPoint, sizePoint);

            this.centerNode = arc.centerNode;
            this.sizeNode = arc.sizeNode;
            // We tell the nodes they belong to this circle
            this.centerNode.getPoint().surfaceProperties.setSurface(this);
            this.sizeNode.getPoint().surfaceProperties.setSurface(this);
        }

        private void create(SimplePoint centerPoint, SimplePoint sizePoint) {
            // Creates an arc with an angle of almost 360 degrees
            arc = new Arc(360f - 360f/(float)numberOfSides, numberOfSides - 1, centerPoint.x,
                    centerPoint.y, sizePoint.x, sizePoint.y);

            // We have to add temporal ids for the points just to construct correctly the model
            arc.sizeNode.point.id = 0;
            for (int i = 0; i < arc.edges.length; i++) {
                arc.points[i].id = i + 1;
                arc.edges[i].Id = i;
                addLine(arc.edges[i]);
            }

            // Adds the line to join the last point of the arc its beginning
            lastEdge = new Line(arc.points[arc.points.length-1], arc.sizeNode.point);
            addLine(lastEdge);

            // To report that the construction of the surface has finished
            commit();
            setModel(createModel(this));
            meshSurface();

            // We report this figure is not any figure, is a circle
            shapeInfo.specialShape = true;
            shapeInfo.typeOfSpecialShape = Constants.TYPE_SHAPE_CIRCLE;
            shapeInfo.circle = this;
        }

        public void addToGeometry(GeometryData geometry) {
            // Nodes
            geometry.addNode(centerNode);
            geometry.addNode(sizeNode);
            //*
            // Points and edges
            for (int i = 0; i < arc.points.length; i++){
                geometry.addPoint(arc.points[i]);
                geometry.addLine(arc.edges[i]);
            }
            geometry.addLine(lastEdge);
            // Surface
            geometry.addSurface(this);
            geometry.addCircle(this);
            // Build again the set of ids, because they changed
            pointsIds.clear();
            Iterator<Point> it = points.iterator();
            while (it.hasNext()) pointsIds.add(it.next().getId());
        }

        public void handleControlNodeMovement(Node node, float x, float y, boolean hasDragStopped) {
            // If the center node is being moved then, we move the circle
            if (node.getId() == centerNode.getId()) arc.updatePosition(x, y);
            // If the size node is being moved, then we increase o decrease the circle's size
            if (node.getId() == sizeNode.getId()) arc.updateSize(x,y);

            if (hasDragStopped) {
                setModel(createModel(this));
                meshSurface();
            }
        }

    }

    public static class Zoom {
        // NDC -> Normalized device coordinates
        // WSC -> World space coordinates
        private final float INVALID_DISTANCE = -1f;
        private float initialRadiusInNDC;
        private float initialScaleFactor;
        private SimplePoint zoomingCenterInNDC;
        private SimplePoint zoomingCenterInWSC;
        private boolean initialData;
        float[] displFromZoomingCenterToCentersView;

        boolean hasStarted;
        boolean isBeingDone;
        boolean hasFinished;

        public Zoom() {
            initialRadiusInNDC = 0f;
            zoomingCenterInNDC = null;
            zoomingCenterInWSC = null;
            displFromZoomingCenterToCentersView = new float[2];
            initialData = false;
        }

        public void setData(float initialScaleFactor,
                            float initialRadiusInNDC,
                            SimplePoint zoomingCenterInNDC,
                            SimplePoint zoomingCenterInWSC,
                            SimplePoint centersViewInWSC) {
            this.initialScaleFactor = initialScaleFactor;
            this.initialRadiusInNDC = initialRadiusInNDC;
            this.zoomingCenterInNDC = zoomingCenterInNDC;
            this.zoomingCenterInWSC = zoomingCenterInWSC;
            displFromZoomingCenterToCentersView[0] = zoomingCenterInWSC.x - centersViewInWSC.x;
            displFromZoomingCenterToCentersView[1] = zoomingCenterInWSC.y - centersViewInWSC.y;
            initialData = true;
        }

        public float getScaleFactorFromPointsInNDC(SimplePoint a, SimplePoint b) {
            float radius, c, scaleFactor;

            // When the data has not been set the scale factor does nothing
            if (!initialData) return 1f;

            radius = getRadiusModelA(a, b);

            // When the points a and b are not valid the scale factor does nothing
            if (radius < 0f) return 1f;

            //c = radius / initialRadiusInNDC;
            c = initialRadiusInNDC / radius;

            scaleFactor = Constants.ZOOMING_FACTOR / c * initialScaleFactor;
            if (scaleFactor > Constants.MAXIMUM_ZOOM) scaleFactor = Constants.MAXIMUM_ZOOM;
            if (scaleFactor < Constants.MINIMUM_ZOOM) scaleFactor = Constants.MINIMUM_ZOOM;

            return scaleFactor;
        }

        private float getRadiusModelA(SimplePoint aInNDC, SimplePoint bInNDC) {
            float radius = 0.5f*Geometry.distanceBetweenPoints(aInNDC, bInNDC);
            if (radius < 0) return INVALID_DISTANCE;
            return radius;
        }

        private float getRadiusModelB(SimplePoint aInNDC, SimplePoint bInNDC) {
            float radiusA, radiusB;

            if (!initialData) return INVALID_DISTANCE;

            radiusA = Geometry.distanceBetweenPoints(aInNDC, zoomingCenterInNDC);
            radiusB = Geometry.distanceBetweenPoints(bInNDC, zoomingCenterInNDC);

            // Invalid radius
            if ((radiusA < 0) || (radiusB < 0)) return INVALID_DISTANCE;

            return 0.5f*(radiusA + radiusB);
        }

        public SimplePoint getZoomingCenterInNDC() { return zoomingCenterInNDC; }
        public SimplePoint getZoomingCenterInWSC() { return zoomingCenterInWSC; }

        public float getDisplacementToCentersViewOnX() {
            return displFromZoomingCenterToCentersView[0];
        }
        public float getDisplacementToCentersViewOnY() {
            return displFromZoomingCenterToCentersView[1];
        }
    }

    public static void scaleView(SimplePoint previousP1NDC, SimplePoint previousP2NDC,
                                 SimplePoint currentP1NDC, SimplePoint currentP2NDC,
                                 float[] viewMatrix,
                                 float[] projectionMatrix,
                                 PlotParameters plotParameters) {
        // We have to do the rotation and projection so the firstTouchPoint and secondTouchPoint have
        // the same position as the pointers (fingers). This means the user may be doing
        // zoom and/or rotating at the same time.

        float[] invertedViewProjectionMatrix = new float[16];
        float[] viewProjectionMatrix = new float[16];
        float[] x = new float[4];
        float[] a = new float[4];

        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        invertM(invertedViewProjectionMatrix, 0, viewProjectionMatrix, 0);

        SimplePoint previousP1WSC = MeshRenderer.convertNormalized2DPointToWorldSpace2DPoint(
                previousP1NDC.x,
                previousP1NDC.y,
                invertedViewProjectionMatrix
        );

        Vector previousNDC = new Vector(
                previousP2NDC.x - previousP1NDC.x,
                previousP2NDC.y - previousP1NDC.y,
                previousP2NDC.z - previousP1NDC.z
        );

        Vector currentNDC = new Vector(
                currentP2NDC.x - currentP1NDC.x,
                currentP2NDC.y - currentP1NDC.y,
                currentP2NDC.z - currentP1NDC.z
        );

        float cosine = Geometry.cosineBetweenVectors(previousNDC, currentNDC);
        if (Math.abs(cosine) > 1){
            if (cosine > 0) cosine = 1;
            else            cosine = -1;
        }
        float angle = (float)Math.acos(cosine);
        Vector sign = previousNDC.crossProduct(currentNDC);
        if (sign.z < 0) angle *= (-1);

        addRotationAroundArbitraryAxis(
                previousP1WSC.x,
                previousP1WSC.y,
                previousP1WSC.z,
                0, 0, 1, angle, viewMatrix);

        float scaleFactor = currentNDC.length() / previousNDC.length();
        plotParameters.setZoomFactor(plotParameters.getZoomFactor() * scaleFactor);

        float w = plotParameters.getMaxX() - plotParameters.getMinX();
        float h = plotParameters.getMaxY() - plotParameters.getMinY();

        x[0] = previousP1WSC.x;
        x[1] = previousP1WSC.y;
        x[2] = previousP1WSC.z;
        x[3] = 1;
        multiplyMV(a, 0, viewMatrix, 0, x, 0);

        float cx = 0.5f*(2f*a[0] - w*currentP1NDC.x);
        float cy = 0.5f*(2f*a[1] - h*currentP1NDC.y);

        plotParameters.setCenter(cx, cy, plotParameters.getCenterZ());

        // We update the projection matrix
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
               plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
    }

    public static void addRotationAroundArbitraryAxis(float a, float b, float c,
                                                      float x, float y, float z,
                                                      float angleInRadians,
                                                      float[] rotationMatrix) {
        float[] newRotation = new float[16];
        float[] aux = new float[16];

        setIdentityM(newRotation, 0);
        getRotationMatrixAroundArbitraryAxis(a, b, c, x, y, z, angleInRadians, newRotation);
        for (int i = 0; i < 16; i++) aux[i] = rotationMatrix[i];

        //MeshRenderer.printSquareMatrixSize4("SCALEVIEW", "New rotation", newRotation);

        multiplyMM(rotationMatrix, 0, newRotation, 0, aux, 0); // Maybe the order is the opposite
    }

    public static float length(float x, float y, float z) {
        return (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public static void getRotationMatrixAroundArbitraryAxis(float a, float b, float c,
                                                            float x, float y, float z,
                                                            float theta,
                                                            float[] R) {
        String TAG = "RotatingObject";
        boolean printInfo = false;

        float l = length(x, y, z);
        if (l < 1e-10){
            Log.i(TAG, "Vector too short!!");
            return;
        }

        // In this instance we normalize the direction vector.
        float u = x/l;
        float v = y/l;
        float w = z/l;

        if (printInfo) {
            Log.i(TAG, "Point = " + a + "  " + b + "  " + c);
            Log.i(TAG, "Axis  = " + x + "  " + y + "  " + z);
            Log.i(TAG, "Norm. axis  = " + u + "  " + v + "  " + w);
            Log.i(TAG, "Angle = " + theta);
        }

        // Set some intermediate values.
        float u2 = u*u;
        float v2 = v*v;
        float w2 = w*w;
        float cosT = (float)Math.cos(theta);
        float oneMinusCosT = 1f - cosT;
        float sinT = (float)Math.sin(theta);

        // Build the matrix entries element by element.
        R[0] = u2 + (v2 + w2) * cosT;
        R[4] = u*v * oneMinusCosT - w*sinT;
        R[8] = u*w * oneMinusCosT + v*sinT;
        R[12] = (a*(v2 + w2) - u*(b*v + c*w))*oneMinusCosT
                + (b*w - c*v)*sinT;

        R[1] = u*v * oneMinusCosT + w*sinT;
        R[5] = v2 + (u2 + w2) * cosT;
        R[9] = v*w * oneMinusCosT - u*sinT;
        R[13] = (b*(u2 + w2) - v*(a*u + c*w))*oneMinusCosT
                + (c*u - a*w)*sinT;

        R[2] = u*w * oneMinusCosT - v*sinT;
        R[6] = v*w * oneMinusCosT + u*sinT;
        R[10] = w2 + (u2 + v2) * cosT;
        R[14] = (c*(u2 + v2) - w*(a*u + b*v))*oneMinusCosT
                + (a*v - b*u)*sinT;

        R[3] = 0f;
        R[7] = 0f;
        R[11] = 0f;
        R[15] = 1f;
    }

    public static class ExtrudeLine {
        private Line lineToExtrude;
        private Line previousLine;
        private Line nextLine;
        private Vector previousNormalVector, nextNormalVector, lineToExtrudeNormalVector;
        private boolean canBeExtruded;
        private float tolerance;
        private Point point1, point2;
        private int dragID;
        private SimplePoint initialPositionFirstPoint;
        private SimplePoint initialPositionLastPoint;
        boolean updateView;

        private void setInitialValuesForVariables() {
            lineToExtrude = previousLine = nextLine = null;
            previousNormalVector = nextNormalVector = lineToExtrudeNormalVector = null;
            canBeExtruded = false;
            tolerance = 0.0001f;
            point1 = point2 = null;
            dragID = Constants.INVALID_ID;
            initialPositionFirstPoint = null;
            initialPositionLastPoint = null;
            updateView = true;
        }

        public ExtrudeLine() {
            setInitialValuesForVariables();
        }

        public boolean startExtrude(Line line, GeometryData geometry, int dragID) {
            lineToExtrude = line;
            initialPositionFirstPoint = new SimplePoint();
            initialPositionLastPoint = new SimplePoint();
            initialPositionFirstPoint.setCoordinates(lineToExtrude.getFirstPoint());
            initialPositionLastPoint.setCoordinates(lineToExtrude.getLastPoint());

            getAdjacentLines();

            //*
            Log.i("EXTRUDE", "previousLine ID = " + previousLine.getId());
            Log.i("EXTRUDE", "lineToExtrudeID = " + lineToExtrude.getId());
            Log.i("EXTRUDE", "lineToExtrude nodes IDs = " + lineToExtrude.getFirstPoint().getId() + "   " + lineToExtrude.getLastPoint().getId());
            Log.i("EXTRUDE", "nextLine ID     = " + nextLine.getId());
            Log.i("EXTRUDE", "nexLine nodes IDs = " + nextLine.getFirstPoint().getId() + "   " + nextLine.getLastPoint().getId());
            //*/

            if ((null != previousLine) && (null != nextLine)) {
                canBeExtruded = true;
                getPointsToBeMoved(geometry);
                this.dragID = dragID;
            }

            return canBeExtruded;
        }

        public int getDragID() { return dragID; }

        // Gets the perpendicular displacement to the line that will be extruded
        private Vector getDisplacement(float pointerPositionOnX, float pointerPositionOnY) {
            float perpendicularDistanceToPointer;

            Vector displacement = new Vector();

            Vector relativePointerPosition = new Vector(
                    pointerPositionOnX - initialPositionFirstPoint.x,
                    pointerPositionOnY - initialPositionFirstPoint.y,
                    0f
            );
            SimplePoint linePoint = new SimplePoint(initialPositionFirstPoint);
            Vector lineVector = new Vector(
                    initialPositionLastPoint.x - initialPositionFirstPoint.x,
                    initialPositionLastPoint.y - initialPositionFirstPoint.y,
                    initialPositionLastPoint.z - initialPositionFirstPoint.z
            );
            Ray lineRay = new Ray(linePoint, lineVector);

            SimplePoint pointerPosition = new SimplePoint(pointerPositionOnX, pointerPositionOnY, 0f);

            perpendicularDistanceToPointer = Geometry.perpendicularDistanceBetween(pointerPosition, lineRay);

            displacement.setComponents(lineToExtrudeNormalVector);
            displacement.scale(perpendicularDistanceToPointer);
            if (relativePointerPosition.dotProduct(lineToExtrudeNormalVector) < 0)
                displacement.scale(-1f);

            return displacement;
        }

        public void extrude(float pointerPositionOnX, float pointerPositionOnY,
                            boolean meshAgainSurface) {
            if (!canBeExtruded) return;
            Vector displacement;

            displacement = getDisplacement(pointerPositionOnX, pointerPositionOnY);
            //displacement.print("EXTRUDE", "Displacement = ");

            point1.setCoordinates(
                    initialPositionFirstPoint.x + displacement.x,
                    initialPositionFirstPoint.y + displacement.y,
                    initialPositionFirstPoint.z + displacement.z
            );

            point2.setCoordinates(
                    initialPositionLastPoint.x + displacement.x,
                    initialPositionLastPoint.y + displacement.y,
                    initialPositionLastPoint.z + displacement.z
            );

            if (meshAgainSurface) {
                Surface surface = lineToExtrude.surfaceProperties.getSurface();
                surface.commit();
                surface.setModel(surface.createModel(surface));
                surface.meshSurface();
            }

            updateView = updateView ? false : true;
        }

        public void stopExtrude() {
            setInitialValuesForVariables();
        }

        private void getPointsToBeMoved(GeometryData geometry) {
            float dotProduct;
            Point point;
            Node node;

            getUnitNormalVectors();

            //*
            previousNormalVector.print("EXTRUDE",      "previousNormalVector      = ");
            lineToExtrudeNormalVector.print("EXTRUDE", "lineToExtrudeNormalVector = ");
            nextNormalVector.print("EXTRUDE",          "nextNormalVector          = ");
            //*/

            dotProduct = previousNormalVector.dotProduct(lineToExtrudeNormalVector);
            point = lineToExtrude.getFirstPoint();
            if (Math.abs(dotProduct) < tolerance) {
                point1 = point;
            } else {
                if (point.nodeInfo.isANode) {
                    node = new Node(point.getX(), point.getY(), point.getZ());
                    point1 = node.point;

                } else {
                    point1 = new Point(point.getX(), point.getY(), point.getZ());
                }
                changeFirstPointToLineToExtrude(point1, geometry);

                // Surface properties
                if (lineToExtrude.surfaceProperties.isItPartOfASurface())
                    point1.surfaceProperties.setSurface(lineToExtrude.surfaceProperties.getSurface());
            }

            dotProduct = nextNormalVector.dotProduct(lineToExtrudeNormalVector);
            point = lineToExtrude.getLastPoint();
            if (Math.abs(dotProduct) < tolerance)
                point2 = point;
            else {
                if (point.nodeInfo.isANode) {
                    node = new Node(point.getX(), point.getY(), point.getZ());
                    point2 = node.point;
                } else {
                    point2 = new Point(point.getX(), point.getY(), point.getZ());
                }
                changeLastPointToLineToExtrude(point2, geometry);

                // Surface properties
                if (lineToExtrude.surfaceProperties.isItPartOfASurface())
                    point2.surfaceProperties.setSurface(lineToExtrude.surfaceProperties.getSurface());
            }

            lineToExtrude.surfaceProperties.getSurface().commit();
        }

        private void changeFirstPointToLineToExtrude(Point point, GeometryData geometry) {
            Surface surface = lineToExtrude.surfaceProperties.getSurface();
            Line line;
            // We have to add the new node to the geometry and also add a new line in the surface
            geometry.addPoint(point);
            line = new Line(lineToExtrude.getFirstPoint(), point);
            geometry.addLine(line);
            // We have to connect the line to be extruded to this new node
            lineToExtrude.setFirstPoint(point);

            /*
            Log.i("EXTRUDE", "Line to extrude ID = " + lineToExtrude.getId());
            Log.i("EXTRUDE", "New line ID = " + line.getId());
            Log.i("EXTRUDE", "New line nodes IDs= " + line.getFirstPoint().getId() + "   " + line.getLastPoint().getId());
            Log.i("EXTRUDE", "Line to extrude nodes IDs = " + lineToExtrude.getFirstPoint().getId() + "   " + lineToExtrude.getLastPoint().getId());
            //*/

            surface.addLine(line);
        }

        private void changeLastPointToLineToExtrude(Point point, GeometryData geometry) {
            Surface surface = lineToExtrude.surfaceProperties.getSurface();
            Line line;
            // We have to add the new node to the geometry and also add a new line in the surface
            geometry.addPoint(point);
            line = new Line(lineToExtrude.getLastPoint(), point);
            geometry.addLine(line);
            // We have to connect the line to be extruded to this new node
            lineToExtrude.setLastPoint(point);

            /*
            Log.i("EXTRUDE", "Line to extrude ID = " + lineToExtrude.getId());
            Log.i("EXTRUDE", "New line ID = " + line.getId());
            Log.i("EXTRUDE", "New line nodes IDs= " + line.getFirstPoint().getId() + "   " + line.getLastPoint().getId());
            Log.i("EXTRUDE", "Line to extrude nodes IDs = " + lineToExtrude.getFirstPoint().getId() + "   " + lineToExtrude.getLastPoint().getId());
            //*/

            surface.addLine(line);
        }

        // To be able to extrude a line, it must be share its nodes with other lines.
        private void getAdjacentLines() {
            Surface surface;
            if (lineToExtrude.surfaceProperties.isItPartOfASurface()) {
                // If the line to extrude belongs to a surface, then we can extrude it.
                surface = lineToExtrude.surfaceProperties.getSurface();
                previousLine = surface.getPreviousLine(lineToExtrude);
                nextLine = surface.getNextLine(lineToExtrude);
            } else {
                // Otherwise we have to find in the object "geometry" if the line share its nodes.
            }
        }

        private void getUnitNormalVectors() {
            previousNormalVector = getUnitNormalVector(previousLine);
            nextNormalVector = getUnitNormalVector(nextLine);
            lineToExtrudeNormalVector = getUnitNormalVector(lineToExtrude);
        }

        private Vector getUnitNormalVector(Line line) {
            // The direction of the normal vector depends on how the line is defined

            Vector n = new Vector();
            Vector a = new Vector();
            Point p1, p2;

            p1 = line.getFirstPoint();
            p2 = line.getLastPoint();

            a.x = p2.getX() - p1.getX();
            a.y = p2.getY() - p1.getY();
            a.z = p2.getZ() - p1.getZ();

            if (Math.abs(a.x) > 1e-5) {
                n.y = 1f;
                n.z = 0f;
                n.x = -(a.y * n.y + a.z * n.z) / a.x;
            } else if (Math.abs(a.y) > 1e-5) {
                n.x = 1f;
                n.z = 0f;
                n.y = -(a.x * n.x + a.z * n.z) / a.y;
            } else if (Math.abs(a.z) > 1e-5) {
                n.x = 1f;
                n.y = 0f;
                n.z = -(a.x * n.x + a.y * n.y) / a.z;
            }
            n.normalize();

            return n;
        }
    }
}
