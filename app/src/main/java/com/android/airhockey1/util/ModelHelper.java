package com.android.airhockey1.util;

import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.data.GeometryData;

import java.util.Vector;

import nb.geometricBot.Model;

/**
 * Created by ernesto on 6/1/16.
 */
public class ModelHelper {

    public static void createNodesAndLinesFromModel(Model model,
                                                    GeometryData geometry) {
        Vector<Geometry.Point> lastAddedPoints;
        Vector<Geometry.Line> lastAddedLines;
        Geometry.Surface[] surfaces;
        int[] edges;
        float[] vertices, holes;
        final int dimension = Constants.PROBLEM_DIMENSION;

        edges = model.getEdgesRef();
        vertices = model.getVerticesRef();
        holes = model.getHolesRef();

        // Add the new points
        geometry.startRecordOfLastAddedPoints();
        for (int i = 0; i < model.getNVertices(); i++)
            geometry.addPoint(new Geometry.Point(vertices[i*dimension], vertices[i*dimension + 1], 0f));
        geometry.stopRecordOfLastAddedPoints();
        lastAddedPoints = geometry.getArrayWithLastAddedPoints();

        // Add the new lines
        geometry.startRecordOfLastAddedLines();
        for (int i = 0; i < model.getNEdges(); i++)
            geometry.addLine(new Geometry.Line(lastAddedPoints.get(edges[i*2]),
                    lastAddedPoints.get(edges[i*2+1])));
        geometry.stopRecordOfLastAddedLines();
        lastAddedLines = geometry.getArrayWithLastAddedLines();

        /*
        // Add the surface or surfaces
        surfaces = new Geometry.Surface[numberOfSurfacesInTheModel(model)];
        addSurfacesFromTheModel(model, lastAddedLines, surfaces);
        Log.i("Geometry", "Geometry size = " + geometry.numberOfSurfaces());
        for (int i = 0; i < surfaces.length; i++){
            surfaces[i].print();
            surfaces[i].meshSurface();
            geometry.addSurface(surfaces[i]);
        }
        Log.i("Geometry", "Geometry size = " + geometry.numberOfSurfaces());
        //*/

        //*
        Geometry.Surface surface = new Geometry.Surface();
        surface.setModel(model, lastAddedPoints, lastAddedLines);
        // To report that the construction of the surface has finished
        surface.commit();
        // Mesh the surface
        surface.meshSurface();
        geometry.addSurface(surface);
        //*/

        geometry.clearRecordOfLastAddedLines();
        geometry.clearRecordOfLastAddedPoints();
    }

    public static void printModel(String name, String TAG, Model model) {

        Log.i(TAG, name);
        Log.i(TAG, "--> Vertices");
        for (int i = 0; i < model.getNVertices(); i++) {
            Log.i(TAG, "" + model.getVerticesRef()[i*2] + "  " + model.getVerticesRef()[i*2+1] );
        }
        Log.i(TAG, "--> Connectivity");
        for (int i = 0; i < model.getNEdges(); i++) {
            Log.i(TAG, "" + model.getEdgesRef()[i*2] + "  " + model.getEdgesRef()[i*2+1] );
        }

    }

}
