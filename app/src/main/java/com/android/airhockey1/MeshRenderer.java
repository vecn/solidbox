package com.android.airhockey1;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.ProblemData;
import com.android.airhockey1.objects.VisualBorderShell;
import com.android.airhockey1.objects.VisualDisplacements;
import com.android.airhockey1.objects.VisualLine;
import com.android.airhockey1.objects.VisualMesh;
import com.android.airhockey1.objects.VisualObject;
import com.android.airhockey1.objects.VisualSolidOfRevolution;
import com.android.airhockey1.objects.VisualSurfaceResults;
import com.android.airhockey1.programs.BatchTextProgram;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.programs.VaryingColorShaderProgram;
import com.android.airhockey1.util.Axis;
import com.android.airhockey1.util.ColorScale;
import com.android.airhockey1.util.GLText;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.SimplePoint;
import com.android.airhockey1.util.Geometry.Zoom;
import com.android.airhockey1.util.GeometryGLSurfaceView;
import com.android.airhockey1.util.MatrixHelper;
import com.android.airhockey1.util.Results;
import com.android.airhockey1.util.Results.ColorPalette;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_CULL_FACE;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_LESS;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;
import static android.opengl.Matrix.setRotateEulerM;
import static android.opengl.Matrix.translateM;

import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import nb.geometricBot.Mesh;
import nb.geometricBot.Model;
import nb.pdeBot.finiteElement.MeshResults;
import nb.pdeBot.finiteElement.solidMechanics.Analysis2D;

/**
 * Created by ernesto on 27/01/16.
 */
public class MeshRenderer implements Renderer {

    private class ImportantFlags {
        public boolean dataHasBeenSet;
        public boolean showMesh;
        public boolean clipAreaChanged;
        public boolean newZooming;
        public boolean doingZooming;

        ImportantFlags() { setAllFalse(); }

        public void setAllTrue() {
            dataHasBeenSet = true;
            showMesh = true;
            clipAreaChanged = true;
            newZooming = true;
            doingZooming = true;
        }

        public void setAllFalse() {
            dataHasBeenSet = false;
            showMesh = false;
            clipAreaChanged = false;
            newZooming = false;
            doingZooming = false;
        }
    }

    private final Context context;

    private final float[] projectionMatrix = new float[16];
    private final float[] modelMatrix = new float[16];
    private final float[] rotationMatrix = new float[16];
    private final float[] viewMatrix = new float[16];
    private final float[] viewProjectionMatrix = new float[16];
    private final float[] invertedProjectionMatrix = new float[16];
    private final float[] invertedViewProjectionMatrix = new float[16];
    private final float[] invertedRotationMatrix = new float[16];
    private final float[] rotationViewProjectionMatrix = new float[16];
    private final float[] modelRotationViewProjectionMatrix = new float[16];

    private VisualMesh visualMesh;
    private VisualSurfaceResults visualSurfaceResults;
    private VisualBorderShell visualBorderShell;
    private VisualObject visualColorScale;
    private VisualObject visualSectionLines;
    private VisualSolidOfRevolution visualSolidOfRevolution;
    //private VisualObject visualSolidOfRevolution;
    private VisualObject visualMeshForSolidOfRevolution;

    // For text
    GLText glText;

    int typeOfSurfaceResultsToShow;

    private ColorShaderProgram colorProgram;
    private VaryingColorShaderProgram varyingColorShaderProgram;
    private BatchTextProgram batchTextShaderProgram;

    private PlotParameters plotParameters;
    private PlotParameters initialPlotParameters;
    private ProblemData problemData;
    private ColorScale colorScale;

    // Field of vision of camera in degrees
    private float angleOfVisionOfCamera;

    private ImportantFlags flags;

    // To handle zoom
    private Zoom zoom;

    private ColorPalette colorPalette;

    public MeshRenderer(Context context,
                        PlotParameters plotParameters) {
        this.context = context;
        this.plotParameters = plotParameters;
        initialPlotParameters = new PlotParameters();
        visualMesh = new VisualMesh();

        problemData = null;
        glText = null;

        visualSurfaceResults = new VisualSurfaceResults();
        visualBorderShell = new VisualBorderShell();
        visualColorScale = new VisualObject();
        visualSectionLines = new VisualObject();
        visualSolidOfRevolution = new VisualSolidOfRevolution();
        //visualSolidOfRevolution = new VisualObject();
        visualMeshForSolidOfRevolution = new VisualObject();

        colorScale = new ColorScale();

        typeOfSurfaceResultsToShow = Constants.NO_SURFACE_RESULT;

        angleOfVisionOfCamera = 90f;

        // Most of the flags are used to update the screen
        flags = new ImportantFlags();
        flags.setAllFalse();
        flags.newZooming = true;

        zoom = new Zoom();

        colorPalette = null;
    }

    public void handleTouchPress(float normalizedX, float normalizedY, int typeOfAction) {

    }

    public void handleTouchDrags(GeometryGLSurfaceView.Drags drags) {
        switch (drags.size()) {
            case 1:
                handleOneDrag(drags.getDrag(0));
                break;

            case 2:
                handleTwoDrags(drags);
                break;
        }
    }

    private void handleOneDrag(GeometryGLSurfaceView.Drag drag) {
        /*
        handlePanning(drag.getBeginningPosition().x, drag.getBeginningPosition().y,
                drag.getEndingPosition().x, drag.getEndingPosition().y);
        //*/
        rotateScene(drag.getBeginningPosition().x, drag.getBeginningPosition().y,
                drag.getEndingPosition().x, drag.getEndingPosition().y);
    }

    public void handlePanning(float normalizedStartX, float normalizedStartY,
                              float normalizedEndX, float normalizedEndY) {
        invertM(invertedProjectionMatrix, 0, projectionMatrix, 0);
        SimplePoint worldSpaceStart = convertNormalized2DPointToWorldSpace2DPoint(
                normalizedStartX, normalizedStartY, invertedProjectionMatrix);
        SimplePoint worldSpaceEnd = convertNormalized2DPointToWorldSpace2DPoint(
                normalizedEndX, normalizedEndY, invertedProjectionMatrix);
        AirHockeyRenderer.handlePanning(worldSpaceStart.x, worldSpaceStart.y,
                worldSpaceEnd.x, worldSpaceEnd.y, plotParameters, projectionMatrix);
        // The color scale is change every time we zoom in or zoom out
        setDimensionsColorScale();
        visualColorScale.createColorScale(colorScale);
    }

    public void rotateScene(float normalizedStartX, float normalizedStartY,
                            float normalizedEndX, float normalizedEndY) {

        Geometry.Vector dragVectorNDC = new Geometry.Vector(normalizedEndX - normalizedStartX,
                normalizedEndY - normalizedStartY, 0f);
        Geometry.Vector rotationAxis = new Geometry.Vector();
        float sign = 1;

        // Rotation axis on the XYZ original coordinate system
        if (Math.abs(dragVectorNDC.x) > 1e-5) {
            rotationAxis.y = 1f;
            rotationAxis.z = 0f;
            rotationAxis.x = -dragVectorNDC.y * rotationAxis.y / dragVectorNDC.x;
            if (dragVectorNDC.x > 0) sign = -1f;
        } else if (Math.abs(dragVectorNDC.y) > 1e-5) {
            rotationAxis.x = 1f;
            rotationAxis.z = 0f;
            rotationAxis.y = -dragVectorNDC.x * rotationAxis.x / dragVectorNDC.y;
            if (dragVectorNDC.y < 0) sign = -1f;
        } else {
            return;
        }

        // Calculates the angle
        float angleInRadians = dragVectorNDC.length() / Constants.DISTANCE_FOR_PI_RAD_ANGLE * sign;

        Geometry.addRotationAroundArbitraryAxis(plotParameters.getCenterX(),
                plotParameters.getCenterY(), plotParameters.getCenterZ(),
                rotationAxis.x, rotationAxis.y, rotationAxis.z, angleInRadians, viewMatrix);
    }

    public void handleTwoDrags(GeometryGLSurfaceView.Drags drags) {
        Geometry.scaleView(
                drags.getDrag(0).getBeginningPosition(),
                drags.getDrag(1).getBeginningPosition(),
                drags.getDrag(0).getEndingPosition(),
                drags.getDrag(1).getEndingPosition(),
                viewMatrix,
                projectionMatrix,
                plotParameters
        );
        // The color scale is change every time we zoom in or zoom out
        setDimensionsColorScale();
        visualColorScale.createColorScale(colorScale);
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        // Background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // OpenGL program (with a shader to draw uniform color over the triangles)
        colorProgram = new ColorShaderProgram(context);

        // OpenGL program (with a shader to draw a varying color over the triangles)
        varyingColorShaderProgram = new VaryingColorShaderProgram(context);

        batchTextShaderProgram = new BatchTextProgram();
        batchTextShaderProgram.init();

        glText = new GLText(batchTextShaderProgram, context.getAssets());
        glText.load("Roboto-Regular.ttf", Constants.FONT_SIZE, 2, 2);
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);

        // Ortographic projection
        plotParameters.setScreenValues(width, height);
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
        /*
        MatrixHelper.perspectiveM(
                projectionMatrix,
                angleOfVisionOfCamera,
                (float)width / (float)height,
                1f,
                10f
        );
        //*/

        setLookAtM(viewMatrix, 0, 0f, 0f, 3f, 0f, 0f, 0f, 0f, 1f, 0f);

        setIdentityM(viewMatrix, 0);

        setIdentityM(rotationMatrix, 0);

        setDimensionsColorScale();
    }

    public void setDimensionsColorScale() {
        float normUnitLength = plotParameters.getUnitLengthNormalizedCoordinateSystem();
        colorScale.setCenter(
                plotParameters.getMinX() + (Constants.COLOR_SCALE_LEFT_MARGIN +
                0.5f * Constants.COLOR_SCALE_WIDTH) / normUnitLength,
                plotParameters.getCenterY());
        colorScale.setDimensions(Constants.COLOR_SCALE_WIDTH / normUnitLength,
                Constants.COLOR_SCALE_HEIGHT / normUnitLength);
        colorScale.setNumberOfDivisions(Constants.COLOR_SCALE_DIVISONS_ON_X,
                Constants.COLOR_SCALE_DIVISONS_ON_Y);
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {

        if (!flags.dataHasBeenSet) return;

        // Clear the rendering surface.
        glClear(GL_COLOR_BUFFER_BIT);

        glClear(GL_DEPTH_BUFFER_BIT);
        // Enable depth test
        glEnable(GL_DEPTH_TEST);
        // Accept fragment if it closer to the camera than the former one
        glDepthFunc(GL_LESS);

        // Update the viewProjection matrix, and create an inverted matrix for
        // touch picking.
        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        //invertM(invertedViewProjectionMatrix, 0, projectionMatrix, 0);

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                drawPlate();
                break;
            case Constants.PLANE_STRAIN:
                drawSection();
                break;
            case Constants.SOLID_OF_REVOLUTION:
                drawSolidOfRevolution();
                break;
        }
        glDisable(GL_DEPTH_TEST);

        drawColorScale();
    }

    private void drawPlate() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMesh.bindData(colorProgram);

            rotateScene();
            positionObjectInScene(0f, 0f, -0.5f*problemData.plate.getThickness());
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();

            rotateScene();
            positionObjectInScene(0f, 0f, 0.5f*problemData.plate.getThickness());
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSurfaceResults.bindData(varyingColorShaderProgram);

        rotateScene();
        positionObjectInScene(0f, 0f, -0.5f * problemData.plate.getThickness());
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();

        rotateScene();
        positionObjectInScene(0f, 0f, 0.5f * problemData.plate.getThickness());
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();
        //*
        visualBorderShell.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualBorderShell.draw();
        //*/
    }

    private void drawSection() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMesh.bindData(colorProgram);

            rotateScene();
            positionObjectInScene(0f, 0f, 0);
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMesh.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSurfaceResults.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSurfaceResults.draw();

        colorProgram.useProgram();
        visualSectionLines.bindData(colorProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        colorProgram.setUniforms(modelRotationViewProjectionMatrix, 1f, 1f, 1f);
        visualSectionLines.draw();
    }

    private void drawSolidOfRevolution() {

        if (flags.showMesh) {
            colorProgram.useProgram();
            visualMeshForSolidOfRevolution.bindData(colorProgram);
            rotateScene();
            positionObjectInScene(0f, 0f, 0f);
            colorProgram.setUniforms(modelRotationViewProjectionMatrix, 0f, 0f, 0f);
            visualMeshForSolidOfRevolution.draw();
        }

        varyingColorShaderProgram.useProgram();
        visualSolidOfRevolution.bindData(varyingColorShaderProgram);
        rotateScene();
        positionObjectInScene(0f, 0f, 0f);
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualSolidOfRevolution.draw();
    }

    private void drawColorScale() {
        varyingColorShaderProgram.useProgram();
        visualColorScale.bindData(varyingColorShaderProgram);
        final float unitLength = plotParameters.getUnitLengthNormalizedCoordinateSystem();

        positionColorScaleInScene();
        varyingColorShaderProgram.setUniforms(modelRotationViewProjectionMatrix);
        visualColorScale.draw();

        if (flags.dataHasBeenSet) {
            glEnable(GLES20.GL_BLEND);
            glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            glText.setScale(Constants.FONT_SIZE_ON_SCREEN / (Constants.FONT_SIZE * unitLength));
            glText.begin(1.0f, 1.0f, 1.0f, 1.0f, modelRotationViewProjectionMatrix);
            glText.draw("                               ", 0f, 1f);
            glText.draw(Results.getLabel(Math.round(colorPalette.getMaxValue()*1000f)/1000f), colorScale.getCenterX(),
                    colorScale.getCenterY() + 0.5f * colorScale.getHeight());
            glText.draw(Results.getLabel(Math.round(colorPalette.getMinValue()*1000f)/1000), colorScale.getCenterX(),
                    colorScale.getCenterY() - 0.5f * colorScale.getHeight());
            glText.draw("                               ", 0f, 1f);
            glDisable(GLES20.GL_BLEND);
        }
    }

    public void drawMesh() {
        flags.showMesh = true;
    }

    public void hideMesh() {
        flags.showMesh = false;
    }

    public void changeVerticesCoordinates(Geometry.Surface modelSurface, MeshResults meshResults) {

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRAIN:
                visualMesh.changeCoordinates(meshResults);
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                break;
            case Constants.PLANE_STRESS:
                visualMesh.changeCoordinates(meshResults);
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                break;
            case Constants.SOLID_OF_REVOLUTION:
                visualSolidOfRevolution.setVerticesCoordinates(meshResults.getVerticesRef());
                visualMeshForSolidOfRevolution.createMeshForSolidOfRevolution(meshResults,
                        modelSurface.getEgeIdOnModel(problemData.solidOfRevolution.getAxis()),
                        problemData.solidOfRevolution.getNumberOfSlices());
                break;
        }
    }

    public void setInitialSurfaceResult(MeshResults meshResults,
                                        ColorPalette colorPalette,
                                        ProblemData problemData,
                                        Geometry.Surface modelSurface,
                                        int typeOfSurfaceResultsToShow) {

        //Log.i("Draw list", "Number of edges = " + meshResults.getNEdges());

        // We use a different color palette for the color scale because it is modify later
        // when the color scale is being constructed
        colorScale.setColorPalette(new Results.ColorPaletteRainbow());
        visualColorScale.createColorScale(colorScale);

        this.problemData = problemData;
        this.colorPalette = colorPalette;

        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                visualBorderShell.setNumberOfElements(meshResults.getNEdges() * 2,
                        -0.5f * problemData.plate.getThickness(), 0.5f * problemData.plate.getThickness());
                visualBorderShell.setMesh(meshResults.getVerticesRef(), meshResults.getConnEdgesRef());
                // Memory for OpenGL to show the results
                visualSurfaceResults.setNumberOfElements(meshResults.getNElements());
                // Creates the triangles used to show the surface results
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                // Sets the values on each vertex of each triangle
                setSurfaceResult(meshResults, colorPalette, typeOfSurfaceResultsToShow);
                // Creates the mesh
                visualMesh.createMesh(meshResults);
                break;

            case Constants.PLANE_STRAIN:
                visualSectionLines.createSectionLines(modelSurface, meshResults);
                // Memory for OpenGL to show the results
                visualSurfaceResults.setNumberOfElements(meshResults.getNElements());
                // Creates the triangles used to show the surface results
                visualSurfaceResults.setMesh(meshResults.getVerticesRef(), meshResults.getConnMtxRef());
                // Sets the values on each vertex of each triangle
                setSurfaceResult(meshResults, colorPalette, typeOfSurfaceResultsToShow);
                // Creates the mesh
                visualMesh.createMesh(meshResults);
                break;

            case Constants.SOLID_OF_REVOLUTION:
                final int axisIdOnModel =
                        modelSurface.getEgeIdOnModel(problemData.solidOfRevolution.getAxis());
                final int numberOfSlices = problemData.solidOfRevolution.getNumberOfSlices();
                visualSolidOfRevolution.create(meshResults.getModelSgmRef(), axisIdOnModel,
                        numberOfSlices);
                //visualSolidOfRevolution.createSolidOfRevolution(meshResults, axisIdOnModel,
                //        numberOfSlices,typeOfSurfaceResultsToShow, colorPalette);
                visualSolidOfRevolution.setVerticesCoordinates(meshResults.getVerticesRef());
                setSurfaceResult(meshResults, colorPalette, typeOfSurfaceResultsToShow);
                visualMeshForSolidOfRevolution.createMeshForSolidOfRevolution(meshResults,
                        axisIdOnModel, numberOfSlices);
                break;
        }

        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            plotParameters.setDepth(meshResults, true);
        else
            plotParameters.setDepth(meshResults, false);

        // Here I have to do the copy, because now plotParameters have the right values
        initialPlotParameters.copy(plotParameters);

        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());

        flags.dataHasBeenSet = true;
    }

    public void setSurfaceResult(MeshResults meshResults,
                                 ColorPalette colorPalette,
                                 int typeOfSurfaceResultsToShow) {
        switch (typeOfSurfaceResultsToShow) {
            case Constants.DISPLACEMENTS:
                // Set min and max values for the color palette
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.ALL_COMPONENTS, colorPalette);
                updateSurfaceResults(Constants.ALL_COMPONENTS, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.DISPLACEMENTS_ON_X:
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.X_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.X_COMPONENT, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.DISPLACEMENTS_ON_Y:
                setMinAndMaxValuesForColorPalette(meshResults.getDisplacementRef(),
                        Constants.Y_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.Y_COMPONENT, meshResults.getDisplacementRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            case Constants.DEFORMATIONS:
                setMinAndMaxValuesForColorPalette(meshResults.getVonMisesStressRef(),
                        Constants.ONE_COMPONENT, colorPalette);
                updateSurfaceResults(Constants.ONE_COMPONENT, meshResults.getVonMisesStressRef(),
                        meshResults.getConnMtxRef(), meshResults.getConnEdgesRef(), colorPalette);
                break;

            /*
            case Constants.VON_MISES:
                visualSurfaceResults.setValues(
                        Constants.ONE_COMPONENT,
                        meshResults.getVonMisesStressRef(),
                        meshResults.getConnMtxRef(),
                        colorPalette
                );
                break;
            */
        }
    }

    private void updateSurfaceResults(char direction,
                                      float[] values,
                                      int[] connMtx,
                                      int[] connEdges,
                                      ColorPalette colorPalette) {
        switch (problemData.getTypeOfProblem()) {
            case Constants.PLANE_STRESS:
                visualSurfaceResults.setValues(
                        direction,
                        values,
                        connMtx,
                        colorPalette
                );
                visualBorderShell.setValues(
                        direction,
                        values,
                        connEdges,
                        colorPalette
                );
                break;

            case Constants.PLANE_STRAIN:
                visualSurfaceResults.setValues(
                        direction,
                        values,
                        connMtx,
                        colorPalette
                );
                break;

            case Constants.SOLID_OF_REVOLUTION:
                //*
                visualSolidOfRevolution.setVerticesValues(
                        direction,
                        values,
                        colorPalette
                );
                //*/
                break;
        }
    }

    private void positionObjectInScene(float x, float y, float z) {
        setIdentityM(modelMatrix, 0);
        translateM(modelMatrix, 0, x, y, z);
        multiplyMM(modelRotationViewProjectionMatrix, 0, rotationViewProjectionMatrix, 0, modelMatrix, 0);
    }

    private void positionColorScaleInScene() {
        for (int i = 0; i < 16; i++) modelRotationViewProjectionMatrix[i] = projectionMatrix[i];
    }

    private void rotateScene() {
        multiplyMM(rotationViewProjectionMatrix, 0, viewProjectionMatrix, 0, rotationMatrix, 0);
    }

    private void doNotRotateScene() {
        setIdentityM(modelMatrix, 0);
        multiplyMM(rotationViewProjectionMatrix, 0, viewProjectionMatrix, 0, modelMatrix, 0);
    }

    private void setRotationMatrix(float angle, float x, float y, float z) {
        /*
        float[] A = new float[16];
        float[] B = new float[16];
        for (int i = 0; i < 16; i++) A[i] = rotationMatrix[i];
        rotateM(B, 0, angle, x, y, z);
        multiplyMM(rotationMatrix, 0, B, 0, A, 0);
        //*/
        rotateM(rotationMatrix, 0, angle, x, y, z);

        Log.i("ROTATION", "Begin ---------------------------------");
        Log.i("ROTATION", "Last object rotation");
        Log.i("ROTATION", "Angle = " + angle);
        Log.i("ROTATION", "Axis  = " + x + "   " + y + "   " + z);
        printSquareMatrixSize4("ROTATION", "Rotation", rotationMatrix);
        Log.i("ROTATION", "End ---------------------------------");
    }

    public void clearTransformations() {
        plotParameters.copy(initialPlotParameters);
        setIdentityM(viewMatrix, 0);
        setIdentityM(rotationMatrix, 0);
        // We update the projection matrix
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
        // The color scale is change every time we zoom in or zoom out
        setDimensionsColorScale();
        visualColorScale.createColorScale(colorScale);
    }

    private void setMinAndMaxValuesForColorPalette(float[] values,
                                                   int direction,
                                                   ColorPalette colorPalette) {
        float smallestValue, largestValue;
        final int dimension = Constants.PROBLEM_DIMENSION;

        // Smallest and largest displacement
        smallestValue = largestValue = 0f;
        switch (direction){
            case Constants.ALL_COMPONENTS:
                smallestValue = Results.smallestMagnitude(dimension, values);
                largestValue = Results.largestMagnitude(dimension, values);
                break;
            case Constants.X_COMPONENT:
                smallestValue = Results.smallestValue(dimension, values, 0);
                largestValue = Results.largestValue(dimension, values, 0);
                break;
            case Constants.Y_COMPONENT:
                smallestValue = Results.smallestValue(dimension, values, 1);
                largestValue = Results.largestValue(dimension, values, 1);
                break;
            case Constants.ONE_COMPONENT:
                smallestValue = Results.smallestValue(values);
                largestValue = Results.largestValue(values);
        }
        colorPalette.setMinMaxValues(smallestValue, largestValue);
    }

    public void printPlotParameters() {
        plotParameters.print("MATRICES", "Plot parameters");
    }

    public void printMatrices() {
        printSquareMatrixSize4("MATRICES", "Projection", projectionMatrix);
        printSquareMatrixSize4("MATRICES", "View", viewMatrix);
        printSquareMatrixSize4("MATRICES", "Rotation", rotationMatrix);
        printSquareMatrixSize4("MATRICES", "Model", modelMatrix);
        printSquareMatrixSize4("MATRICES", "ModelRotationViewProjection", modelRotationViewProjectionMatrix);
    }

    public static void printSquareMatrixSize4(String TAG, String label, float[] M) {
        Log.i(TAG, "Begin --------------------------------------------------");
        Log.i(TAG, label);
        for (int j = 0; j < 4; j++) {
            Log.i(TAG, M[0*4+j] + " " + M[1*4+j] + " " + M[2*4+j] + " " + M[3*4+j]);
        }
        Log.i(TAG, "End ---------------------------------------------------");
    }

    public static SimplePoint convertNormalized2DPointToWorldSpace2DPoint(float normalizedX,
                                                                          float normalizedY,
                                                                          float[] invertedTransformationMatrix) {
        final float[] normalizedPoint = {normalizedX, normalizedY, 0, 1};
        final float[] worldSpacePoint = new float[4];
        multiplyMV(worldSpacePoint, 0, invertedTransformationMatrix, 0, normalizedPoint, 0);
        return new SimplePoint(worldSpacePoint[0], worldSpacePoint[1], worldSpacePoint[2]);
    }
}
