package com.android.airhockey1.data;

import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Rectangle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by ernesto on 5/17/16.
 */
public class Rectangles extends DataStructureB<Rectangle> {
    public Rectangles() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            dos.writeInt(elements.size());

            for (int i = 0; i < elements.size(); i++)
                if (!elements.get(i).writeToFile(dos)) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, GeometryData geometry) {
        int n;
        Rectangle rectangle;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                rectangle = new Rectangle();
                if (!rectangle.readFromFile(dis)) return false;
                rectangle.addToGeometry(geometry);
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
