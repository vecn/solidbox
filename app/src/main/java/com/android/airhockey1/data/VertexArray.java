package com.android.airhockey1.data;

/***
 * Excerpted from "OpenGL ES for Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/kbogla for more book information.
 ***/

import android.util.Log;

import com.android.airhockey1.Constants;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.android.airhockey1.Constants.BYTES_PER_FLOAT;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class VertexArray {

    // To store our vertex data in native code
    private final FloatBuffer floatBuffer;
    private int size;

    public VertexArray(float[] vertexData) {
        floatBuffer = ByteBuffer
                .allocateDirect(vertexData.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        size = vertexData.length;
    }

    public VertexArray(int length) {
        size = length;
        floatBuffer = ByteBuffer
                .allocateDirect(length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
    }

    public void setValue(int index, float value) {
        floatBuffer.put(index, value);
    }

    // Generic method to associate an attribute in our shader with the data
    public void setVertexAttribPointer(int dataOffset, int attributeLocation,
                                       int componentCount, int stride) {
        // Where OpenGL will start to read the data
        floatBuffer.position(dataOffset);
        // Tells to OpenGL where can read the data for the attribute
        glVertexAttribPointer(attributeLocation, componentCount, GL_FLOAT, false, stride, floatBuffer);
        // Enables the attribute
        glEnableVertexAttribArray(attributeLocation);
        floatBuffer.position(0);
    }

    public void printVertexArray() {
        Log.i("Vertex array", "VERTEX ARRAY");
        for (int i = 0; i < size; i++)
            Log.i("Vertex array", "" + floatBuffer.get(i));
    }

}