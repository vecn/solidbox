package com.android.airhockey1.data;

import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Point;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by ernesto on 4/03/16.
 */
public class Points extends DataStructureA<Point> {
    public Points() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            Iterator<Point> it = elements.iterator();
            Point p;

            int nSpecialElements = 0;
            while (it.hasNext())
                if (it.next().surfaceProperties.isItASpecialSurface()) nSpecialElements++;

            dos.writeInt(numberOfElements - nSpecialElements);

            it = elements.iterator();
            while (it.hasNext()) {
                p = it.next();
                // Here are not written the points that belong a special surface
                if (!p.surfaceProperties.isItASpecialSurface())
                    if (!p.writeToFile(dos)) return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis) {
        int n;
        Point point;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                point = new Point();
                if (!point.readFromFile(dis)) return false;
                addElementWithId(point);
            }

            restart();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
