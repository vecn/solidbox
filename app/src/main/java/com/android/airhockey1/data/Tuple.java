package com.android.airhockey1.data;

/**
 * Created by ernesto on 25/02/16.
 */
public class Tuple implements Comparable<Tuple> {
    public Integer x;
    public Integer y;

    public Tuple(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int compareTo(Tuple tuple){
        return x.compareTo(tuple.x);
    }
}
