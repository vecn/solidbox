package com.android.airhockey1.data;

import android.util.Log;

import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Node;
import com.android.airhockey1.util.Geometry.Point;
import com.android.airhockey1.util.Geometry.Line;
import com.android.airhockey1.util.Geometry.Surface;
import com.android.airhockey1.util.Geometry.Circle;
import com.android.airhockey1.util.Geometry.Rectangle;
import com.android.airhockey1.util.Geometry.RegularPolygon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;

/**
 * Created by ernesto on 2/02/16.
 */
public class GeometryData {
    // Primary elements (everything is constructed using them)
    private Points points;
    private Nodes nodes;
    private Lines lines;
    private Surfaces surfaces;

    // Secondary elements
    private Circles circles;
    private Rectangles rectangles;
    private RegularPolygons regularPolygons;

    public GeometryData() {
        points = new Points();
        nodes = new Nodes();
        lines = new Lines();
        surfaces = new Surfaces();

        circles = new Circles();
        rectangles = new Rectangles();
        regularPolygons = new RegularPolygons();
    }

    public void deleteSurface(Surface surface) {
        surfaces.removeElement(surface);
    }
    public void deleteLine(Line line) { lines.removeElement(line); }
    public void deleteCircle(Circle circle) { circles.removeElement(circle); }
    public void deleteRectangle(Rectangle rectangle) { rectangles.removeElement(rectangle); }
    public void deleteRegularPolygon(RegularPolygon regularPolygon) { regularPolygons.removeElement(regularPolygon); }
    public void deleteNode(Node node) {
        points.removeElement(node.getPoint());
        nodes.removeElement(node);
    }
    public void deletePoint(Point point) {
        if (point.getNodeInfo().isPartOfANode())
            nodes.removeElement(point.getNodeInfo().getNode());
        points.removeElement(point);
    }

    public void addPoint(Point point) {
        if (point.getNodeInfo().isPartOfANode())
            nodes.addElement(point.getNodeInfo().getNode());
        points.addElement(point);
    }
    public void addNode(Node node) {
        points.addElement(node.getPoint());
        nodes.addElement(node);
    }
    public void addLine(Line line) { lines.addElement(line); }
    public void addSurface(Surface surface) { surfaces.addElement(surface); } // The surface can be bounded with points and/or nodes
    public void addCircle(Circle circle) { circles.addElement(circle); }
    public void addRectangle(Rectangle rectangle) { rectangles.addElement(rectangle); }
    public void addRegularPolygon(RegularPolygon regularPolygon) {
        regularPolygons.addElement(regularPolygon);
    }

    public int numberOfPoints() { return points.getSize(); };
    public int numberOfNodes() { return nodes.getSize(); };
    public int numberOfLines() { return lines.getSize(); };
    public int numberOfSurfaces() { return surfaces.getSize(); }

    public Point getPoint(int index) { return points.getElement(index); }
    public Node getNode(int index) { return nodes.getElement(index); }
    public Line getLine(int index) { return lines.getElement(index); }
    public Surface getSurface(int index) { return surfaces.getElement(index); }

    public Points getPoints() { return points; }
    public Nodes getNodes() { return nodes; }
    public Lines getLines() { return lines; };
    public Surfaces getSurfaces() { return surfaces; }

    public void startRecordOfLastAddedPoints() { points.startRecordOfLastAddedElements(); }
    public void stopRecordOfLastAddedPoints() { points.endRecordOfLastAddedElements(); }
    public void clearRecordOfLastAddedPoints() { points.clearRecordOfLastAddedElements(); }
    public Vector<Point> getArrayWithLastAddedPoints() { return points.getArrayWithLastAddedElements(); }

    public void startRecordOfLastAddedNodes() { nodes.startRecordOfLastAddedElements(); }
    public void stopRecordOfLastAddedNodes() { nodes.endRecordOfLastAddedElements(); }
    public void clearRecordOfLastAddedNodes() { nodes.clearRecordOfLastAddedElements(); }
    public Vector<Node> getArrayWithLastAddedNodes() { return nodes.getArrayWithLastAddedElements(); }

    public void startRecordOfLastAddedLines() { lines.startRecordOfLastAddedElements(); }
    public void stopRecordOfLastAddedLines() { lines.endRecordOfLastAddedElements(); }
    public void clearRecordOfLastAddedLines() { lines.clearRecordOfLastAddedElements(); }
    public Vector<Line> getArrayWithLastAddedLines() { return lines.getArrayWithLastAddedElements(); }

    public boolean writeToFile(DataOutputStream dos) {
        //*
        if (!(points.writeToFile(dos) &&
                nodes.writeToFile(dos) &&
                lines.writeToFile(dos) &&
                surfaces.writeToFile(dos) &&
                rectangles.writeToFile(dos) &&
                circles.writeToFile(dos) &&
                regularPolygons.writeToFile(dos))) return false;
        //*/
        /*
        if (!(points.writeToFile(dos) &&
                nodes.writeToFile(dos) &&
                lines.writeToFile(dos) &&
                surfaces.writeToFile(dos) &&
                rectangles.writeToFile(dos))) return false;
                //*/
        return true;
    }

    public boolean readFromFile(DataInputStream dis) {
        //*
        if (!(points.readFromFile(dis) &&
                nodes.readFromFile(dis, points) &&
                lines.readFromFile(dis, points) &&
                surfaces.readFromFile(dis, points, lines) &&
                rectangles.readFromFile(dis, this) &&
                circles.readFromFile(dis, this) &&
                regularPolygons.readFromFile(dis, this))) return false;
                //*/
        /*
        if (!(points.readFromFile(dis) &&
                nodes.readFromFile(dis, points) &&
                lines.readFromFile(dis, points) &&
                surfaces.readFromFile(dis, points, lines) &&
                rectangles.readFromFile(dis, this))) return false;
                //*/
        return true;
    }

    public void clear() {
        circles.clear();
        rectangles.clear();
        regularPolygons.clear();
        surfaces.clear();
        lines.clear();
        nodes.clear();
        points.clear();
    }
}
