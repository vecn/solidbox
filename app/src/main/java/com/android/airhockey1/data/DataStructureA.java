package com.android.airhockey1.data;

import com.android.airhockey1.util.*;
import com.android.airhockey1.util.Geometry;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

/**
 * Created by ernesto on 2/02/16.
 */
public class DataStructureA<T extends InterfaceDataStructure> {
    protected int counter;
    protected int numberOfElements;
    protected Vector<Integer> positionInArray;
    protected Queue<Integer> availableIds;
    protected Vector<T> elements;
    protected boolean recordTheLastAddedElements;
    protected Vector<T> lastAddedElements;

    public void clear() {
        counter = 0;
        numberOfElements = 0;
        positionInArray.clear();
        availableIds.clear();
        elements.clear();
        recordTheLastAddedElements = false;
        lastAddedElements.clear();
    }

    protected void initializeVariables() {
        counter = 0;
        numberOfElements = 0;
        positionInArray = new Vector();
        availableIds = new LinkedList();
        elements = new Vector();
        recordTheLastAddedElements = false;
        lastAddedElements = new Vector();
    }

    public DataStructureA() {
        initializeVariables();
    }

    public T getElement(int index) {
        // Out of bounds
        if (index < 0) return null;
        if (index > numberOfElements - 1) return null;
        return elements.get(index);
    }

    public void addElement(T element) {
        // Looks for available last positions in the array
        if (availableIds.size() > 0) {
            int availablePosition = counter - availableIds.size();
            int availableId = availableIds.poll();

            element.setId(availableId);
            elements.set(availablePosition, element);

            positionInArray.set(availableId, availablePosition);
            numberOfElements++;
        } else {
            element.setId(counter);
            elements.add(element);

            positionInArray.add(counter);

            counter++;
            numberOfElements++;
        }

        if (recordTheLastAddedElements) lastAddedElements.add(element);
    }

    // Used when the data is being loaded from a file
    public void addElementWithId(T element) {
        elements.add(element);
        numberOfElements++;
    }

    // Used to determine the available Ids and other info when the data was loaded from a file
    public void restart() {
        int largestId;
        boolean[] existId;

        if (elements.size() == 0) return;

        // Finds the largest id
        largestId = elements.get(0).getId();
        for (int i = 0; i < elements.size(); i++)
            if(elements.get(i).getId() > largestId) largestId = elements.get(i).getId();

        // Size before the app was paused
        counter = largestId + 1;

        // We resize our arrays
        //elements.setSize(counter);
        positionInArray.setSize(counter);

        // Initialize some variables
        existId = new boolean[counter];
        for (int i = 0; i < counter; i++){
            positionInArray.set(i, -1);
            existId[i] = false;
        }

        // Reconstruct availableIds and positionInArray
        for (int i = 0; i < numberOfElements; i++) {
            existId[elements.get(i).getId()] = true;
            positionInArray.set(elements.get(i).getId(), i);
        }
        for (int i = 0; i < existId.length; i++) if (!existId[i]) availableIds.add(i);
    }

    public void removeElement(T element) {
        removeElementById(element.getId());
    }

    public void removeElementById(int id){
        if (availableIds.contains(id)) return;
        // We add the id to que queue to use it later
        availableIds.add(id);
        // We move the last element to this position
        elements.set(positionInArray.get(id), elements.get(numberOfElements-1));
        positionInArray.set(elements.get(numberOfElements-1).getId(), positionInArray.get(id));
        // We set the reverence to null!!!
        elements.set(numberOfElements-1, null);
        positionInArray.set(id, -1);
        numberOfElements--;
    }

    public T getElementById(int id) {
        if ((id > (positionInArray.size() - 1)) || (id < 0)) return null;
        return elements.get(positionInArray.get(id));
    }

    public int getSize(){ return numberOfElements; }

    public void startRecordOfLastAddedElements(){
        recordTheLastAddedElements = true;
    }

    public void endRecordOfLastAddedElements(){
        recordTheLastAddedElements = false;
    }

    public void clearRecordOfLastAddedElements(){
        lastAddedElements.clear();
    }

    // Returns a queue with the last added elements
    public Vector<T> getArrayWithLastAddedElements() {
        return lastAddedElements;
    }
}
