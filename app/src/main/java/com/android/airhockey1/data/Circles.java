package com.android.airhockey1.data;

import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Circle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.CRC32;

/**
 * Created by ernesto on 5/17/16.
 */
public class Circles extends DataStructureB<Circle>{
    public Circles() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            dos.writeInt(elements.size());

            for (int i = 0; i < elements.size(); i++)
                if (!elements.get(i).writeToFile(dos)) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, GeometryData geometry) {
        int n;
        Geometry.Circle circle;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                circle = new Geometry.Circle();
                if (!circle.readFromFile(dis)) return false;
                addElement(circle);
                circle.addToGeometry(geometry);
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
