package com.android.airhockey1.data;

import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Surface;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by ernesto on 3/02/16.
 */
public class Surfaces extends DataStructureA<Surface> {

    public Surfaces() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {

            Iterator<Surface> it = elements.iterator();
            Surface s;

            int nSpecialElements = 0;
            while (it.hasNext())
                if (it.next().shapeInfo.isSpecialShape()) nSpecialElements++;

            dos.writeInt(numberOfElements - nSpecialElements);

            it = elements.iterator();
            while (it.hasNext()) {
                s = it.next();
                // Here are not written the surfaces that belong a special surface
                if (!s.shapeInfo.isSpecialShape())
                    if (!s.writeToFile(dos)) return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, Points points, Lines lines) {
        int n;
        Surface surface;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                surface = new Surface();
                if (!surface.readFromFile(dis, points, lines)) return false;
                addElementWithId(surface);
            }

            restart();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
