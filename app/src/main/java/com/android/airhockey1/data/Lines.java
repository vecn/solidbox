package com.android.airhockey1.data;

import android.provider.ContactsContract;
import android.widget.Toast;

import com.android.airhockey1.TabFragment1;
import com.android.airhockey1.util.*;
import com.android.airhockey1.util.Geometry.Line;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by ernesto on 3/02/16.
 */
public class Lines extends DataStructureA<Line> {

    public Lines () {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            Iterator<Line> it = elements.iterator();
            Line l;

            int nSpecialElements = 0;
            while (it.hasNext())
                if (it.next().surfaceProperties.isItASpecialSurface()) nSpecialElements++;

            dos.writeInt(numberOfElements - nSpecialElements);

            it = elements.iterator();
            while (it.hasNext()) {
                l = it.next();
                // Here are not written the lines that belong a special surface
                if (!l.surfaceProperties.isItASpecialSurface())
                    if (!l.writeToFile(dos)) return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, Points points) {
        int n;
        Line line;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                line = new Line();
                if (!line.readFromFile(dis, points)) return false;
                addElementWithId(line);
            }

            restart();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
