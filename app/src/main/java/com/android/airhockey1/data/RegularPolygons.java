package com.android.airhockey1.data;

import com.android.airhockey1.util.Geometry.RegularPolygon;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by ernesto on 5/17/16.
 */
public class RegularPolygons extends DataStructureB<RegularPolygon> {
    public RegularPolygons() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            dos.writeInt(elements.size());

            for (int i = 0; i < elements.size(); i++)
                if (!elements.get(i).writeToFile(dos)) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, GeometryData geometry) {
        int n;
        RegularPolygon regularPolygon;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                regularPolygon = new RegularPolygon();
                if (!regularPolygon.readFromFile(dis)) return false;
                addElement(regularPolygon);
                regularPolygon.addToGeometry(geometry);
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
