package com.android.airhockey1.data;

import android.util.Log;

import com.android.airhockey1.util.*;
import com.android.airhockey1.util.Geometry.Node;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by ernesto on 3/02/16.
 */
public class Nodes extends DataStructureA<Node> {

    public Nodes() {
        super();
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            Iterator<Node> it = elements.iterator();
            Node n;

            int nSpecialElements = 0;
            while (it.hasNext())
                if (it.next().getPoint().surfaceProperties.isItASpecialSurface()) nSpecialElements++;

            dos.writeInt(numberOfElements - nSpecialElements);

            it = elements.iterator();
            while (it.hasNext()) {
                n = it.next();
                // Here are not written the nodes that belong a special surface
                if (!n.getPoint().surfaceProperties.isItASpecialSurface())
                    if (!n.writeToFile(dos)) return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, Points points) {
        int n;
        Node node;

        try {
            n = dis.readInt();
            if (n < 0) return false;

            for (int i = 0; i < n; i++) {
                node = new Node(null);
                if (!node.readFromFile(dis, points)) return false;
                addElementWithId(node);
            }

            restart();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
