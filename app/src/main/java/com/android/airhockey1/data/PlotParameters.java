package com.android.airhockey1.data;

import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.util.Results;
import com.android.airhockey1.util.Geometry.Line;

import nb.geometricBot.Mesh;
import nb.pdeBot.Material;
import nb.pdeBot.finiteElement.MeshResults;

/**
 * Created by ernesto on 16/02/16.
 */
public class PlotParameters {
    // Main data
    private float zoomFactor;

    private float screenWidth, screenHeight; // In pixels (I'm guessing)
    private float centerX, centerY, centerZ; // In world-space coordinates
    private float depth;                     // In world-space coordinates

    private boolean stopUpdates;

    private boolean rangesCalculated;
    private float minX, maxX;
    private float minY, maxY;
    private float minZ, maxZ;
    private float normalizedUnitLength;
    private float aspectRatio;
    private float scaleFactor;

    public PlotParameters() {
        zoomFactor = 0f;
        centerX = centerY = centerZ = 0f;
        minX = maxX = 0f;
        minY = maxY = 0f;
        minZ = maxZ = 0f;
        depth = 0f;
        rangesCalculated = false;

        screenWidth = screenHeight = 0f;
        normalizedUnitLength = 0f;
        aspectRatio = 0f;

        scaleFactor = 0f;

        stopUpdates = false;
    }

    public void setZoomFactor(float zoomFactor){
        if (stopUpdates) return;
        this.zoomFactor = zoomFactor;
        // We have to update the ranges
        rangesCalculated = false;
    }

    public void setCenter(float centerX, float centerY, float centerZ){
        if (stopUpdates) return;
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        // We have to update the ranges
        rangesCalculated = false;
    }

    public float getZoomFactor(){
        return zoomFactor;
    }

    public float getCenterX(){
        return centerX;
    }
    public float getCenterY() {
        return centerY;
    }
    public float getCenterZ() {
        return centerZ;
    }
    public float getDepth() {
        return depth;
    }

    public float getMinX() {
        if (!rangesCalculated) calculateRanges();
        return minX;
    }

    public float getMaxX() {
        if (!rangesCalculated) calculateRanges();
        return maxX;
    }

    public float getMinY() {
        if (!rangesCalculated) calculateRanges();
        return minY;
    }

    public float getMaxY() {
        if (!rangesCalculated) calculateRanges();
        return maxY;
    }

    public float getMinZ() {
        if (!rangesCalculated) calculateRanges();
        return minZ;
    }

    public float getMaxZ() {
        if (!rangesCalculated) calculateRanges();
        return maxZ;
    }

    public void setScreenValues(float screenWidth, float screenHeight){
        if (stopUpdates) return;
        rangesCalculated = false;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public void setDepth(float depth) {
        rangesCalculated = false;
        this.depth = depth;
    }

    public float getUnitLengthNormalizedCoordinateSystem() {
        calculateNormalizedUnitLength();
        return normalizedUnitLength;
    }

    private void calculateRanges() {
        calculateAspectRatio();
        calculateNormalizedUnitLength();

        if (screenHeight > screenWidth){
            // Portrait
            minX = centerX - 1f/normalizedUnitLength;
            maxX = centerX + 1f/normalizedUnitLength;

            minY = centerY - aspectRatio/normalizedUnitLength;
            maxY = centerY + aspectRatio/normalizedUnitLength;
        } else {
            // Landscape
            minX = centerX - aspectRatio/normalizedUnitLength;
            maxX = centerX + aspectRatio/normalizedUnitLength;

            minY = centerY - 1f/normalizedUnitLength;
            maxY = centerY + 1f/normalizedUnitLength;
        }

        minZ = centerZ - depth - (float)Math.sqrt(centerX*centerX + centerY*centerY);
        maxZ = centerZ + depth + (float)Math.sqrt(centerX*centerX + centerY*centerY);

        rangesCalculated = true;
    }

    private void calculateAspectRatio() {
        if (screenHeight > screenWidth) aspectRatio = screenHeight / screenWidth;
        else                            aspectRatio = screenWidth / screenHeight;
    }

    private void calculateNormalizedUnitLength() {
        // Unit length in the normalized space as function of the zoom factor.
        normalizedUnitLength = 1f/((float) Constants.REFERENCE_NUMBER_OF_UNITS)*zoomFactor;
    }

    // It calculates the distance the scene has to be moved so it is completely shown on the screen.
    // This will be used when instead of the orthographic projection we use a perspective projection.
    public float calculateDepth(float angleOfVision) {
        float depthForX, depthForY, largestDepth;
        float angleInRadians = (float)Math.toRadians(angleOfVision);
        if (!rangesCalculated) calculateRanges();

        depthForX = depthForY = 0f;
        if (screenHeight < screenWidth) {
            // Landscape
            depthForY = 0.5f*(maxY - minY) / (float)Math.tan(0.5f*angleInRadians);
            depthForX = 0.5f*(maxX - minX) / (aspectRatio*(float)Math.tan(0.5f*angleInRadians));
            Log.i("Depth", "Landscape orientation");
        } else {

        }

        largestDepth = depthForX;
        if (largestDepth < depthForY) largestDepth = depthForY;
        Log.i("Depth", "Largest depth = " + largestDepth);

        return largestDepth;
    }

    /*
    public float calculateDepth(MeshResults meshResults) {
        float min, max, depthMin, depthMax;
        float[] coordinates = meshResults.getVerticesRef();

        if (null == meshResults) return 1f;

        min = max = 0f;
        if (coordinates.length > 0) {
            min = coordinates[0];
            max = coordinates[0];
        }

        // Minimum and maximum coordinates' values
        for (int i = 0; i < coordinates.length; i++) {
            if (min > coordinates[i]) min = coordinates[i];
            if (max < coordinates[i]) max = coordinates[i];
        }

        depthMin = min - (max - min);
        depthMax = max + (max - min);

        return (Math.abs(depthMin) < Math.abs(depthMax)) ? Math.abs(depthMax) : Math.abs(depthMin);
    }
    //*/

    public void setDepth(MeshResults meshResults,
                         boolean isAxialSymmetry) {
        rangesCalculated = false;
        final int dimension = Constants.PROBLEM_DIMENSION;
        float[] coordinates = meshResults.getVerticesRef();
        float smallestX, smallestY, largestX, largestY;

        if (null == meshResults) return;
        if (dimension > coordinates.length) return;

        // The most distant vertex (its distance)
        if (isAxialSymmetry) {
            smallestX = Results.smallestValue(dimension, coordinates, 0);
            largestX  = Results.largestValue(dimension, coordinates, 0);
            smallestY = Results.smallestValue(dimension, coordinates, 1);
            largestY  = Results.largestValue(dimension, coordinates, 1);
            if (Math.abs(largestX - smallestX) > Math.abs(largestY - smallestY))
                depth = Math.abs(largestX - smallestX);
            else
                depth = Math.abs(largestY - smallestY);
        } else {
            depth = Results.largestMagnitude(dimension, coordinates);
        }
        //Log.i("DEPTH", "Depth = " + depth);
    }

    public void dontAllowUpdates() { stopUpdates = true; }
    public void allowUpdates() { stopUpdates = false; }

    public void print(String TAG, String label) {
        if (!rangesCalculated) calculateRanges();
        Log.i(TAG, "Begin --------------------------------------");
        Log.i(TAG, label);
        Log.i(TAG, "X ranges = " + minX + "   " + maxX);
        Log.i(TAG, "Y ranges = " + minY + "   " + maxY);
        Log.i(TAG, "Center   = " + centerX + "   " + centerY + "   " + centerZ);
        Log.i(TAG, "Width    = " + (maxX-minX));
        Log.i(TAG, "Height   = " + (maxY-minY));
        Log.i(TAG, "Depth (+-)   = " + depth);
        Log.i(TAG, "Aspect ratio = " + aspectRatio);
        Log.i(TAG, "End ----------------------------------------");
    }

    public void copy(PlotParameters plotParameters) {
        screenWidth = plotParameters.screenWidth;
        screenHeight = plotParameters.screenHeight;
        zoomFactor = plotParameters.zoomFactor;
        centerX = plotParameters.centerX;
        centerY = plotParameters.centerY;
        centerZ = plotParameters.centerZ;
        depth = plotParameters.depth;

        stopUpdates = false;
        calculateRanges();
        rangesCalculated = true;
    }
}
