package com.android.airhockey1.data;

import android.provider.ContactsContract;
import android.util.Log;

import com.android.airhockey1.Constants;
import com.android.airhockey1.util.Geometry;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by ernesto on 24/02/16.
 */
public class ProblemData {
    private int typeOfProblem;
    private int numberOfMeshNodes;

    public class Plate {
        private float thickness;

        Plate() {
            thickness = 0.2f;
        }

        public void setThickness(float thickness){
            this.thickness = thickness;
        }
        public float getThickness(){
            return thickness;
        }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                dos.writeFloat(thickness);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis) {
            try {
                thickness = dis.readFloat();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public class Section {
        Section() { }
    }

    public class SolidOfRevolution {
        private Geometry.Line axis;
        private int numberOfSlices;

        SolidOfRevolution() {
            axis = null;
            numberOfSlices = 20;
        }

        public void setAxis(Geometry.Line axis){ this.axis = axis; }
        public void clearAxis(){ axis = null; }
        public Geometry.Line getAxis() { return axis; }

        public void setNumberOfSlices(int numberOfSlices) { this.numberOfSlices = numberOfSlices; }
        public int getNumberOfSlices() { return numberOfSlices; }

        public boolean writeToFile(DataOutputStream dos) {
            try {
                if (null != axis) dos.writeInt(axis.getId());
                else              dos.writeInt(Constants.INVALID_ID);
                dos.writeInt(numberOfSlices);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public boolean readFromFile(DataInputStream dis, Lines lines) {
            int id;
            try {
                id = dis.readInt();
                numberOfSlices = dis.readInt();
                if (Constants.INVALID_ID != id) axis = lines.getElementById(id);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public Plate plate;
    public Section section;
    public SolidOfRevolution solidOfRevolution;

    public ProblemData() {
        typeOfProblem = Constants.PLANE_STRESS;
        numberOfMeshNodes = 50;
        plate = new Plate();
        section = new Section();
        solidOfRevolution = new SolidOfRevolution();
    }

    public void setProblemType(int type) {
        typeOfProblem = type;
    }

    public int getTypeOfProblem() {
        return typeOfProblem;
    }
    public int getNumberOfMeshNodes() {
        return numberOfMeshNodes;
    }
    public int setMeshSize(int numberOfMeshNodes) {
        return this.numberOfMeshNodes = numberOfMeshNodes;
    }

    public boolean writeToFile(DataOutputStream dos) {
        try {
            dos.writeInt(typeOfProblem);
            dos.writeInt(numberOfMeshNodes);

            Log.i("READING", "Type of problem before = " + typeOfProblem);
            Log.i("READING", "Number of mesh nodes before = " + numberOfMeshNodes);

            if (!plate.writeToFile(dos)) return false;
            if (!solidOfRevolution.writeToFile(dos)) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean readFromFile(DataInputStream dis, GeometryData geometry) {
        try {
            typeOfProblem = dis.readInt();
            numberOfMeshNodes = dis.readInt();

            Log.i("READING", "Type of problem = " + typeOfProblem);
            Log.i("READING", "Number of mesh nodes = " + numberOfMeshNodes);

            if (!plate.readFromFile(dis)) return false;
            if (!solidOfRevolution.readFromFile(dis, geometry.getLines())) return false;

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
