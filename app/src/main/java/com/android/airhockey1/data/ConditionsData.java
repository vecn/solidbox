package com.android.airhockey1.data;

import com.android.airhockey1.util.Conditions;

/**
 * Created by ernesto on 4/02/16.
 */
public class ConditionsData {
    NodesConditions nodesConditions;

    ConditionsData() {
        nodesConditions = new NodesConditions();
    }

    public void addNodeConditions(Conditions.AppliedConditions nodeConditions) {
        nodesConditions.addElement(nodeConditions);
    }

}
