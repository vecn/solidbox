package com.android.airhockey1;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.invertM;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.multiplyMV;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;
import static android.opengl.Matrix.translateM;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import com.android.airhockey1.data.Lines;
import com.android.airhockey1.data.Nodes;
import com.android.airhockey1.data.PlotParameters;
import com.android.airhockey1.data.Points;
import com.android.airhockey1.data.ProblemData;
import com.android.airhockey1.data.Surfaces;
import com.android.airhockey1.objects.AxisOrientation;
import com.android.airhockey1.objects.Table;
import com.android.airhockey1.objects.VisualAxis;
import com.android.airhockey1.objects.VisualCircle;
import com.android.airhockey1.objects.VisualLine;
import com.android.airhockey1.objects.VisualObject;
import com.android.airhockey1.objects.VisualSurface;
import com.android.airhockey1.programs.BatchTextProgram;
import com.android.airhockey1.programs.ColorShaderProgram;
import com.android.airhockey1.programs.TextureShaderProgram;
import com.android.airhockey1.util.Axis;
import com.android.airhockey1.util.Conditions;
import com.android.airhockey1.util.GLText;
import com.android.airhockey1.util.Geometry;
import com.android.airhockey1.util.Geometry.Vector;
import com.android.airhockey1.util.GeometryGLSurfaceView.Drag;
import com.android.airhockey1.util.GeometryGLSurfaceView.Drags;
import com.android.airhockey1.util.ModelHelper;
import com.android.airhockey1.util.TextureHelper;

import com.android.airhockey1.util.Geometry.*;
import com.android.airhockey1.data.GeometryData;

import com.android.airhockey1.util.Conditions.Force;
import com.android.airhockey1.util.Conditions.FixedDisplacement;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import nb.geometricBot.Model;

public class AirHockeyRenderer implements Renderer {

    private class ImportantFlags {
        // Variables to handle the type of drawing
        public boolean anActionHasBeenExecuted;
        public boolean aSurfaceHasBeenAdded;
        public boolean magneticGrid;
        public boolean newDrag;
        public boolean newZooming;
        public boolean doingZooming;
        public boolean zoomingFinished;
        public boolean showGrid;
        public boolean newExtrude;

        ImportantFlags() { }

        public void setAllTrue() {
            anActionHasBeenExecuted = true;

            aSurfaceHasBeenAdded = true;
            magneticGrid = true;
            newDrag = true;
            newZooming = true;
            doingZooming = true;
            zoomingFinished = true;
            showGrid = true;
            newExtrude = true;
        }

        public void setAllFalse() {
            anActionHasBeenExecuted = false;
            aSurfaceHasBeenAdded = false;
            magneticGrid = false;
            newDrag = false;
            newZooming = false;
            doingZooming = false;
            zoomingFinished = false;
            showGrid = false;
            newExtrude = false;
        }
    }

    private final Context context;

    // Matrices for OpenGL
    private final float[] projectionMatrix = new float[16];
    private final float[] modelMatrix = new float[16];
    private final float[] viewMatrix = new float[16];
    private final float[] viewProjectionMatrix = new float[16];
    private final float[] invertedViewProjectionMatrix = new float[16];
    private final float[] modelViewProjectionMatrix = new float[16];
    private final float[] aux = new float[16];

    // Objects that can be drawn on the screen
    private Table table;
    private VisualCircle visualNode;
    private VisualCircle visualPoint;
    private VisualLine visualLines;
    private VisualSurface visualSurfaces;
    private AxisOrientation axisOrientation;
    private VisualAxis visualAxis;
    private VisualObject visualPartialFixedNode;
    private VisualObject visualFixedNode;
    private VisualObject visualForces;
    private VisualObject visualLinearForces;

    private TextureShaderProgram textureProgram;
    private BatchTextProgram batchTextShaderProgram;
    private int texture;

    // Flags used to update the screen
    private ImportantFlags flags;

    // OpenGL programs
    private ColorShaderProgram colorProgram;

    // Geometry (contains the set of nodes, lines and surfaces)
    private GeometryData geometry;

    // Axis (the x and y axis and the grid)
    Axis axis;

    // Queue used when a line is being drawn
    private Queue<Point> pointsToDrawLines;

    // Queue used for boolean operation with polygons
    private Queue<Surface> surfacesBooleanOperation;

    // Queue used when drawing a predefined figure (rectangle, circle)
    private Queue<SimplePoint> pointsToDrawShape;

    // Previous and current action done by the user (action means that the user pressed a button
    // to do something)
    private int previousAction;
    private int currentAction;

    // Previous and current event done by the user (an event means if the user did a touch or
    // a drag on the screen)
    private int previousEvent;
    private int currentEvent;

    private Surface currentSurface;
    private Conditions.Force currentForce;
    private Conditions.FixedDisplacement currentFixedDisplacement;

    // To handle when zooming
    private Zoom zoom;

    // To extrude lines
    private ExtrudeLine extrudeLine;

    // Colors to draw the nodes
    private float[] colorNode;
    private float[] colorSelectedNode;

    // Colors to draw lines
    private float[] colorLine;
    private float[] colorSelectedLine;

    // Plot parameters
    PlotParameters plotParameters;

    // For text on the screen (labels, titles, etc)
    GLText glText;

    // To draw a regular polygon
    int numberOfSidesRegularPolygon;

    // To split a segment
    int numberOfDivisions;

    // Problem data
    ProblemData problemData;

    // Force with the largest magnitude
    float largestForce;

    // Queue with the needed updates
    BlockingQueue<Integer> updates;

    public AirHockeyRenderer(Context context,
                             GeometryData geometry,
                             PlotParameters plotParameters,
                             ProblemData problemData) {
        this.context = context;
        table = new Table();
        pointsToDrawLines = new LinkedList();
        surfacesBooleanOperation = new LinkedList();
        pointsToDrawShape = new LinkedList();

        flags = new ImportantFlags();
        flags.setAllFalse();
        flags.showGrid = true;
        flags.newDrag = true;
        flags.newZooming = true;
        flags.newExtrude = true;

        currentSurface = new Surface();
        previousAction = currentAction = Constants.INITIAL_VALUE_INT;

        previousEvent = currentEvent = Constants.INITIAL_VALUE_INT;

        zoom = new Zoom();
        extrudeLine = new ExtrudeLine();

        // This data has to be passed from outside
        //this.geometry = new GeometryData();
        this.geometry = geometry;
        this.currentForce = new Force();
        this.currentFixedDisplacement = new FixedDisplacement();
        this.plotParameters = plotParameters;

        // Color for nodes
        colorNode = new float[3];
        colorSelectedNode = new float[3];
        colorNode[0] = 0.7f;
        colorNode[1] = 0f;
        colorNode[2] = 0f;
        colorSelectedNode[0] = 1f;
        colorSelectedNode[1] = 0.1f;
        colorSelectedNode[2] = 0.1f;

        // Color for lines
        colorLine = new float[3];
        colorSelectedLine = new float[3];
        colorLine[0] = 0f;
        colorLine[1] = 0.7f;
        colorLine[2] = 0f;
        colorSelectedLine[0] = 0.8f;
        colorSelectedLine[1] = 1f;
        colorSelectedLine[2] = 0.8f;
        colorSelectedLine[2] = 0.8f;

        Log.i("VisualLines", "Geometry lines size = " + geometry.getLines().getSize());
        if ((Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem()) &&
                (null != problemData.solidOfRevolution.getAxis())) {
            visualLines = new VisualLine(geometry.getLines(), true, problemData.solidOfRevolution.getAxis());
        } else {
            visualLines = new VisualLine(geometry.getLines(), false, null);
        }
        visualSurfaces = new VisualSurface(geometry.getSurfaces());

        axisOrientation = null;

        glText = null;

        numberOfSidesRegularPolygon = 5;
        numberOfDivisions = 0;

        this.problemData = problemData;

        largestForce = 0f;

        updates = new LinkedBlockingQueue();
    }

    public void setCurrentAction(int currentAction) {
        // Saves the last action
        this.previousAction = this.currentAction;
        // Records what type of action is being executed
        this.currentAction = currentAction;
    }

    public void activateMagneticGrid() {
        flags.magneticGrid = true;
    }

    public void deactivateMagneticGrid() {
        flags.magneticGrid = false;
    }

    public void switchStatusMagneticGrid() {
        if (flags.magneticGrid) flags.magneticGrid = false;
        else                    flags.magneticGrid = true;
    }

    public void handleTouchPress(float normalizedX, float normalizedY) {
        float radius;
        Node node;
        Point point;
        Line line;
        Surface surface;
        Rectangle rectangle;
        RegularPolygon regularPolygon;
        Circle circle;
        SimplePoint p1, p2, gridPoint, finalPoint;
        SimplePoint worldSpacePoint = convertNormalized2DPointToWorldSpace2DPoint(
                normalizedX,
                normalizedY,
                invertedViewProjectionMatrix
        );

        //Log.i("Clicking", "" + normalizedX + "  " + normalizedY );
        //Log.i("Clicking", "" + worldSpacePoint.x + "  " + worldSpacePoint.y + "  " + worldSpacePoint.z);

        // If the current and last action are different we may have to do something
        if (previousAction != Constants.INITIAL_VALUE_INT) manageChangeInActions();

        manageEvents(Constants.EVENT_PRESS);

        finalPoint = worldSpacePoint;
        if (flags.magneticGrid) {
            gridPoint = getPointOnTheGrid(worldSpacePoint.x, worldSpacePoint.y);
            finalPoint = gridPoint;
        }

        Log.i("Clicking", "Type of action  = " + currentAction);

        // Actions
        switch (currentAction) {

            case Constants.DRAWING_A_NODE:
                geometry.addNode(new Node(finalPoint.x, finalPoint.y, 0f));
                break;

            case Constants.DRAWING_A_LINE:
                // Detects the selected node using the original touched point
                node = detectSelectedNode(worldSpacePoint.x, worldSpacePoint.y);
                // We add the node to the queue if it is valid
                if(node != null){
                    Log.i("Clicking", "A node is been added.");
                    pointsToDrawLines.add(node.getPoint());
                } else {
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two nodes we can draw the line
                if (pointsToDrawLines.size() == 2) {
                    geometry.addLine(new Line(pointsToDrawLines.poll(), pointsToDrawLines.poll()));
                    updates.add(Constants.CAD_UPDATE_LINES);
                }
                break;

            case Constants.DRAWING_A_SURFACE:
                //*
                // Detects the selected line using the original touched point
                line = detectSelectedLine(worldSpacePoint.x, worldSpacePoint.y);
                if (line != null){
                    updates.add(Constants.CAD_UPDATE_LINES);
                    line.propertiesInCAD.select();
                    currentSurface.addLine(line);
                    Log.i("MODEL", "A line has been added.");
                    Log.i("MODEL", "" + line.propertiesInCAD.isSelected());
                } else {
                    currentAction = Constants.DOING_NOTHING;
                    currentSurface.clear();
                    unselectLines();
                    updates.add(Constants.CAD_UPDATE_LINES);
                }

                // Detects if the user has defined a closed surface
                if (currentSurface.isAValidSurface()) {
                    Log.i("Surfaces", "A valid surface has been added.");
                    // We finish the construction of the surface
                    currentSurface.commit();
                    // Mesh the surface so we can draw it easier later;
                    currentSurface.setModel(currentSurface.createModel(currentSurface));
                    currentSurface.meshSurface();
                    // We put it in our geometry
                    geometry.addSurface(currentSurface);
                    // We create a new current surface
                    currentSurface = new Surface();
                    unselectLines();
                    updates.add(Constants.CAD_UPDATE_LINES);
                    updates.add(Constants.CAD_UPDATE_SURFACES);
                    currentAction = Constants.DOING_NOTHING;
                }
                //*/
                break;
            case Constants.SELECTING_A_POINT_TO_APPLY_FORCE:
                // Detects the selected node using the original touched point
                point = detectSelectedPoint(worldSpacePoint.x, worldSpacePoint.y);
                Log.i("Current Force", "" + currentForce.getForceX() + "  " + currentForce.getForceY());
                if (point != null){
                    point.copyForce(currentForce);
                    updates.add(Constants.CAD_UPDATE_FORCES);
                    //node.propertiesInCAD.select();
                }
                break;
            case Constants.SELECTING_A_POINT_TO_FIX_DISPLACEMENT:
                // Detects the selected node using the original touched point
                point = detectSelectedPoint(worldSpacePoint.x, worldSpacePoint.y);
                Log.i("SUPPORTS", "Imposing");
                if (point != null){
                    Log.i("SUPPORTS", "Point selected");
                    point.copyFixedDisplacement(currentFixedDisplacement);
                    //node.propertiesInCAD.select();
                }
                break;
            case Constants.SELECTING_A_SURFACE_FOR_UNIFY_BOOLEAN_OPERATION:
                Log.i("Unify", "Unifying");
                // Detects the selected surface using the original touched point
                surface = detectSelectedSurface(worldSpacePoint.x, worldSpacePoint.y);
                if (null != surface) {
                    // When is selected we add it to the queue
                    surfacesBooleanOperation.add(surface);
                    Log.i("UNIFY", "Surface id = " + surface.getId());
                    Log.i("UNIFY", "Surface is special = " + surface.shapeInfo.isSpecialShape() +
                            "  " + surface.shapeInfo.getTypeOfSpecialShape());
                } else {
                    // Otherwise we stop this action
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two surfaces we can perform the operation
                if (surfacesBooleanOperation.size() == 2) {
                    unifySurfaces(surfacesBooleanOperation.poll(), surfacesBooleanOperation.poll());
                    currentAction = Constants.DOING_NOTHING;
                }
                break;
            case Constants.SELECTING_A_SURFACE_FOR_INTERSECT_BOOLEAN_OPERATION:
                Log.i("Intersect", "Intersecting");
                // Detects the surface node using the original touched point
                surface = detectSelectedSurface(worldSpacePoint.x, worldSpacePoint.y);
                if (null != surface) {
                    // When is selected we add it to the queue
                    surfacesBooleanOperation.add(surface);
                } else {
                    // Otherwise we stop this action
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two surfaces we can perform the operation
                if (surfacesBooleanOperation.size() == 2) {
                    intersectSurfaces(surfacesBooleanOperation.poll(), surfacesBooleanOperation.poll());
                    currentAction = Constants.DOING_NOTHING;
                }
                break;
            case Constants.SELECTING_A_SURFACE_FOR_SUBSTRACT_BOOLEAN_OPERATION:
                Log.i("Substract", "Substracting");
                // Detects the surface node using the original touched point
                surface = detectSelectedSurface(worldSpacePoint.x, worldSpacePoint.y);
                if (null != surface) {
                    // When is selected we add it to the queue
                    surfacesBooleanOperation.add(surface);
                } else {
                    // Otherwise we stop this action
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two surfaces we can perform the operation
                if (surfacesBooleanOperation.size() == 2) {
                    substractSurfaces(surfacesBooleanOperation.poll(), surfacesBooleanOperation.poll());
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.SELECTING_A_SURFACE_FOR_DIFFERENCE_BOOLEAN_OPERATION:
                Log.i("Difference", "Difference");
                // Detects the surface node using the original touched point
                surface = detectSelectedSurface(worldSpacePoint.x, worldSpacePoint.y);
                if (null != surface) {
                    // When is selected we add it to the queue
                    surfacesBooleanOperation.add(surface);
                } else {
                    // Otherwise we stop this action
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two surfaces we can perform the operation
                if (surfacesBooleanOperation.size() == 2) {
                    differenceSurfaces(surfacesBooleanOperation.poll(), surfacesBooleanOperation.poll());
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.SELECTING_A_SURFACE_FOR_COMBINE_BOOLEAN_OPERATION:
                Log.i("Combine", "Combine");
                // Detects the surface node using the original touched point
                surface = detectSelectedSurface(worldSpacePoint.x, worldSpacePoint.y);
                if (null != surface) {
                    // When is selected we add it to the queue
                    surfacesBooleanOperation.add(surface);
                } else {
                    // Otherwise we stop this action
                    currentAction = Constants.DOING_NOTHING;
                }
                // When we have two surfaces we can perform the operation
                if (surfacesBooleanOperation.size() == 2) {
                    combineSurfaces(surfacesBooleanOperation.poll(), surfacesBooleanOperation.poll());
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.DRAWING_A_RECTANGLE:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new SimplePoint(finalPoint.x, finalPoint.y, finalPoint.z));

                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    rectangle = new Rectangle(0.5f*(p2.x + p1.x), 0.5f*(p2.y + p1.y),
                            Math.abs(p2.x - p1.x), Math.abs(p2.y - p1.y));
                    rectangle.addToGeometry(geometry);
                    updates.add(Constants.CAD_UPDATE_LINES);
                    updates.add(Constants.CAD_UPDATE_SURFACES);
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.DRAWING_A_REGULAR_POLYGON:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new SimplePoint(finalPoint.x, finalPoint.y, finalPoint.z));

                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    regularPolygon = new RegularPolygon(numberOfSidesRegularPolygon,
                            p1.x, p1.y, p2.x, p2.y);
                    regularPolygon.addToGeometry(geometry);
                    updates.add(Constants.CAD_UPDATE_LINES);
                    updates.add(Constants.CAD_UPDATE_SURFACES);
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.DRAWING_A_CIRCLE:
                // We use the finalPoints to draw the shape because it may be on the magnetic grid.
                pointsToDrawShape.add(new SimplePoint(finalPoint.x, finalPoint.y, finalPoint.z));
                if (pointsToDrawShape.size() == 2) {
                    p1 = pointsToDrawShape.poll();
                    p2 = pointsToDrawShape.poll();
                    circle = new Circle(p1, p2, 20);
                    circle.addToGeometry(geometry);
                    updates.add(Constants.CAD_UPDATE_LINES);
                    updates.add(Constants.CAD_UPDATE_SURFACES);
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.SPLITTING_SEGMENT:
                // Detects the selected line using the original touched point
                line = detectSelectedLine(worldSpacePoint.x, worldSpacePoint.y);

                if (null != line){
                    splitSegment(line);
                }
                currentAction = Constants.DOING_NOTHING;
                break;

            case Constants.SELECTING_ROTATION_AXIS:
                if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem()) {
                    // Detects the selected line using the original touched point
                    line = detectSelectedLine(worldSpacePoint.x, worldSpacePoint.y);

                    if (null != line){
                        problemData.solidOfRevolution.setAxis(line);
                        updates.add(Constants.CAD_UPDATE_LINES);
                    }
                    currentAction = Constants.DOING_NOTHING;
                }
                break;

            case Constants.SELECTING_A_LINE_TO_APPLY_FORCE:
                // Detects the selected node using the original touched point
                line = detectSelectedLine(worldSpacePoint.x, worldSpacePoint.y);
                Log.i("Current Force", "" + currentForce.getForceX() + "  " + currentForce.getForceY());
                if (line != null){
                    line.copyForce(currentForce);
                    updates.add(Constants.CAD_UPDATE_FORCES);
                }
                break;
        }
    }

    private void manageEvents(int eventHappening) {
        previousEvent = currentEvent;
        currentEvent = eventHappening;
    }

    private void combineSurfaces(Surface sA, Surface sB) {
        Model model, modelA, modelB;

        //modelA = TabFragment2.createModelFromSurface(sA);
        //modelB = TabFragment2.createModelFromSurface(sB);
        modelA = sA.getModel();
        modelB = sB.getModel();

        model = Model.combine(modelA, modelB);

        deleteWholeSurface(geometry, sA);
        deleteWholeSurface(geometry, sB);

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        // We clear the rotation axis
        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            problemData.solidOfRevolution.setAxis(null);
        // We report that some elements have been deleted
        updates.add(Constants.CAD_UPDATE_LINES);
        updates.add(Constants.CAD_UPDATE_SURFACES);
        updates.add(Constants.CAD_UPDATE_FORCES);
    }

    private void differenceSurfaces(Surface sA, Surface sB) {
        Model model, modelA, modelB;

        //modelA = TabFragment2.createModelFromSurface(sA);
        //modelB = TabFragment2.createModelFromSurface(sB);
        modelA = sA.getModel();
        modelB = sB.getModel();

        model = Model.difference(modelA, modelB);

        deleteWholeSurface(geometry, sA);
        deleteWholeSurface(geometry, sB);

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        // We clear the rotation axis
        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            problemData.solidOfRevolution.setAxis(null);
        // We report that some elements have been deleted
        updates.add(Constants.CAD_UPDATE_LINES);
        updates.add(Constants.CAD_UPDATE_SURFACES);
        updates.add(Constants.CAD_UPDATE_FORCES);
    }

    private void substractSurfaces(Surface sA, Surface sB) {
        Model model, modelA, modelB;

        //modelA = TabFragment2.createModelFromSurface(sA);
        //modelB = TabFragment2.createModelFromSurface(sB);
        modelA = sA.getModel();
        modelB = sB.getModel();

        model = Model.substract(modelA, modelB);

        deleteWholeSurface(geometry, sA);
        deleteWholeSurface(geometry, sB);

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        // We clear the rotation axis
        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            problemData.solidOfRevolution.setAxis(null);
        // We report that some elements have been deleted
        updates.add(Constants.CAD_UPDATE_LINES);
        updates.add(Constants.CAD_UPDATE_SURFACES);
        updates.add(Constants.CAD_UPDATE_FORCES);
    }

    private void intersectSurfaces(Surface sA, Surface sB) {
        Model model, modelA, modelB;

        //modelA = TabFragment2.createModelFromSurface(sA);
        //modelB = TabFragment2.createModelFromSurface(sB);
        modelA = sA.getModel();
        modelB = sB.getModel();

        model = Model.intersect(modelA, modelB);

        deleteWholeSurface(geometry, sA);
        deleteWholeSurface(geometry, sB);

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        // We clear the rotation axis
        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            problemData.solidOfRevolution.setAxis(null);
        // We report that some elements have been deleted
        updates.add(Constants.CAD_UPDATE_LINES);
        updates.add(Constants.CAD_UPDATE_SURFACES);
        updates.add(Constants.CAD_UPDATE_FORCES);
    }

    // Unify two surfaces
    private void unifySurfaces(Surface sA, Surface sB) {
        Model model, modelA, modelB;

        modelA = sA.getModel();
        modelB = sB.getModel();

        model = Model.unify(modelA, modelB);

        deleteWholeSurface(geometry, sA);
        deleteWholeSurface(geometry, sB);

        ModelHelper.createNodesAndLinesFromModel(model, geometry);

        // We clear the rotation axis
        if (Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem())
            problemData.solidOfRevolution.setAxis(null);
        // We report that some elements have been deleted
        updates.add(Constants.CAD_UPDATE_LINES);
        updates.add(Constants.CAD_UPDATE_SURFACES);
        updates.add(Constants.CAD_UPDATE_FORCES);
    }

    public static void deleteWholeSurface(GeometryData geometry, Surface surface){
        Circle circle;
        Rectangle rectangle;
        RegularPolygon regularPolygon;

        if (surface.shapeInfo.isSpecialShape()) {
            switch (surface.shapeInfo.getTypeOfSpecialShape()) {
                case Constants.TYPE_SHAPE_CIRCLE:
                    circle = surface.shapeInfo.getCircle();
                    circle.deleteWholeShapeFromGeometry(geometry);
                    break;
                case Constants.TYPE_SHAPE_RECTANGLE:
                    rectangle = surface.shapeInfo.getRectangle();
                    rectangle.deleteWholeShapeFromGeometry(geometry);
                    break;
                case Constants.TYPE_SHAPE_REGULAR_POLYGON:
                    regularPolygon = surface.shapeInfo.getRegularPolygon();
                    regularPolygon.deleteWholeShapeFromGeometry(geometry);
                    break;
            }
        } else {
            surface.deleteWholeSurfaceFromGeometry(geometry);
        }
    }

    // Do something if the previous and the current action are different. This is only useful for
    // those actions which are performed on more than one step.
    private void manageChangeInActions( ){
        if(previousAction != currentAction){
            switch (previousAction) {
                case Constants.DRAWING_A_NODE:
                    break;
                case Constants.DRAWING_A_LINE:
                    // Removes all the points in the queue
                    pointsToDrawLines.clear();
                    break;
                case Constants.DRAWING_A_REGULAR_POLYGON:
                    pointsToDrawShape.clear();
                    break;
                case Constants.DRAWING_A_RECTANGLE:
                    pointsToDrawShape.clear();
                    break;
                case Constants.DRAWING_A_CIRCLE:
                    pointsToDrawShape.clear();
                    break;
                case Constants.DRAWING_A_SURFACE:
                    currentSurface.clear();
                    break;
                case Constants.SELECTING_A_SURFACE_FOR_UNIFY_BOOLEAN_OPERATION:
                    surfacesBooleanOperation.clear();
                    break;
                case Constants.SELECTING_A_SURFACE_FOR_COMBINE_BOOLEAN_OPERATION:
                    surfacesBooleanOperation.clear();
                    break;
                case Constants.SELECTING_A_SURFACE_FOR_DIFFERENCE_BOOLEAN_OPERATION:
                    surfacesBooleanOperation.clear();
                    break;
                case Constants.SELECTING_A_SURFACE_FOR_SUBSTRACT_BOOLEAN_OPERATION:
                    surfacesBooleanOperation.clear();
                    break;
                case Constants.SELECTING_A_SURFACE_FOR_INTERSECT_BOOLEAN_OPERATION:
                    surfacesBooleanOperation.clear();
                    break;
            }
        }
    }

    private Node detectSelectedNode(float pX, float pY) {
        final float radiusSelection =
                Constants.VISUAL_NODE_RADIUS * Constants.VISUAL_NODE_FACTOR_SELECTION /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem();
        float selectionDistance;
        float smallestSelectionDistance;
        boolean wasANodeSelected = false;
        Node node, selectedNode;

        // Sees if there are nodes
        if (geometry.getNodes().getSize() == 0) return null;

        // Chooses a valid value for smallestSelectionDistance and selectedNode
        selectedNode = geometry.getNode(0);
        smallestSelectionDistance = (float) Math.sqrt(
                (pX - selectedNode.GetX()) * (pX - selectedNode.GetX()) +
                (pY - selectedNode.GetY()) * (pY - selectedNode.GetY()) );

        // Finds the smallest distance valid value
        for (int i = 0; i < geometry.numberOfNodes(); i++) {
            //node = setNodes.GetNextNode();
            node = geometry.getNode(i);
            selectionDistance = (float) Math.sqrt(
                    (pX - node.GetX()) * (pX - node.GetX()) +
                    (pY - node.GetY()) * (pY - node.GetY()) );
            // Is in the selection area??
            if (selectionDistance < radiusSelection) {
                wasANodeSelected = true;
                if (selectionDistance < smallestSelectionDistance) selectedNode = node;
            }
        }

        // When a node was not selected
        if(!wasANodeSelected) return null;

        return selectedNode;
    }

    private Point detectSelectedPoint(float pX, float pY) {
        final float radiusSelection =
                Constants.VISUAL_NODE_RADIUS * Constants.VISUAL_NODE_FACTOR_SELECTION /
                        plotParameters.getUnitLengthNormalizedCoordinateSystem();
        float selectionDistance;
        float smallestSelectionDistance;
        boolean wasAPointSelected = false;
        Point point, selectedPoint;

        // Sees if there are nodes
        if (geometry.getPoints().getSize() == 0) return null;

        // Chooses a valid value for smallestSelectionDistance and selectedNode
        selectedPoint = geometry.getPoint(0);
        smallestSelectionDistance = (float) Math.sqrt(
                (pX - selectedPoint.getX()) * (pX - selectedPoint.getX()) +
                        (pY - selectedPoint.getY()) * (pY - selectedPoint.getY()) );

        // Finds the smallest distance valid value
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            selectionDistance = (float) Math.sqrt(
                    (pX - point.getX()) * (pX - point.getX()) +
                            (pY - point.getY()) * (pY - point.getY()) );
            // Is in the selection area??
            if (selectionDistance < radiusSelection) {
                wasAPointSelected = true;
                if (selectionDistance < smallestSelectionDistance) selectedPoint = point;
            }
        }

        // When a node was not selected
        if(!wasAPointSelected) return null;

        return selectedPoint;
    }

    private Line detectSelectedLine(float pX, float pY) {
        Line selectedLine, line;
        float selectionDistance;
        float smallestSelectionDistance;

        float selectionWidth = Constants.VISUAL_LINE_WIDTH * Constants.VISUAL_LINE_FACTOR_SELECTION /
                plotParameters.getUnitLengthNormalizedCoordinateSystem();
        boolean wasALineSelected = false;
        Ray ray = new Ray(new SimplePoint(), new Vector());
        SimplePoint clickPoint = new SimplePoint(pX, pY, 0f);

        // Sees if there are lines to be selected
        if (geometry.numberOfLines() == 0) return null;

        // Sees if a line was selected

        // Chooses a valid value for smallestSelectionDistance and selectedNode
        selectedLine = geometry.getLine(0);
        ray.point.setCoordinates(
                selectedLine.getFirstPoint().getX(),
                selectedLine.getFirstPoint().getY(),
                selectedLine.getFirstPoint().getZ());
        ray.vector.setComponents(
                selectedLine.getLastPoint().getX() - selectedLine.getFirstPoint().getX(),
                selectedLine.getLastPoint().getY() - selectedLine.getFirstPoint().getY(),
                selectedLine.getLastPoint().getZ() - selectedLine.getFirstPoint().getZ());
        smallestSelectionDistance = Geometry.distanceBetween(clickPoint, ray);

        // Finds the smallest distance valid value
        for (int i = 0; i < geometry.numberOfLines(); i++) {
            //line = setLines.GetNextLine();
            line = geometry.getLine(i);
            ray.point.setCoordinates(
                    line.getFirstPoint().getX(),
                    line.getFirstPoint().getY(),
                    line.getFirstPoint().getZ());
            ray.vector.setComponents(
                    line.getLastPoint().getX() - line.getFirstPoint().getX(),
                    line.getLastPoint().getY() - line.getFirstPoint().getY(),
                    line.getLastPoint().getZ() - line.getFirstPoint().getZ());
            selectionDistance = Geometry.distanceBetween(clickPoint, ray);
            // Is in the selection area??
            if (selectionDistance < selectionWidth) {
                wasALineSelected = true;
                if (selectionDistance < smallestSelectionDistance) selectedLine = line;
            }
        }

        // When a line was not selected
        if(!wasALineSelected) return null;

        return selectedLine;
    }

    private Surface detectSelectedSurface(float pX, float pY) {
        Surface selectedSurface, surface;
        float selectionDistance;
        float smallestSelectionDistance;
        boolean wasASurfaceSelected;
        Model model;

        wasASurfaceSelected = false;
        if (geometry.numberOfSurfaces() == 0) return null;

        // Checks if the point is inside in a surface
        selectedSurface = geometry.getSurface(0);
        for (int i = 0; i < geometry.numberOfSurfaces(); i++) {
            selectedSurface = geometry.getSurface(i);
            //model = TabFragment2.createModelFromSurface(selectedSurface);
            model = selectedSurface.getModel();
            if (model.isPointInside(pX, pY)) {
                Log.i("Surfaces", "Surface has been selected!!");
                break;
            }
        }

        return selectedSurface;
    }

    // We'll convert these normalized device coordinates into world-space
    // coordinates. We'll pick a point on the near and far planes, and draw a
    // line between them. To do this transform, we need to first multiply by
    // the inverse matrix, and then we need to undo the perspective divide.
    private Ray convertNormalized2DPointToRay(float normalizedX, float normalizedY) {
        final float[] nearPointNdc = {normalizedX, normalizedY, -1, 1};
        final float[] farPointNdc =  {normalizedX, normalizedY,  1, 1};

        final float[] nearPointWorld = new float[4];
        final float[] farPointWorld = new float[4];

        multiplyMV(nearPointWorld, 0, invertedViewProjectionMatrix, 0, nearPointNdc, 0);
        multiplyMV(farPointWorld, 0, invertedViewProjectionMatrix, 0, farPointNdc, 0);

        // Why are we dividing by W? We multiplied our vector by an inverse
        // matrix, so the W value that we end up is actually the *inverse* of
        // what the projection matrix would create. By dividing all 3 components
        // by W, we effectively undo the hardware perspective divide.
        // We have to divide by W because OpenGL does that and not the projection matrix.
        divideByW(nearPointWorld);
        divideByW(farPointWorld);

        // We don't care about the W value anymore, because our points are now
        // in world coordinates.
        SimplePoint nearPointRay = new SimplePoint(nearPointWorld[0], nearPointWorld[1], nearPointWorld[2]);

        SimplePoint farPointRay = new SimplePoint(farPointWorld[0], farPointWorld[1], farPointWorld[2]);

        return new Ray(nearPointRay, Geometry.vectorBetween(nearPointRay, farPointRay));
    }

    // Converts a point in the normalized coordinates into the  world space coordinates. In this case
    // we do not need to worry about the "w" component because we are using a ortographic projection.
    public static SimplePoint convertNormalized2DPointToWorldSpace2DPoint(float normalizedX,
                                                                          float normalizedY,
                                                                          float[] invertedTransformationMatrix) {
        final float[] normalizedPoint = {normalizedX, normalizedY, 0, 1};
        final float[] worldSpacePoint = new float[4];
        multiplyMV(worldSpacePoint, 0, invertedTransformationMatrix, 0, normalizedPoint, 0);
        return new SimplePoint(worldSpacePoint[0], worldSpacePoint[1], worldSpacePoint[2]);
    }

    private void divideByW(float[] vector) {
        vector[0] /= vector[3];
        vector[1] /= vector[3];
        vector[2] /= vector[3];
    }

    // When the dragging has been finished
    public void stopDragging() {
        flags.newDrag = true;
    }

    public void handleTouchDrags(Drags drags) {
        Line line;
        SimplePoint worldSpacePoint;

        manageEvents(Constants.EVENT_DRAG);

        // Detects the selected nodes for each drag (it could be also lines)
        detectDragSelectedNodes(drags);

        // When this happens a new zoom may be performed
        if ((2 != drags.size()) || (previousEvent != Constants.EVENT_DRAG)) flags.newZooming = true;

        // Reports when the zoom stops
        if ((2 != drags.size()) && flags.doingZooming) {
            flags.doingZooming = false;
            flags.zoomingFinished = true;
        }

        switch (currentAction) {
            case Constants.EXTRUDING_SEGMENT:
                if (1 == drags.size()) {

                    worldSpacePoint = getEndingWorldSpacePointsFromDrag(
                            drags.getDrag(0),
                            invertedViewProjectionMatrix
                    );
                    line = null;
                    if (flags.newExtrude) {
                        // Detects the selected line
                        line = detectSelectedLine(worldSpacePoint.x, worldSpacePoint.y);
                        if (null != line) {
                            handleExtrude(line, worldSpacePoint.x, worldSpacePoint.y,
                                    drags.getDrag(0).getID(), drags.getDrag(0).hasStopped());
                            flags.newExtrude = false;
                            currentAction = Constants.EXTRUDING_SEGMENT;
                        } else {
                            flags.newExtrude = true;
                            currentAction = Constants.DOING_NOTHING;
                        }
                    } else {
                        handleExtrude(line, worldSpacePoint.x, worldSpacePoint.y,
                                drags.getDrag(0).getID(), drags.getDrag(0).hasStopped());

                        if (drags.getDrag(0).hasStopped()) {
                            flags.newExtrude = true;
                            currentAction = Constants.DOING_NOTHING;
                            extrudeLine.stopExtrude();
                        }
                    }
                }

                //currentAction = Constants.DOING_NOTHING;

                break;

            default:
                Log.i("DRAGGING", "Dragging something");

                switch (drags.size()) {
                    case 1:
                        // If there is only one drag then the user may be panning or dragging
                        // one object
                        handleOneDrag(drags.getDrag(0));
                        break;
                    case 2:
                        // If there are two drags the user may be dragging two objects or
                        // zooming
                        handleTwoDrags(drags);
                        break;
                    default:
                        // When there are 0 or more than two drags the user is dragging nodes
                        handleMoreThanTwoDrags(drags);
                        break;
                }
                break;
        }

        // When we stop the zooming, we create again the figures for supports and nodes
        if (flags.zoomingFinished) {
            visualNode = new VisualCircle(
                    Constants.VISUAL_NODE_RADIUS/plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                    Constants.VISUAL_NODE_POINT_NUMPOINTS
            );
            visualPoint = new VisualCircle(
                    Constants.VISUAL_POINT_RADIUS/plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                    Constants.VISUAL_NODE_POINT_NUMPOINTS
            );
            visualPartialFixedNode.createPartialFixedSupport(0f, 0f,
                    Constants.SUPPORT_TRIANGLE_BASE/plotParameters.getUnitLengthNormalizedCoordinateSystem());
            visualFixedNode.createFixedSupport(0f, 0f,
                    Constants.SUPPORT_TRIANGLE_BASE / plotParameters.getUnitLengthNormalizedCoordinateSystem());
            flags.zoomingFinished = false;
        }
    }

    private void handleOneDrag(Drag drag) {
        Node node;
        SimplePoint initialWorldSpacePoint, endingWorldSpacePoint;
        SimplePoint finalPoint, gridPoint;

        // Gets the drag points (initially in normalized device coordinates) in world-space coordinates
        initialWorldSpacePoint = getInitialWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);
        endingWorldSpacePoint = getEndingWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);

        /*
        Log.i("PANNING", "++++++++++++++++++++++++++++");
        Log.i("PANNING", "ID = " + drag.getID());
        Log.i("PANNING", "N-SC = " + drag.getBeginningPosition().x + "  " + drag.getBeginningPosition().y);
        Log.i("PANNING", "N-SC = " + drag.getEndingPosition().x + "  " + drag.getEndingPosition().y);
        Log.i("PANNING", "W-SC = " + initialWorldSpacePoint.x + "  " + initialWorldSpacePoint.y);
        Log.i("PANNING", "W-SC = " + endingWorldSpacePoint.x + "  " + endingWorldSpacePoint.y);
        //*/

        // If the magnetic grid is activated we choose the closest point on the grid
        finalPoint = endingWorldSpacePoint;
        if (flags.magneticGrid){
            gridPoint = getPointOnTheGrid(endingWorldSpacePoint.x, endingWorldSpacePoint.y);
            finalPoint =  gridPoint;
        }

        // Node related to the drag
        node = drag.getNode();
        if (null == node) {
            // The user is panning because didn't select a node
            handlePanning(
                    initialWorldSpacePoint.x, initialWorldSpacePoint.y,
                    endingWorldSpacePoint.x, endingWorldSpacePoint.y,
                    plotParameters,
                    projectionMatrix
            );
            updates.add(Constants.CAD_UPDATE_AXIS);
        } else {
            // The user is dragging a node
            dragNode(node, finalPoint.x, finalPoint.y, drag.hasStopped());
        }
    }

    private void handleTwoDrags(Drags drags) {
        SimplePoint aInNDC, bInNDC, aInWSC, bInWSC;
        SimplePoint zoomingCenterInNDC, zoomingCenterInWSC;
        float radiusInNDC;

        if ((null == drags.getDrag(0).getNode()) || (null == drags.getDrag(1).getNode())) {
            // In this case the user is zooming
            if (flags.newZooming) {
                aInNDC = drags.getDrag(0).getBeginningPosition();
                bInNDC = drags.getDrag(1).getBeginningPosition();
                aInWSC = convertNormalized2DPointToWorldSpace2DPoint(
                        aInNDC.x,
                        aInNDC.y,
                        invertedViewProjectionMatrix
                );
                bInWSC = convertNormalized2DPointToWorldSpace2DPoint(
                        bInNDC.x,
                        bInNDC.y,
                        invertedViewProjectionMatrix
                );

                radiusInNDC = 0.5f*Geometry.distanceBetweenPoints(aInNDC, bInNDC);
                zoomingCenterInNDC = Geometry.middlePoint(aInNDC, aInNDC);
                zoomingCenterInWSC = Geometry.middlePoint(aInWSC, bInWSC);
                zoom.setData(plotParameters.getZoomFactor(),radiusInNDC, zoomingCenterInNDC, zoomingCenterInWSC,
                        new SimplePoint(plotParameters.getCenterX(), plotParameters.getCenterY(),
                                plotParameters.getCenterZ()));

                flags.newZooming = false;
                flags.doingZooming = true;

                /*
                Log.i("ZOOMING", "Setting data for zooming");
                Log.i("ZOOMING", "WSC zooming center = " + zoomingCenterInNDC.x + "   " + zoomingCenterInNDC.y);
                Log.i("ZOOMING", "WSC zooming center = " + zoomingCenterInWSC.x + "   " + zoomingCenterInWSC.y);
                //*/

            }

            // When one of the pointers is close to a node, then the drag will be related to this
            // node. But after we finish zooming we do not want the node to be moved, so we have
            // to disconnect the node with the pointer.
            if (drags.getDrag(0).hasNodeBeenSet()) drags.getDrag(0).removeNodeRelatedToDrag();
            if (drags.getDrag(1).hasNodeBeenSet()) drags.getDrag(1).removeNodeRelatedToDrag();

            aInNDC = drags.getDrag(0).getEndingPosition();
            bInNDC = drags.getDrag(1).getEndingPosition();

            //Log.i("ZOOMING", "Old view center = " + plotParameters.getCenterX() + "  " + plotParameters.getCenterY());
            handleZooming(aInNDC, bInNDC, zoom, plotParameters, projectionMatrix);
            // We report that the clip area has changed
            updates.add(Constants.CAD_UPDATE_AXIS);
        } else {
            // In this case the user is dragging two nodes
            handleOneDrag(drags.getDrag(0));
            handleOneDrag(drags.getDrag(1));
        }
    }

    private void handleMoreThanTwoDrags(Drags drags) {
        for (int i = 0; i < drags.size(); i++) handleOneDrag(drags.getDrag(i));
    }

    public static SimplePoint getInitialWorldSpacePointsFromDrag(Drag drag,
                                                                 float[] invertedTransformationMatrix) {
        SimplePoint initialNormSpacePoint = drag.getBeginningPosition();
        SimplePoint initialWorldSpacePoint =
                convertNormalized2DPointToWorldSpace2DPoint(
                        initialNormSpacePoint.x,
                        initialNormSpacePoint.y,
                        invertedTransformationMatrix
                );
        return initialWorldSpacePoint;
    }

    public static SimplePoint getEndingWorldSpacePointsFromDrag(Drag drag,
                                                    float[] invertedTransformationMatrix) {
        SimplePoint endingNormSpacePoint = drag.getEndingPosition();
        SimplePoint endingWorldSpacePoint =
                convertNormalized2DPointToWorldSpace2DPoint(
                        endingNormSpacePoint.x,
                        endingNormSpacePoint.y,
                        invertedTransformationMatrix
                );
        return endingWorldSpacePoint;
    }

    private void detectDragSelectedNodes(Drags drags) {
        Node node;
        SimplePoint initialWorldSpacePoint;
        Drag drag;

        // Detect selected nodes
        for (int i = 0; i < drags.size(); i++) {
            drag = drags.getDrag(i);
            // We try to find the selected node only at the beginning
            if (!drag.hasNodeBeenSet()) {
                initialWorldSpacePoint = getEndingWorldSpacePointsFromDrag(drag, invertedViewProjectionMatrix);
                node = detectSelectedNode(initialWorldSpacePoint.x, initialWorldSpacePoint.y);
                drag.setNodeRelatedToDrag(node);
            }
        }
    }

    private void dragNode(Node node, float x, float y, boolean hasStopped) {

        if (node.getPoint().surfaceProperties.isItPartOfASurface()) {
            //Log.i("DRAG", "Node is part of a surface");
            Surface surface = node.getPoint().surfaceProperties.getSurface();
            if (surface.shapeInfo.isSpecialShape()) {
                //Log.i("DRAG", "Node is part of a SPECIAL surface");
                switch (surface.shapeInfo.getTypeOfSpecialShape()) {
                    case Constants.TYPE_SHAPE_CIRCLE:
                        Circle circle = surface.shapeInfo.getCircle();
                        circle.handleControlNodeMovement(node, x, y, hasStopped);
                        break;

                    case Constants.TYPE_SHAPE_RECTANGLE:
                        Rectangle rectangle = surface.shapeInfo.getRectangle();
                        rectangle.handleControlNodeMovement(node, x, y, hasStopped);
                        break;

                    case Constants.TYPE_SHAPE_REGULAR_POLYGON:
                        RegularPolygon regularPolygon = surface.shapeInfo.getRegularPolygon();
                        regularPolygon.handleControlNodeMovement(node, x, y, hasStopped);
                        break;
                }
            } else {
                //Log.i("DRAG", "Node is part of a NON-SPECIAL surface");
                surface.handleControlNodeMovement(node, x, y, hasStopped);
            }
            if (hasStopped) {
                //Log.i("DRAG", "Updating OpenGL data");
                updates.add(Constants.CAD_UPDATE_SURFACES);
            }

        } else {
            //Log.i("DRAG", "Node DOES NOT belong to a surface");
            node.SetCoordinates(x, y, 0f);
        }

        updates.add(Constants.CAD_UPDATE_FORCES);
        updates.add(Constants.CAD_UPDATE_LINES);
    }

    public static void handlePanning(float oldX, float oldY,
                                     float newX, float newY,
                                     PlotParameters plotParameters,
                                     float[] projectionMatrix) {
        float displacementOnX, displacementOnY;

        // Displacements from the start dragging point
        displacementOnX = newX - oldX;
        displacementOnY = newY - oldY;

        plotParameters.setCenter(plotParameters.getCenterX() - displacementOnX,
                plotParameters.getCenterY() - displacementOnY, plotParameters.getCenterZ());

        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
    }

    public static void handleZooming(SimplePoint a,
                                     SimplePoint b,
                                     Zoom zoom,
                                     PlotParameters plotParameters,
                                     float[] projectionMatrix) {
        float scaleFactor = zoom.getScaleFactorFromPointsInNDC(a, b);

        //Log.i("ZOOMING", "Scale factor = " + scaleFactor);

        // We update the plot parameters
        /*
        plotParameters.setCenter(
                zoom.zoomingCenterInWSC.x - zoom.displFromZoomingCenterToCentersView[0],
                zoom.zoomingCenterInWSC.y - zoom.displFromZoomingCenterToCentersView[1]
        );
        //*/
        plotParameters.setCenter(
                zoom.getZoomingCenterInWSC().x - zoom.getDisplacementToCentersViewOnX(),
                zoom.getZoomingCenterInWSC().y - zoom.getDisplacementToCentersViewOnY(),
                plotParameters.getCenterZ()
        );
        plotParameters.setZoomFactor(scaleFactor);
        //Log.i("ZOOMING", "New view center = " + plotParameters.getCenterX() + "  " + plotParameters.getCenterY());

        // We update the projection matrix
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());
    }

    private float clamp(float value, float min, float max) {
        return Math.min(max, Math.max(value, min));
    }

    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        textureProgram = new TextureShaderProgram(context);
        colorProgram = new ColorShaderProgram(context);

        texture = TextureHelper.loadTexture(context, R.drawable.air_hockey_surface);

        batchTextShaderProgram = new BatchTextProgram();
        batchTextShaderProgram.init();

        glText = new GLText(batchTextShaderProgram, context.getAssets());

        // Load the font from file (set size + padding), creates the texture
        // NOTE: after a successful call to this the font is ready for rendering!
        // Create Font (Height: Constants.FONT_SIZE Pixels / X+Y Padding 2 Pixels)
        glText.load( "Roboto-Regular.ttf", Constants.FONT_SIZE,  2, 2 );
    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to fill the entire surface.
        glViewport(0, 0, width, height);

        // Projection through a Frustum
        //MatrixHelper.perspectiveM(projectionMatrix, 45, (float) width / (float) height, 1f, 10f);

        float aspectRatio = (float)height / (float)width;
        if (width > height) {
            // Landscape
            axisOrientation = new AxisOrientation(-0.8f, -0.6f, 0.3f, 0.3f, aspectRatio, 1);
        } else {
            // Portrait
            axisOrientation = new AxisOrientation(-0.8f, -0.6f, 0.3f, 0.3f, 1, aspectRatio);
        }

        // Ortographic projection
        plotParameters.setScreenValues(width, height);
        orthoM(projectionMatrix, 0, plotParameters.getMinX(), plotParameters.getMaxX(),
                plotParameters.getMinY(), plotParameters.getMaxY(),
                plotParameters.getMinZ(), plotParameters.getMaxZ());

        /*
            orthoM(float[] m, int mOffset, float left, float right, float bottom, float top, float near, float far)

            float[] m       The destination array—this array’s length should be at least
                            sixteen elements so it can store the orthographic projection
                            matrix.
            int mOffset     The offset into m into which the result is written
            float left      The minimum range of the x-axis
            float right     The maximum range of the x-axis
            float bottom    The minimum range of the y-axis
            float top       The maximum range of the y-axis
            float near      The minimum range of the z-axis
            float far       The maximum range of the z-axis
        */

        /*
            perspectiveM(float[] m, float yFovInDegrees, float aspect, float n, float f)
            m               Array to store the matrix

            yFovInDegrees   Field of vision of the camera in degrees
            aspect          Aspect ratio of the screen
            n               This should be set to the distance to the near plane and must be
                            positive. For example, if this is set to 1, the near plane will be
                            located at z of -1.
            f               This should be set to the distance to the far plane and must be positive
                            and greater than the distance to the near plane
        */

        setLookAtM(viewMatrix, 0, 0f, 0f, 3f, 0f, 0f, 0f, 0f, 1f, 0f);

        /*
            setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX,
                       float centerY, float centerZ, float upX, float upY, float upZ)

            rm                              Destination array (rm)
            rmOffset                        Offset where the function will start the writing in the
                                            array
            eyeX, eyeY, eyeZ                This is where the eye will be
            centerX, centerY, centerZ       This is where the eye is looking, this position will
                                            appear in the center of the scene
            upX, upY, upZ                   If we were talking about your eyes, then this is where
                                            your head will be pointing. An upY of 1 means your head
                                            would be pointing straight up.

        */

        setIdentityM(viewMatrix, 0);

        // Nodes
        visualNode = new VisualCircle(
                Constants.VISUAL_NODE_RADIUS/plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                Constants.VISUAL_NODE_POINT_NUMPOINTS
        );

        visualPoint = new VisualCircle(
                Constants.VISUAL_POINT_RADIUS/plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                Constants.VISUAL_NODE_POINT_NUMPOINTS
        );

        // Supports
        visualPartialFixedNode = new VisualObject();
        visualPartialFixedNode.createPartialFixedSupport(0f, 0f,
                Constants.SUPPORT_TRIANGLE_BASE/plotParameters.getUnitLengthNormalizedCoordinateSystem());
        visualFixedNode = new VisualObject();
        visualFixedNode.createFixedSupport(0f, 0f,
                Constants.SUPPORT_TRIANGLE_BASE/plotParameters.getUnitLengthNormalizedCoordinateSystem());
        // Axis
        axis = new Axis();
        axis.setPlotParameters(plotParameters);
        visualAxis = new VisualAxis(axis);

        getLargestForce();
        visualForces = new VisualObject();
        visualForces.createForces(
                geometry.getPoints(),
                plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                largestForce
        );

        visualLinearForces = new VisualObject();
        visualLinearForces.createForces(
                geometry.getLines(),
                plotParameters.getUnitLengthNormalizedCoordinateSystem(),
                largestForce
        );
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        boolean updateVisualLines;
        boolean updateVisualSurfaces;
        boolean updateVisualForces;
        boolean updateVisualAxis;
        // Clear the rendering surface.
        glClear(GL_COLOR_BUFFER_BIT);

        // Update the viewProjection matrix, and create an inverted matrix for
        // touch picking.
        multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        invertM(invertedViewProjectionMatrix, 0, viewProjectionMatrix, 0);

        /*
        // Draw the table.
        positionTableInScene();
        textureProgram.useProgram();
        textureProgram.setUniforms(modelViewProjectionMatrix, texture);
        table.bindData(textureProgram);
        table.draw();
        //*/

        // Enables the program
        colorProgram.useProgram();

        int numberOfUpdates, currentUpdate, previousUpdate, laterUpdate;
        //Log.i("UPDATES", "Is empty before? = " + updates.isEmpty());

        numberOfUpdates = 0;
        currentUpdate = Constants.CAD_UPDATE_INVALID;
        if (!updates.isEmpty()) {
            currentUpdate = updates.poll();
            processUpdate(currentUpdate);
            numberOfUpdates = 1;
        }

        while (numberOfUpdates < 6) {
            //Log.i("UPDATES", "Size = " + updates.size());
            //Log.i("UPDATES", "Is empty after? = " + updates.isEmpty());

            if (updates.isEmpty()) break;

            laterUpdate = Constants.CAD_UPDATE_INVALID;
            while (!updates.isEmpty()) {
                laterUpdate = updates.poll();
                if (laterUpdate != currentUpdate) break;
            }

            if (laterUpdate == currentUpdate) break;
            else                              currentUpdate = laterUpdate;

            processUpdate(currentUpdate);
            numberOfUpdates++;
        }

        //*
        // When drawing lines or surfaces there is no translation
        positionObjectInScene(0f, 0f, 0f, viewProjectionMatrix, modelViewProjectionMatrix);
        if (flags.showGrid) drawGrid();
        drawAxis();

        colorProgram.useProgram();
        drawSupports();

        // When drawing lines or surfaces there is no translation
        positionObjectInScene(0f, 0f, 0f, viewProjectionMatrix, modelViewProjectionMatrix);

        // Enables transparency in OpenGL to draw the surfaces
        glEnable(GLES20.GL_BLEND);
        glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        drawSurfaces();
        GLES20.glDisable(GLES20.GL_BLEND);

        drawLines();

        drawPoints();

        // One object is used to draw all the nodes (in this case the moved every time)
        drawNodes();

        positionObjectInScene(0f, 0f, 0f, viewProjectionMatrix, modelViewProjectionMatrix);
        drawForces();

        drawAxisOrientation();
        //*/

        // Reset the flags when the update is done
        //resetUpdateFlags();
    }

    private void processUpdate(int update) {
        switch (update) {
            case Constants.CAD_UPDATE_LINES:
                if ((Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem()) &&
                        (null != problemData.solidOfRevolution.getAxis()))
                    visualLines.updateOpenGLData(geometry.getLines(), true,
                            problemData.solidOfRevolution.getAxis());
                else
                    visualLines.updateOpenGLData(geometry.getLines(), false, null);
                Log.i("Updates", "Lines");
                break;

            case Constants.CAD_UPDATE_AXIS:
                axis.setPlotParameters(plotParameters);
                visualAxis.updateOpenGLData(axis);
                Log.i("Updates", "Axis");
                break;

            case Constants.CAD_UPDATE_FORCES:
                getLargestForce();
                visualForces.updateForces(geometry.getPoints(),
                        plotParameters.getUnitLengthNormalizedCoordinateSystem(), largestForce);
                visualLinearForces.updateForces(geometry.getLines(),
                        plotParameters.getUnitLengthNormalizedCoordinateSystem(), largestForce);
                Log.i("Updates", "Forces");
                break;

            case Constants.CAD_UPDATE_SURFACES:
                visualSurfaces.updateOpenGLData(geometry.getSurfaces());
                Log.i("Updates", "Surfaces");
                break;

            default:
                Log.i("Updates", "Unknown");
                break;
        }
    }

    private void drawForces() {
        visualForces.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 125f / 255f, 240f / 255f, 217f / 255f);

        visualForces.draw();

        visualLinearForces.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 125f / 255f, 255f / 255f, 150f / 255f);
        visualLinearForces.draw();
    }

    private void drawAxis() {
        visualAxis.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.5f, 0.5f, 0.5f);
        visualAxis.drawAxis();

        glEnable(GLES20.GL_BLEND);
        glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        glText.setScale(Constants.FONT_SIZE_ON_SCREEN /
                        (Constants.FONT_SIZE * plotParameters.getUnitLengthNormalizedCoordinateSystem())
        );
        glText.begin(1.0f, 1.0f, 1.0f, 1.0f, modelViewProjectionMatrix);
        glText.draw("                  ", 1f, 0f);
        axis.mainTics.drawLabels(glText);
        glText.draw("                  ", 0f, 0f);
        glDisable(GLES20.GL_BLEND);
    }

    private void drawGrid() {
        visualAxis.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.2f, 0.2f, 0.2f);
        visualAxis.drawGrid();
    }

    private void drawAxisOrientation() {
        axisOrientation.bindData(colorProgram);
        setIdentityM(modelViewProjectionMatrix, 0);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0.5f, 0.5f, 0.2f);
        axisOrientation.draw();
    }

    private void drawNodes() {
        Node node;

        visualNode.bindData(colorProgram);

        for (int i = 0; i < geometry.numberOfNodes(); i++) {
            node = geometry.getNode(i);
            positionObjectInScene(node.GetX(), node.GetY(), node.GetZ(), viewProjectionMatrix,
                    modelViewProjectionMatrix);
            if (!node.propertiesInCAD.isSelected())
                colorProgram.setUniforms(modelViewProjectionMatrix,
                    colorNode[0], colorNode[1], colorNode[2]);
            else
                colorProgram.setUniforms(modelViewProjectionMatrix,
                        colorSelectedNode[0], colorSelectedNode[1], colorSelectedNode[2]);
            visualNode.draw();
        }
    }

    private void drawPoints() {
        Point point;

        visualPoint.bindData(colorProgram);

        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            positionObjectInScene(point.getX(), point.getY(), point.getZ(), viewProjectionMatrix,
                    modelViewProjectionMatrix);
            colorProgram.setUniforms(modelViewProjectionMatrix, 0.5f, 1.0f, 0.5f);
            visualPoint.draw();
        }
    }

    private void drawSupports() {
        //Node node;
        Point point;
        boolean isFixedOnX, isFixedOnY;

        visualPartialFixedNode.bindData(colorProgram);

        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasFixedDisplacement()) {
                    isFixedOnX = point.getConditions().getFixedDisplacement().isFixedX();
                    isFixedOnY = point.getConditions().getFixedDisplacement().isFixedY();
                    if (isFixedOnX && isFixedOnY) continue;
                    positionObjectInScene(point.getX(), point.getY(), point.getZ(), viewProjectionMatrix, aux);
                    if (isFixedOnX)
                        // Rotate 90 degrees the support around the z-axis
                        rotateScene(-90f, 0f, 0f, 1f, aux, modelViewProjectionMatrix);
                    else
                        rotateScene(0f, 0f, 0f, 1f, aux, modelViewProjectionMatrix);

                    colorProgram.setUniforms(modelViewProjectionMatrix, 1f, 162f / 255f, 0f);
                    visualPartialFixedNode.draw();
                }
            }
        }

        visualFixedNode.bindData(colorProgram);
        for (int i = 0; i < geometry.numberOfPoints(); i++) {
            point = geometry.getPoint(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasFixedDisplacement()) {
                    isFixedOnX = point.getConditions().getFixedDisplacement().isFixedX();
                    isFixedOnY = point.getConditions().getFixedDisplacement().isFixedY();
                    if ((!isFixedOnX) || (!isFixedOnY)) continue;
                    positionObjectInScene(point.getX(), point.getY(), point.getZ(),
                            viewProjectionMatrix, modelViewProjectionMatrix);
                    colorProgram.setUniforms(modelViewProjectionMatrix, 1f, 162f / 255f, 0f);
                    visualFixedNode.draw();
                }
            }
        }
    }

    private void drawLines() {

        visualLines.bindData(colorProgram);

        // Non-selected lines
        colorProgram.setUniforms(modelViewProjectionMatrix, colorLine[0], colorLine[1], colorLine[2]);
        visualLines.drawNonSelectedLines();

        // Selected lines
        colorProgram.setUniforms(modelViewProjectionMatrix, colorSelectedLine[0],
                colorSelectedLine[1], colorSelectedLine[2]);
        visualLines.drawSelectedLines();

        if ((Constants.SOLID_OF_REVOLUTION == problemData.getTypeOfProblem()) &&
                (null != problemData.solidOfRevolution.getAxis())) {
            colorProgram.setUniforms(modelViewProjectionMatrix, 1f, 1f, 0.3f);
            visualLines.drawRotationAxis();
        }
    }

    private void drawSurfaces() {
        visualSurfaces.bindData(colorProgram);
        colorProgram.setUniforms(modelViewProjectionMatrix, 0f, 0f, 0.7f, 0.5f);
        visualSurfaces.draw();
    }

    private void positionTableInScene() {
        // The table is defined in terms of X & Y coordinates, so we rotate it
        // 90 degrees to lie flat on the XZ plane.
        setIdentityM(modelMatrix, 0);
        //rotateM(modelMatrix, 0, -90f, 1f, 0f, 0f);
        multiplyMM(modelViewProjectionMatrix, 0, viewProjectionMatrix,
                0, modelMatrix, 0);
    }

    // The mallets and the puck are positioned on the same plane as the table.
    private void positionObjectInScene(float x, float y, float z, float[] matrixIn, float[] matrixOut) {
        setIdentityM(modelMatrix, 0);
        translateM(modelMatrix, 0, x, y, z);
        multiplyMM(matrixOut, 0, matrixIn, 0, modelMatrix, 0);
    }
    private void rotateScene(float angle, float x, float y, float z, float[] matrixIn, float[] matrixOut) {
        setIdentityM(modelMatrix, 0);
        rotateM(modelMatrix, 0, angle, x, y, z);
        multiplyMM(matrixOut, 0, matrixIn, 0, modelMatrix, 0);
    }

    public Surfaces getSurfaces() { return geometry.getSurfaces(); }
    public Nodes getNodes() { return geometry.getNodes(); }
    public Lines getLines() { return geometry.getLines(); }

    // Current force to be applied
    public void setCurrentForce(Force currentForce) {
        Log.i("Current Force Setting", "" + currentForce.getForceX() + "  " + currentForce.getForceY());
        this.currentForce.copyForce(currentForce);
    }

    // Current fixed displacement to be applied
    public void setCurrentFixedDisplacement(FixedDisplacement fixedDisplacement) {
        this.currentFixedDisplacement.copyFixedDisplacement(fixedDisplacement);
    }

    public void unselectLines() {
        Log.i("Unselecting lines", "Size = " + geometry.numberOfLines());
        for (int i = 0; i < geometry.numberOfLines(); i++)
            geometry.getLine(i).propertiesInCAD.unselect();
    }

    public void showGrid() {
        flags.showGrid = true;
    }

    public void hideGrid() {
        flags.showGrid = false;
    }

    public void setNumberOfSidesRegularPolygon(int numberSidesRegularPolygon) {
        this.numberOfSidesRegularPolygon = numberSidesRegularPolygon;
    }

    public void setNumberOfDivisions(int numberOfDivisions) {
        this.numberOfDivisions = numberOfDivisions;
    }

    private SimplePoint getPointOnTheGrid(float x, float y) {
        float intervalSize, previousX, previousY;
        int numberOfIntervalsOnX, numberOfIntervalsOnY;
        SimplePoint gridPoint = new SimplePoint();

        // When the second tics are shown I have to change this
        intervalSize = axis.mainTics.getIntervalSize();

        numberOfIntervalsOnX = (int)Math.floor(x / intervalSize);
        if (x < 0f) numberOfIntervalsOnX = (int)Math.ceil(x / intervalSize);
        numberOfIntervalsOnY = (int)Math.floor(y / intervalSize);
        if (y < 0f) numberOfIntervalsOnY = (int)Math.ceil(y / intervalSize);

        previousX = (float)numberOfIntervalsOnX*intervalSize;
        previousY = (float)numberOfIntervalsOnY*intervalSize;

        gridPoint.x = previousX;
        if (Math.abs(x) > Math.abs(previousX)+ 0.5f*intervalSize){
            if (x > 0f) gridPoint.x = previousX + intervalSize;
            else        gridPoint.x = previousX - intervalSize;
        }

        gridPoint.y = previousY;
        if (Math.abs(y) > Math.abs(previousY) + 0.5f*intervalSize){
            if (y > 0f) gridPoint.y = previousY + intervalSize;
            else        gridPoint.y = previousY - intervalSize;
        }

        gridPoint.z = 0f;

        return gridPoint;
    }

    private void splitSegment(Line selectedLine) {
        int numberOfNewPoints;
        SimplePoint position;
        Vector delta;
        Node node;
        Point point1, point2, point;
        Line line;
        Surface surface = null;
        boolean partOfASurface = false;
        boolean createNodes = false;

        // Nothing needs to be done when this occurs
        if (null == selectedLine) return;
        if (numberOfDivisions < 2) return;

        // Number of nodes needed to define the new segments
        numberOfNewPoints = numberOfDivisions - 1;

        // The lines are created using objects of type Point
        point1 = selectedLine.getFirstPoint();
        point2 = selectedLine.getLastPoint();

        delta = new Vector();
        delta.x = (point2.getX() - point1.getX()) / (float)numberOfDivisions;
        delta.y = (point2.getY() - point1.getY()) / (float)numberOfDivisions;
        delta.z = (point2.getZ() - point1.getZ()) / (float)numberOfDivisions;

        // To store the location where the new nodes will be added
        position = new SimplePoint(point1.getX(), point1.getY(), point1.getZ());

        // If the selected line belongs to a surface, removes the line from the surface
        if (selectedLine.surfaceProperties.isItPartOfASurface()) {
            partOfASurface = true;
            surface = selectedLine.surfaceProperties.getSurface();
            surface.print("SPLITTING", "Surface before");
            surface.removeLine(selectedLine);
        }

        if (selectedLine.getFirstPoint().getNodeInfo().isPartOfANode() &&
                selectedLine.getLastPoint().getNodeInfo().isPartOfANode()) createNodes = true;

        // Adds almost all the new segments
        point1 = selectedLine.getFirstPoint();
        for (int i = 0; i < numberOfNewPoints; i++) {
            position = position.translate(delta);
            if (createNodes) {
                node = new Node(position.x, position.y, position.z);
                point = node.getPoint();
            } else {
                point = new Point(position.x, position.y, position.z);
            }
            geometry.addPoint(point);

            point2 = point;
            line = new Line(point1, point2);

            geometry.addLine(line);
            if (partOfASurface) surface.addLine(line);

            point1 = point2;
        }
        // Adds the new last segment
        point2 = selectedLine.getLastPoint();
        line = new Line(point1, point2);

        geometry.addLine(line);
        if (partOfASurface) surface.addLine(line);

        if (partOfASurface) {
            // To report that the construction of the surface has finished
            surface.commit();
            surface.print("SPLITTING", "Surface after");
            // Mesh it to later draw it with OpenGL
            surface.setModel(surface.createModel(surface));
            surface.meshSurface();
        }

        // Deletes the original line from the set of lines (do this until the very end, this
        // operation deletes the element completely)
        geometry.deleteLine(selectedLine);
    }

    private void handleExtrude(Line selectedLine, float pointerPosOnX, float pointerPosOnY,
                               int dragID, boolean hasStopped) {
        boolean canBeExtruded;
        if (flags.newExtrude) {
            // When we want to start a extrude
            canBeExtruded = extrudeLine.startExtrude(selectedLine, geometry, dragID);
            if (canBeExtruded) {
                Log.i("DRAGGING", "Extrude beginning");
                extrudeLine.extrude(pointerPosOnX, pointerPosOnY, hasStopped);
                flags.newExtrude = false;
                if (hasStopped) updates.add(Constants.CAD_UPDATE_SURFACES);
                updates.add(Constants.CAD_UPDATE_LINES);
                updates.add(Constants.CAD_UPDATE_FORCES);
            } else {
                Log.i("DRAGGING", "Extrude cannot be started");
                // If the extrude cannot be done we reset these variables
                flags.newExtrude = true;
                currentAction = Constants.DOING_NOTHING;
                extrudeLine.stopExtrude();
            }
        } else {
            // When we are doing a extrude. We perform the extrude only if the dragID is the same.
            if (dragID == extrudeLine.getDragID()) {
                Log.i("DRAGGING", "Extrude continuing");
                extrudeLine.extrude(pointerPosOnX, pointerPosOnY, hasStopped);
                if (hasStopped) updates.add(Constants.CAD_UPDATE_SURFACES);
                updates.add(Constants.CAD_UPDATE_LINES);
                updates.add(Constants.CAD_UPDATE_FORCES);
            } else {
                Log.i("DRAGGING", "Extrude cannot be continued");
                // If the extrude cannot be done we reset these variables
                flags.newExtrude = true;
                currentAction = Constants.DOING_NOTHING;
                extrudeLine.stopExtrude();
            }
        }
    }

    private void getLargestForce() {
        Points points = geometry.getPoints();
        Lines lines = geometry.getLines();
        Point point;
        Line line;
        float force;

        // Nodal forces
        largestForce = 0f;
        for (int i = 0; i < points.getSize(); i++) {
            point = points.getElement(i);
            if (point.hasConditions()) {
                if (point.getConditions().hasAppliedForce()){
                    force = point.getConditions().getForce().getMagnitude();
                    if (force > largestForce) largestForce = force;
                }
            }
        }

        // Linear forces
        for (int i = 0; i < lines.getSize(); i++) {
            line = lines.getElement(i);
            if (line.hasConditions()) {
                if (line.getConditions().hasAppliedForce()){
                    force = line.getConditions().getForce().getMagnitude();
                    if (force > largestForce) largestForce = force;
                }
            }
        }
    }

    public void updateLines() { updates.add(Constants.CAD_UPDATE_LINES); }
    public void updateForces() { updates.add(Constants.CAD_UPDATE_FORCES); }
    public void updateSurfaces() { updates.add(Constants.CAD_UPDATE_SURFACES); }

}