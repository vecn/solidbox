/**
 * Model: Definition of the Geometry. This class is used as input for most
 * of the Geometric Bot routines.
 * 2011-2015 Victor Eduardo Cardoso Nungaray
 * Twitter: @victore_cardoso
 * email: victorc@cimat.mx
 *
 */

package nb.geometricBot;

public class Model {
    protected float[] vertices;   /* Concatenated coordinates of vertices */
    protected int[]  edges;       /* Concatenated IDs of vertices forming edges */
    protected float[] holes;      /* Concatenated coordinates of holes */

    public Model() {
	vertices = null;
	edges = null;
	holes = null;
    }

    static {
	//System.loadLibrary("nbots");
	//System.loadLibrary("nbots_jni");
        System.loadLibrary("nbots");
    }

    public static native Model combine(Model modelA, Model modelB);
    public static native Model unify(Model modelA, Model modelB);
    public static native Model intersect(Model modelA, Model modelB);
    public static native Model substract(Model modelA, Model modelB);
    public static native Model difference(Model modelA, Model modelB);
    
    public native ModelStatus verify();
    public native boolean isContinuum();
    public native boolean isPointInside(double x, double y);

    public int getNVertices() {
	if (null == vertices)
	    return 0;
	else
	    return vertices.length / 2;
	
    }

    public int getNEdges() {
	if (null == edges)
	    return 0;
	else
	    return edges.length / 2;
    }

    public int getNHoles() {
	if (hasHoles())
	    return holes.length / 2;
	else
	    return 0;
    }

    public boolean hasHoles() {
	return (null != holes);
    }

    public float[] getVerticesRef() {
	return vertices;
    }

    public int[] getEdgesRef() {
	return edges;
    }

    public float[] getHolesRef() {
	return vertices;
    }

    public void setEdges(int[] edges) { this.edges = edges; }
    public void setVertices(float[] vertices) { this.vertices = vertices; }
    public void setHoles(float[] holes) { this.holes = holes; }
}
